{% unless chartJsAlreadyIncluded %}
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
{% endunless %}
{% assign chartJsAlreadyIncluded = true %}