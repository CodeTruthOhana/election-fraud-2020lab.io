
## Election Integrity Scorecard

{% assign scorecard = site.data.electionScorecard | find: 'state', page.state %}
{% assign totalColorGrade = scorecard.total | divided_by: 5.0 | round | minus: 11 %}
{% if totalColorGrade > 6 %}
    {% assign totalColorGrade = 6 %}
{% elsif totalColorGrade < 0 %}
    {% assign totalColorGrade = 0 %}
{% endif %}

The Heritage Foundation's [Election Integrity Scorecard](https://www.heritage.org/electionscorecard/index.html) is an assessment of each state's voting laws, written regulations and procedures. {{ page.state }} was ranked {{ scorecard.rank | downcase }} out of 51.

[![Election Integrity Scorecard](/images/scorecard.png){:width="1017" height="627" style="width: 520px"}](https://www.heritage.org/electionscorecard/index.html)

<div class="responsive-table scorecard-table" style="font-size: 85%">
<table>
<tbody>
<tr><th>{{ page.state }}'s Total Score </th><th class="color-grade-{{ totalColorGrade }}"> {{ scorecard.total }}%</th></tr>
<tr><td>Voter ID Implementation </td><td> {{ scorecard.voterIdImplementation | divided_by: 20.0 | times: 100 | round }}%</td></tr>
<tr><td>Accuracy of Voter Registration Lists </td><td> {{ scorecard.accuracyOfVoterRegistrationLists | divided_by: 30.0 | times: 100 | round }}%</td></tr>
<tr><td>Absentee Ballot Management </td><td> {{ scorecard.absenteeBallotManagement | divided_by: 21.0 | times: 100 | round }}%</td></tr>
<tr><td>Vote Harvesting/Trafficking Restrictions </td><td> {{ scorecard.voteHarvestingTraffickingRestrictions | divided_by: 4.0 | times: 100 | round }}%</td></tr>
<tr><td>Access of Election Observers </td><td> {{ scorecard.accessOfElectionObservers | divided_by: 3.0 | times: 100 | round }}%</td></tr>
<tr><td>Verification of Citizenship </td><td> {{ scorecard.verificationOfCitizenship | divided_by: 4.0 | times: 100 | round }}%</td></tr>
<tr><td>Identification for Voter Assistance </td><td> {{ scorecard.identificationForVoterAssistance | divided_by: 3.0 | times: 100 | round }}%</td></tr>
<tr><td>Vote Counting Practices </td><td> {{ scorecard.voteCountingPractices | divided_by: 3.0 | times: 100 | round }}%</td></tr>
<tr><td>Election Litigation Procedures </td><td> {{ scorecard.electionLitigationProcedures | divided_by: 3.0 | times: 100 | round }}%</td></tr>
<tr><td>Restriction of Same-day Registration </td><td> {{ scorecard.restrictionOfSameDayRegistration | divided_by: 3.0 | times: 100 | round }}%</td></tr>
<tr><td>Restriction of Automatic Registration </td><td> {{ scorecard.restrictionOfAutomaticRegistration | divided_by: 3.0 | times: 100 | round }}%</td></tr>
<tr><td>Restriction of Private Funding of Election Officials or Government Agencies </td><td> {{ scorecard.restrictionOfPrivateFunding | divided_by: 3.0 | times: 100 | round }}%</td></tr>
</tbody>
</table>
</div>

[View the full Scorecard for {{ page.state }}](https://www.heritage.org/electionscorecard/pages/states/{{ page.abbr }}.html){:.button}

It's important to note that the scores above reflect the laws, regulations, and processes that exist within the state, but not whether they were actually followed. In the 2020 election, some states with highly-rated election integrity measures did not uphold them. [^scorecard1] As The Heritage Foundation reminds us, even the best laws are not worth much unless responsible officials enforce them rigorously. It is up to the citizens of each state to make sure that their elected and appointed public officials do just that.

[^scorecard1]: Looking at the nationwide trends over the 2020 election, it became commonplace to stretch or completely ignore the laws for absentee/mail-in ballot eligibility, bulk ballot collection, independent observation, voter ID, signature verification, citizenship verification, removal of inactive/ineligible voters from rolls, [voting machine accreditation](/in-detail/vstl-labs-not-eac-accredited/), retention of election records, transparency of operations, and numerous other laws. Most breaches have not yet been prosecuted.

