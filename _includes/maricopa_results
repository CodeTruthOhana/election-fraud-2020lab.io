
On Sep 24, 2021, the Arizona Senate heard presentations from the key auditors, presenting their findings. 

{:.highlight}
> *The audit report identifies more than 57K questionable votes, intentionally deleted election files, and other suspicious voting machine activity.* [^r1]

The official written reports can be obtained from [The Arizona State Senate
Republican Caucus](https://www.azsenaterepublicans.com/audit), or we've included direct links below.

{% include expandable_box 
title="Show Full List of Reports"
content="

{:.small}
Cyber Ninjas Report <br> (recount and ballot audits) | [Presentation 1](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_f878dbf2bfa24d1e90f54618df2b80c5.pptx?dn=Cyber%20Ninjas%20Presentation%201.pptx)
 | [Presentation 2](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_1d9d46ba558e4edd8107297bd20908a4.pptx?dn=Cyber%20Ninjas%20Presentation%202.pptx)
 | [Report Volume 1](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_a91b5cd3655445b498f9acc63db35afd.pdf)
 | [Report Volume 2](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_1ec91dd80a024d5d8612c5490de1c460.pdf)
 | [Report Volume 3](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_d36cb5eaca56435d84171b4fe7ee6919.pdf)
CyFIR Report <br> (voting machines & cyber security) | [Presentation from Ben Cotton, CyFIR Founder](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_2a043c72ca3e4823bdb76aaa944728d1.pptx?dn=Cotton%20Presentation.pptx)
EchoMail Report <br> (ballot envelopes) | [Report by Dr. Shiva Ayyadurai](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_05deb65815ab4d4b83938d71bc53459b.pdf)
Randy Pullen's Report | [Presentation](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_da0ff269385247e4a2a609e810615347.pptx?dn=Pullen%20Presentation.pptx)
 | [Report](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_763c992801e445bdae02f07f2c9c1876.pdf)
Ken Bennett's Report (election procedures) | [Presentation](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_32f578c8a8494eeb8b5e56fd75300b68.pptx?dn=Bennett%20Presentation.pptx)
 | [Report](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_7fd579d4a05149cfae79f0c3e3209abb.pdf)
Senate President Karen Fann | [Letter to Attorney General Mark Brnovich](https://wixlabs-pdf-dev.appspot.com/assets/pdfjs/web/viewer.html?file=%2Fpdfproxy%3Finstance%3DP7TOcRxxoPNdfJRKbKZlVlMZWQdBP9purLF3vn3HWlo.eyJpbnN0YW5jZUlkIjoiYzc4ZmVkYTctZDQ1Yy00NTM1LTlhYzktOTRkYmIzODBmODBlIiwiYXBwRGVmSWQiOiIxM2VlMTBhMy1lY2I5LTdlZmYtNDI5OC1kMmY5ZjM0YWNmMGQiLCJtZXRhU2l0ZUlkIjoiYzY5MmY1MjctZGE3NS00Yzg2LWI1ZDEtOGIzZDVkNGQ1YjQzIiwic2lnbkRhdGUiOiIyMDIxLTA5LTI1VDAwOjAxOjI0Ljc2MFoiLCJkZW1vTW9kZSI6ZmFsc2UsImFpZCI6IjQyMTUyMDZhLWQyMWUtNDM2ZS1iOGJmLWUyZTAzNDI3YTE1OSIsImJpVG9rZW4iOiIwMTFkMTg4MC0wZTI5LTA5YjMtMmYxOC0xZmU2ZWVjZGEzNGQiLCJzaXRlT3duZXJJZCI6IjJmMzQ3MGVlLWZiYTAtNDUxNS1hYTMwLTViZGM1MmQyZWI4ZSJ9%26compId%3Dcomp-ktyy3f60%26url%3Dhttps%3A%2F%2Fdocs.wixstatic.com%2Fugd%2F2f3470_ffcfbdf19b944b409b0f64bc0547146a.pdf&rng=1632528512931#page=1&links=true&originalFileName=Audit%20Cover%20Letter%20to%20Attorney%20General&locale=en&allowDownload=true&allowPrinting=true&pagemode=none)

{:.small}
We recommend checking [The Arizona State Senate Republican Caucus](https://www.azsenaterepublicans.com/audit) website for the latest versions, in case they release further reports or updates that we have not listed here.

"%}

Major issues identified:

* 255,326 early votes are recorded in the VM55 file of counted ballots but are missing from the EV33 file [^r6]

* 17,322 duplicate votes (where voters sent in 2 or more ballots) that were illegally counted in the final certified tally (a large surge of these were dated the week following the election) [^r4]

* 27,807 ballots cast from individuals who had moved prior to the election, and are unlikely to have physically received their ballots legally

* Envelopes without signatures that were stamped as "approved"

* Ballot envelopes showing an apparent "approved" stamp behind the basic graphics of the envelope, suggesting tampering, or at the very least, poor quality scanning and archival of envelope images

* The systems related to elections integrity had numbers that could not balance or agree with each other

* The voter rolls and the registration management process itself having many data integrity issues. For instance, over 200 individuals were easily identifiable as likely being the same person but having two different Voter IDs and voting twice in the election.

* Without access to the County’s detailed records including personally identifiable information and registration systems it is more likely there were many tens of thousands of improper votes in the election from double voters, deceased voters, voters for which there is no trace in the public records nor association to their voting address

* Proper voter registration law and procedures were not followed

* There were unexplained large purges of registered voters, right after the election, of people who had voted in the election

* There was back-dating of registrations, adjustments made to historical voting and voter records, unexplained linking of voter registration affidavits to multiple voters and more

* Files were missing from the Election Management System (EMS) Server and other voting machines, either intentionally or negligently removed

* Ballot images on the EMS were corrupt or missing

* Logs appeared to have been intentionally overwritten

* All data in one database related to the 2020 General Election had been fully wiped. This occurred on the *day prior* to Maricopa's own internal audit. [^r2]

* On the ballot side, batches were not always clearly delineated, duplicated ballots were missing the required serial numbers, some originals were duplicated more than once, and the auditors were never provided chain-of-custody documentation for the ballots for the time-period prior to the ballot’s movement into the auditors’ care. This all increased the complexity and difficulty in properly auditing the results.

* There were significant anomalies identified in the ratio of hand-folded ballots, on-demand printed ballots, and a significant increase in provisional ballot rejections for a mail-in ballot already being cast, suggestive of mail-in ballots being cast for voters without their knowledge.

* Maricopa County failed to follow basic cyber security best practices and guidelines from CISA [^r3]

* Remote Access and "Terminal Services" features of Windows were enabled allowing machines to be remotely controlled [^r3]

* Software and patch protocols were not followed [^r3]

* Credential management was flawed: unique usernames and passwords were not allocated. Many (if not all) accounts shared the same password, and multiple users appear to have shared the same account. [^r3]

* A dual-boot configuration was discovered on adjudication equipment which is *not* an approved configuration. The second hard drive contained non-Maricopa County data.

Here is a summary table from the Cyber Ninjas report (Volume III). This does ***not*** include the 17,322 duplicate ballots found by Dr. Shiva (EchoMail) and the 255,326 early votes that appear in the VM55 but are missing from EV33 file.

[![](/images/arizona/report-summary.png){:.image-border}](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_d36cb5eaca56435d84171b4fe7ee6919.pdf)

A lot of the mainstream media has picked up on a single results table from Doug Logan that showed that the audit team's count of the ballots closely matched the certified results, using that as supposed proof that Biden still won the state. What is being overlooked is the highly questionable validity of those votes. 

Also note that this is not the complete audit report. Analysis of the routers, Splunk logs, and paper ballots is still ongoing.

Thanks to [@LibertyOverwatchChannel](https://t.me/LibertyOverwatchChannel) for sifting through the reports and providing summaries. A further summary, with slightly more detail is on [Patrick Byrne's website](https://www.deepcapture.com/2021/09/based-on-these-factual-findings-the-election-should-not-be-certified-and-the-reported-results-are-not-reliable/).

Below, you can watch the full 3-hour hearing:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/sAAu6O33rNE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Several workers from the forensic audit --- Maricopa residents who voted in the election and passed background checks --- were interviewed on what they witnessed during the audit. They reported seeing numerous anomalies including ballots that were filled in "too perfectly", indicating possible duplication by machine; and abnormal repeating patterns such as 7 for Biden, 1 for Trump, 7 for Biden, 1 for Trump, 7 for Biden, 1 for Trump. Despite being reported, these issues may not have made it into the final report. See their testimony in the video below:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vkj62l/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

*Editor's note:* Regarding the machine-printed ballots mentioned in the video above, Doug Logan, the lead auditor from Cyber Ninjas reported that there *are* some legitimate scenarios where ballots are marked by a machine, such as if they are damaged, or unable to be scanned [^r7], or from ballot marking devices (BMDs) used by voters who are unable or uncomfortable marking a ballot by hand. While BMDs may be needed for voters with disabilities, they have [known issues](https://www.electiondefense.org/ballot-marking-devices) when used in large populations. 


{:.info}
*Analysis of the routers, Splunk logs, and paper ballots is still ongoing. We'll update this page as more findings are uncovered.*


### Continued Debate

Maricopa County officials took to Twitter and sympathetic media channels to deny the allegations and minimize the report's findings. They claimed the results were mistaken and that they had provided everything in the Senate's subpoena, despite several key items still being withheld. 

Members of the Maricopa County Board of Supervisors, as well as Ken Bennett, Senate liason for the audit, presented testimony to Federal Congress' Oversight Committee on the findings, during which the county admitted to removing election files from machines prior to the audit. [^r8]

Dr. Shiva, who ran the audit of ballot envelopes, held a follow-up open discussion forum on the Maricopa Audit, open to Republicans, Democrats and state/county officials. See the [full video and our summary here](/in-detail/dr-shiva-maricopa-arizona-audit-open-forum/).

Cyber Ninjas later provided a great, [detailed rebuttal](https://wendyrogers.org/cyber-ninjas-respond-to-maricopa-county-lies/) to Maricopa County on their denials and deflections. It addresses specific claims, one-by-one, with screenshot evidence and links to further references.


### So What Happens Now?

Senator Karen Fann has [forwarded](https://wixlabs-pdf-dev.appspot.com/assets/pdfjs/web/viewer.html?file=%2Fpdfproxy%3Finstance%3DP7TOcRxxoPNdfJRKbKZlVlMZWQdBP9purLF3vn3HWlo.eyJpbnN0YW5jZUlkIjoiYzc4ZmVkYTctZDQ1Yy00NTM1LTlhYzktOTRkYmIzODBmODBlIiwiYXBwRGVmSWQiOiIxM2VlMTBhMy1lY2I5LTdlZmYtNDI5OC1kMmY5ZjM0YWNmMGQiLCJtZXRhU2l0ZUlkIjoiYzY5MmY1MjctZGE3NS00Yzg2LWI1ZDEtOGIzZDVkNGQ1YjQzIiwic2lnbkRhdGUiOiIyMDIxLTA5LTI1VDAwOjAxOjI0Ljc2MFoiLCJkZW1vTW9kZSI6ZmFsc2UsImFpZCI6IjQyMTUyMDZhLWQyMWUtNDM2ZS1iOGJmLWUyZTAzNDI3YTE1OSIsImJpVG9rZW4iOiIwMTFkMTg4MC0wZTI5LTA5YjMtMmYxOC0xZmU2ZWVjZGEzNGQiLCJzaXRlT3duZXJJZCI6IjJmMzQ3MGVlLWZiYTAtNDUxNS1hYTMwLTViZGM1MmQyZWI4ZSJ9%26compId%3Dcomp-ktyy3f60%26url%3Dhttps%3A%2F%2Fdocs.wixstatic.com%2Fugd%2F2f3470_ffcfbdf19b944b409b0f64bc0547146a.pdf&rng=1632528512931#page=1&links=true&originalFileName=Audit%20Cover%20Letter%20to%20Attorney%20General&locale=en&allowDownload=true&allowPrinting=true&pagemode=none) all reports onto Attorney General Mark Brnovich, who is in the process of identifying criminal activity and other breaches of law that need to be prosecuted.

The state congress will also likely explore further actions such as potentially decertifying the results. We expect to hear more about this in the coming days and weeks. [^r5]






[^r1]: Correction: We previously quoted the report as saying *Cyber Ninjas writes, “based on these factual findings, the election should not be certified, and the reported results are not reliable."* Apparently this conclusion that "the election should not be certified" was included in an early, leaked draft report, but Doug Logan, author of the report states that this was added to the draft by a junior audit staff member and was removed from the final report. [^r7] [^r1b] Doug made every attempt to present the findings factually and neutrally. We have since removed that statement.

[^r1b]: According to interview with Doug Logan at The Gateway Pundit: "[Exclusive Interview with Doug Logan from Cyber Ninjas on the Arizona Senate Forensic Audit – Updated](https://www.thegatewaypundit.com/2021/09/threatened-exclusive-interview-doug-logan-cyber-ninjas-arizona-senate-forensic-audit/)", Sep 28, 2021

[^r2]: Maricopa County claims that they "archived" a lot of data prior to the audit, and are still in possession of this data, but since it was not listed in the subpoena, they were not required to provide it to auditors. It has not yet been confirmed whether this data actually exists as they claim.

[^r3]: Maricopa County claim that these security practices are unnecessary because the vote tabulation network is "airgapped" (meaning it's on its own isolated network, unconnected to the internet), however they appear to be naively unaware (or criminally complicit) as to the numerous ways that attackers can infiltrate an airgapped network. Evidence in the report also demonstrates that there were periods of internet connectivity, contradicting their claims.

[^r4]: According to Dr. Shiva, there was a massive surge of duplicate ballots *AFTER* the polls closed on election day. Between November 4th and November 9th, 25% of all double votes were received by election officials, eventually getting counted in the final total. Curious timing? Did they wait to see how many extra votes they needed? See the relevant snippet of the presentation via [this Tweet](https://twitter.com/realLizUSA/status/1441507645119205383?s=20).

    Dr. Shiva and his research team were able to track the timing of the duplicate ballots by looking at the timestamps of all Early Voting Ballots that were received by election officials.

    Dr. Shiva also held a follow-up open forum on the Maricopa Audit. See the [full video and our summary here](/in-detail/dr-shiva-maricopa-arizona-audit-open-forum/).

[^r5]: Whether the state legislature can actually "decertify" the previous election is hotly debated. Arizona Governor Doug Ducey has stated it's not legally possible to do so, and he won't pursue it (although he has been highly resistant to the entire audit process, and potentially complicit). Attorney Matt DePerno has [written](https://wendyrogers.org/attorney-matt-deperno-responds-to-az-senate-lawyer-who-claimed-senate-could-not-decertify/) to Senator Wendy Rogers and also stated in [interviews](https://rumble.com/embed/vkc4hz/?pub=4) that the Constitution *does* give this power to the state. They just need the courage to exert the power given to them.

[^r6]: UncoverDC explains these files and what they were expected to contain. See "[Cyber Ninjas Report: 255,326 Ballots Cast, No Record Of Receipt](https://uncoverdc.com/2021/09/28/cyber-ninjas-report-255326-ballots-cast-no-record-of-receipt/)", Sep 28, 2021.

[^r7]: Conservative Podcast: [Interview with Doug Logan, Cyber Ninjas CEO](https://rumble.com/vnf092-cyber-ninjas-ceo-tells-congress-to-fork-off-and-joins-us-instead.html), Oct 7, 2021. It's a long video, and we've misplaced the timestamp, but it's probably within the 10min - 1hr segment.

[^r8]: C-Span User Clip: "[Maricopa County Admits They Deleted Files That Were Archived, Not Given in Subpoena](https://www.c-span.org/video/?c4981036/user-clip-maricopa-county-admits-deleted-files-archived-subpoena)", Oct 7, 2021. Or see [the full 3hr 45min hearing video](https://www.c-span.org/video/?515200-1/house-hearing-arizona-2020-election-audit), on C-Span.
