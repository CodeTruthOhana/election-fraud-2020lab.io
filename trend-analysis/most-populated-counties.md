---
title: Most Populated Counties Historical Results
meta_desc: List of the most populated counties and their historical results
parent_page: /trend-analysis/3-historical-county-trends/
last_updated: 20 Sep 2021
# draft_status: true
# draft_until_date: '2021-09-18'
comment_intro: Did you notice any strange trends or anomalies in the data we showed above? Let us know in the comments below.
---

Here is the list of all the most populated counties (with over 200K total votes in 2020) sorted from highest to lowest total votes.

{% include large_image url="/images/trend-analysis/most-populated-counties/MostPopulatedCounties1.png" maxWidth="1550" %}
{% include large_image url="/images/trend-analysis/most-populated-counties/MostPopulatedCounties2.png" maxWidth="1550" %}
{% include large_image url="/images/trend-analysis/most-populated-counties/MostPopulatedCounties3.png" maxWidth="1550" %}
{% include large_image url="/images/trend-analysis/most-populated-counties/MostPopulatedCounties4.png" maxWidth="1550" %}
{% include large_image url="/images/trend-analysis/most-populated-counties/MostPopulatedCounties5.png" maxWidth="1550" %}