---
title: "Case Study: Maryland Voter Registration Trends"
meta_desc: Examining the trends in voter registrations in Maryland, across several counties.
parent_page: /trend-analysis/historical-county-analysis-registration-data/
last_updated: 7 Oct 2021
draft_status: true  # link is hidden from main trend analysis page
# draft_until_date: '2021-09-29'
state: Maryland
comment_intro: Have any thoughts or feedback on the data presented above? Let us know in the comments below.
---

<div class="tip emoji" markdown="1">
📃 This case study is based on the method we described in *[How to Predict Election Results Using Registration Data](/trend-analysis/historical-county-analysis-registration-data/)*.
</div>

{% include toc %}

We'll start with an overview of all counties in Maryland. Below is the "Historical County Range Analysis" table that we covered in [Part 4](/trend-analysis/3-historical-county-trends/), which will give us some direction for which counties to analyze first:

{% include large_image url="/images/trend-analysis/MarylandCounties.png" maxWidth="1600" %}

By scanning this table, there's a few things we notice:

- 2 counties voted Democrat for the first time. They are *Frederick* and *Talbot*. (When we checked the historical records, they've voted Republican since at least 1988.)
- *Kent* also voted Democrat in 2020 for the first time since 2008.
- *Anne Arundel* has an (extremely) high Loc.20 value. It only turned Democrat in 2016.
- *Baltimore*, *Howard*, and *Charles* have very high Loc.20 values.
- *Calvert* and *Wicomico* have very low Loc.20 values.

This gives us a short-list of counties that deserve deeper inspection. Let's start with Anne Arundel.

## Anne Arundel County

In the state overview table above, Anne Arundel has an extremely high Loc.20 value. This is due to the Democrat winning margin increasing from 2.2% to 14.5% between the last two elections. This is a fairly large shift.

Back to the spreadsheet that we developed in our [last article](/trend-analysis/historical-county-analysis-registration-data/), we've populated the data for the county, over the six elections, and highlighted some of the numbers that stand out.

{% include large_image url="/images/trend-analysis/AnneArundel.png" maxWidth="1900" %}

{% include expandable_box
        title="What do the column names mean?"
        content="

Here's the column definitions again, from our previous article:

{:.small}
TV | Total Votes Counted in Election
TR | Total Registrations
RR | Republican Registrations
DR | Democrat Registrations
OR | Other Registrations
RV | Republican Votes
DV | Democrat Votes
OV | Other Votes
RRP | Republican Registrations as Percentage of Total
DRP | Democrat Registered as Percentage of Total
ORP | Unaffiliated (Other) Voters as Percentage of Total
PGap | Percentage Gap between Republican and Democrat Registration Percentages (RRP -- DRP)
RRI | Republican Registration Increase (the change from previous election)
DRI | Democrat Registration Increase (the change from previous election)
ORI | Unaffiliated (Other) Registration Increase (the change from previous election)
RVP | Republican Votes as Percentage of Total
DVP | Democrat Votes as Percentage of Total
OVP | Unaffiliated (Other) Votes as Percentage of Total
TVI | Total Vote Increase (the change from previous election)
RVI | Republican Vote Increase (the change from previous election)
DVI | Democrat Vote Increase (the change from previous election)
OVI | Unaffiliated (Other) Vote Increase (the change from previous election)
T/TR | Total Turnout (Votes) Divided by Total Registrations (TV ÷ TR)
T/RR | Republican Turnout (Votes) Divided by Republican Registrations (RV ÷ RR)
T/DR | Democrat Turnout (Votes) Divided by Democrat Registrations (DV ÷ DR)

" %}

**Observations:**

- This county used to be Republican and switched to the Democrat party in 2016. The RVP has been steadily decreasing since 2004.
- Total vote in 2020: 309,617
- Columns to pay attention to are highlighted. They are from left to right: TRI, RRI, DRI, TVI, DRI and T/DR
- TRI: High registration increase of 31,320 in 2020
- DRI: Democrats increased their registration by a record 16,903 in 2020
- RRI: Republicans only increased their registration by 1,060
- TVI: Total Vote Increase of 39,536 which is similar to the TVI in 2004
- RVI: Republicans increased their votes for the first time since 2004
- DVI: Democrats increased their votes by 44,404, which is more than twice the previous record set in 2004
- T/RR (Republican turnout): Increased slightly to 0.94 but is well below the maximum set in 2004 (of 1.11). General trend is downwards.
- T/DR (Democrat turnout): Increased substantially to 0.98 and is a new record, exceeding the previous high of 0.85 established in 2012 by more than 0.14. ⚠️  
  (Yes, that is a lot. The more you study these numbers across many counties and states, the more you acquire an understanding of what is *normal*.)

**Analysis:**

The increase of 44,404 Democrat votes is suspicious enough and is the reason why the T/DR ratio is so high.

If we were to use a typical T/DR ratio of 0.85 from previous years, we can calculate an estimate of the excess votes.

Estimate Democrat Votes (EDV):  | DR * T/DR  | = | 175,642 * 0.85     | = | 149,296
Excess Democrat Votes:          | DV -- EDV  | = | 172,823 -- 149,296 | = | **23,527**

This number is **conservative** as it does not account for the excessive new Democrat registrations in 2020 which may not all be 100% legitimate, as canvassing efforts in other states (see our [canvassing](/canvassing/) page) have shown.

The real question is: The DVI in 2012 and 2016 was respectively 1,620 and 1,784. In 2020 it jumped to 44,404. What changed? Was Biden really that much more popular? Even more popular than Obama?

Baltimore City, Howard, and Charles have very similar patterns. In some of these cases the DVI was negative in 2016, only to see a surge in 2020.


## Talbot County

This county voted Democrat for the first time in 2020.

{% include large_image url="/images/trend-analysis/Talbot.png" maxWidth="1900" %}

**Observations:**

- T/DR (Democrat turnout): Bucks the trend in 2020, with a huge surge to 1.02
- DVI: Democrat vote increase of 2,409, despite vote decreases (negative numbers) in the previous 2 elections
- DVI is more than 10 times bigger than RVI (quite incredible, especially for a county that has historically always voted Republican)
- Democrats won the county by 116 votes ⚠️

**Analysis:**

The T/DR is excessive. A more realistic number based on previous years would be 0.89 (and even that is generous).

Estimate Democrat Votes (EDV): | DR * T/DR | = | 10,808 * 0.89   | = | 9,619
Excess Democrat Votes:         | DV -- EDV | = | 11,062 -- 9,619 | = | **1,443**

If we accept this adjustment, Talbot (and Frederick) should have never turned blue in 2020.

We would start with a full forensic audit of the smaller counties to build momentum. Smaller counties are easier and faster to complete.


## Calvert County

Here we have a Republican county with a very low Loc.20 value, meaning that it voted Republican, but the winning margin was relatively low compared to previous years.

{% include large_image url="/images/trend-analysis/Calvert.png" maxWidth="1900" %}

**Observations:**

- T/DR: Set a new record in 2020 at 0.91 ⚠️
- T/RR: Dropped significantly in 2020 to 0.89 (a 0.12 drop!) ⚠️
- DVI: Democrat vote increase of 4,362, despite a strong decrease (negative) in the 2016 election.
- RVI: Is negative for the first time ever! ⚠️ This is despite the Republican registrations increasing by 2,581 in 2020.
- It's becoming more and more clear that the Democrats inflated their ballot counts in 2020. The real difference with Calvert is how aggressively the Republican votes were "trimmed".
- It is also obvious that the T/RR was "trimmed" in Anne Arundel and Talbot in 2016 and 2020. Scroll back up and look at the drop. It was just more obvious in Calvert because they only started trimming the T/RR in 2020. (And yes the negative RVI is a dead give away.)

**Analysis:**

For Calvert, let's perform an estimate for both the Republican and Democrat votes, based on the registration trends (we'll assume again for now that the voter rolls are clean, although we're seeing increasing evidence that they're not).

* The T/DR (Democrat turnout rate) is high. A more realistic number would be 0.88 (We believe 0.76 is closer to reality, but we will be generous).
* The T/RR (Republican turnout rate) is too low. A realistic value, in keeping with the trend and previous elections, would be closer to 1.01.

Estimated Democrat Votes (EDV):   | DR * T/DR | = | 24,703 * 0.88   | = | 21,739
Excess Democrat Votes:            | DV - EDV  | = | 22,587 - 21,739 | = | **848**
Estimated Republican Votes (ERV): | RR * T/RR | = | 28,398 * 1.01   | = | 28,682
Shortfall Republican Votes:       | ERV - RV  | = | 28,682 - 25,346 | = | **3,336**
**Total estimated discrepancy:**  |           |   |                 |   | **4,184**

This may not seem like a lot, but relatively speaking 4,184 represents 8.5% of all the total votes for Calvert County in 2020.

----

For further similar case studies, see: {% include other_sibling_pages %}

----

{% include article_series from="/trend-analysis/" %}
