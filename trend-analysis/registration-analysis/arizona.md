---
title: "Case Study: Arizona Voter Registration Trends"
meta_desc: Examining the trends in voter registrations in Maricopa County. We also compare these to the forensic audit findings.
parent_page: /trend-analysis/historical-county-analysis-registration-data/
last_updated: 7 Oct 2021
draft_status: true  # link is hidden from main trend analysis page
# draft_until_date: '2021-09-29'
state: Arizona
comment_intro: Have any thoughts or feedback on the data presented above? Let us know in the comments below.
---

<div class="tip emoji" markdown="1">
📃 This case study is based on the method we described in *[How to Predict Election Results Using Registration Data](/trend-analysis/historical-county-analysis-registration-data/)*.
</div>

## Maricopa County, Arizona

{:.tip}
The Maricopa County forensic audit report was presented on Fri Sept 24, 2021.  
[Read more about their findings](/in-detail/maricopa-arizona-forensic-audit-report-results/).

Maricopa is one of the largest counties in America with a total of 2,076,086 votes in 2020.

{% include large_image url="/images/trend-analysis/Maricopa.png" maxWidth="1900" %}

**Observations:**

- Maricopa county voted Democrat for the first time in 2020 in a very long time.
- Dramatic increase in DVP. (Over 5% in one election cycle...)
- Huge DVI increase in 2020. Nearly **3** times larger than the previous record set in 2004.
- T/DR: Set a new record in 2020 at 1.28

**Analysis:**

If we were to use a more realistic T/DR ratio of 1.10 (which is in line with the historical downward trend), we can estimate the Democrat excess votes.

Estimate Democrat Votes (EDV):  | DR * T/DR = | 814,343 * 1.10 =    | 895,777
Excess Democrat Votes:          | DV - EDV =  | 1,040,774 - 895,777 = | **144,997**

This number is **conservative** as it does not account for the excessive new Democrat and "Other" registrations in 2020.

In fact, we find the DVI increase of 337,867 to be extremely high. That is **219,701** more votes than the previous record set in 2008. (That is nearly 3 times more votes than the previous record!)

It is also **237,248** more votes than the 2016 DVI value.

This number is suspiciously close to the 255,326 number Doug Logan mentioned in his audit presentation. This number represents the early votes that are recorded in the VM55 final voter file, but do not have a corresponding EV33 entry (in the Early Voting Returns file. This file keeps records of the date the ballot was returned and the method. i.e. by mail or in-person).

Here is a screenshot from Doug Logan's audit presentation:

{% include large_image url="/images/trend-analysis/Slide26.png" maxWidth="1000" %}

{:.caution.emoji}
⚠️ How is it possible that vote entries end up in the final voter file (VM55), but have no corresponding entries in the EV33 file? They literally "magically" appeared in the final vote tally, and no one knows how they got there...

Furthermore, in the official Cyber Ninja [Volume III report](https://c692f527-da75-4c86-b5d1-8b3d5d4d5b43.filesusr.com/ugd/2f3470_d36cb5eaca56435d84171b4fe7ee6919.pdf), we find the following paragraphs on Corrupt Ballot Images (page 70 - 73). These paragraphs are worth reading very carefully. **The implications are startling**:

{% include large_image url="/images/trend-analysis/CorruptBallots1.png" maxWidth="1000" %}
{% include large_image url="/images/trend-analysis/CorruptBallots2.png" maxWidth="1000" %}
{% include large_image url="/images/trend-analysis/CorruptBallots3.png" maxWidth="1000" %}

This is no coincidence. The number of corrupt ballots nearly matches the number of "magic" votes, which also matches our data analysis. This represents a perfect triangulation of **independent** observations (also known as a "[Nomological network](https://en.wikipedia.org/wiki/Nomological_network)") all pointing to the same conclusion... **There are over 250K votes and ballots that are unaccounted for!**

{:.caution.emoji}
⚠️ As an aside, this would also suggest the formula we use for calculating excess votes from the T/DR value is **too lenient**.

----

For further similar case studies, see: {% include other_sibling_pages %}

----

{% include article_series from="/trend-analysis/" %}
