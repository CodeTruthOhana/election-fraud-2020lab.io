---
title: Styling Cheatsheet
comments_enabled: false
wide_layout: true
examples: |

    Here | is
    a | table

    And here | is another
    ---------|-----------
    table    | with
    a        | heading row

    ......

    {:.info}
    This syntax works when there's just a single paragraph to go in the box.
    
    <div class="info" markdown="1">
    This syntax is needed when there's *multiple* paragraphs.

    Like this.
    </div>
    
    ......
    
    {:.tip}
    Tips are similar to info, but a bit smaller, so as to not draw so much attention. This syntax works when there's just a single paragraph to go in the box.
    
    <div class="tip" markdown="1">
    This syntax is needed when there's *multiple* paragraphs.

    Like this.
    </div>
    
    ......
    
    {:.tip.emoji}
    💡 **TIP:**  
    You can include an emoji symbol as the first character, but be sure to give add `.emoji` to make the icon larger and give it proper spacing. You also need to include *two spaces* at the end of the first line to force this next line underneath.
    
    <div class="tip emoji" markdown="1">
    💡 **TIP:**      
    This syntax is needed when there's *multiple* paragraphs.

    Like this.
    </div>
    
    ......
    
    {:.important.emoji}
    ⚠️ **IMPORTANT!**  
    Important is bold --- to stand out to the user! This syntax works when there's just a single paragraph to go in the box. Don't forget two spaces after those asterisks.
    
    <div class="important emoji" markdown="1">
    🤯 This syntax is needed when there's *multiple* paragraphs.

    Like this.
    </div>
    
    ......
    
    {:.caution.emoji}
    ⚠️ This syntax works when there's just a single paragraph to go in the box.
    
    <div class="caution" markdown="1">
    This syntax is needed when there's *multiple* paragraphs.

    Like this.
    </div>
    
    ......
    
    {:.hint}
    This syntax works when there's just a single paragraph to go in the box.
    
    <div class="hint emoji" markdown="1">
    😉 This syntax is needed when there's *multiple* paragraphs.

    Like this.
    </div>
    
    ......
    
    > This is used for quotes. All quotes turn 'curly' by default --- not just in these boxes, but everywhere on the site.
    > 
    > You can optionally add a signature to the quote, like the line below."
    >
    > <small>--- George Washington</small>
    
    ......
    
    {:.highlight}
    > This is useful for BIG important text that needs to stand out, but is not actually a quote.
    >
    > It can also have multiple lines.

    ......

    Here is a paragraph with a footnote. It gets added to the very bottom of the page. [^7]

    [^7]: Here is the text of my footnote. It can be placed anywhere in the document. Regardless of the number used, they will always start from 1 from the viewers perspective.

---

For Markdown basics, see the brief [cheat sheet](https://www.markdownguide.org/cheat-sheet/), or the more comprehensive [guide](https://www.markdownguide.org/basic-syntax/). I haven't covered these again here.

<style>
    pre {white-space: break-spaces;}
    .inner > table > tbody > tr > td {border-top: 0}
</style>

{% assign examples = page.examples | split: "......" %}

<table>
<tr>
    <th>Source Code</th>
    <th>Result</th>
</tr>

{% for e in examples %}
<tr><td><pre>{{ e | strip | escape }}</pre></td><td markdown="1">{{ e }}</td></tr>
{% endfor %}

<tr><td><pre>{% raw %}{% include expandable_box 
title="Show me the details"
content="
This can have several paragraphs, bullets, numbers, etc., even tables.

1. Numbers
2. Like
3. This

Be sure that \"quotes\" are escaped with a slash.
" %}{% endraw %}</pre></td><td markdown="1">{% include expandable_box
    title="Show me the details"
    content="
This can have several paragraphs, bullets, numbers, etc.

1. Numbers
2. Like
3. This

Be sure that \"quotes\" are escaped with a slash.
" %}</td></tr>

</table>