---
title: What is a Voting System Test Laboratory (VSTL)?
last_updated: 8 Oct 2021
comments_enabled: true
---

A Voting System Test Laboratory (VSTL) is a private company, accredited by the federal Election Assistance Commission (EAC), and contracted by an election equipment manufacturer (such as [Dominion](/in-detail/dominion-voting-machines/), ES&S, HART, Smartmatic, etc.) to audit their machines and certify whether or not they pass the EAC standards.


### Accredited Companies

* Pro V&V

* SLI Compliance


### Significant Concerns

* Pro V&V did not have active accreditation when it certified Dominion machines for use in the 2020 election. [See the Colorado report](/fraud-summary-by-state/colorado/#use-of-non-certified-voting-machines).

* Maricopa County in Arizona chose to use two VSTL firms, Pro V&V and SLI Compliance to do an internal audit of their 2020 election results, despite objections from the state Senate. [See the Arizona report](/fraud-summary-by-state/arizona/#1-maricopa-county-internal-audit). The findings did little to secure election integrity, and the Senate proceeded with their full forensic audit.

* Equipment manufacturers may choose which VSTL they use when applying for certification

* Equipment manufacturers pay the VSTL *directly*, which creates a conflict of interest. If a VSTL's income comes from the equipment companies, it's quite possible for them to apply special favor to their client in order to retain the client and stay in business. 

  - Pro V&V openly stated on their website that their focus is *their clients* --- not the people, not the state, nor the government that they're certifying for, but their paying customers.

* There are no regulations on the price charged by the VSTL for certification

The following video goes into much greater detail about the concerns of this arrangement:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vku86j/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>



### References

* [EAC Website › Voting System Test Laboratories (VSTL)](https://www.eac.gov/voting-equipment/voting-system-test-laboratories-vstl)

* [State Requirements and the Federal Voting System Testing and Certification Program](https://www.eac.gov/sites/default/files/eac_assets/1/1/State%20Requirements%20and%20the%20Federal%20Voting%20System%20Testing%20and%20Certification%20Program.pdf)

* [VSTL manual from EAC](https://www.eac.gov/sites/default/files/eac_assets/1/28/VSTLManual%207%208%2015%20FINAL.pdf) --- the accreditation requirements are particularly relevant. One needs to refer to this version (*not* the new version approved in February 2021) as version 2.0 was pertinent to 2018 and 2020 elections.
