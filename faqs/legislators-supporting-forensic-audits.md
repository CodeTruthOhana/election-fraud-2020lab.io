---
title: Which state legislators support forensic audits and election integrity?
last_updated: 24 Nov 2021
comments_enabled: true
---

{% assign reps = site.data.supportiveStateReps %}
{% assign numReps = reps | size %}

Following the results of the Maricopa Arizona Forensic Audit, {{ site.data.supportiveStateReps | size }} state representatives from {{ site.data.supportiveStateReps | group_by: 'State' | size }} states [signed a letter](https://wendyrogers.org/wp-content/uploads/2021/10/MFR-Election-Integrity-Statement-v2.pdf) led by Arizona Representative Wendy Rogers asking for forensic audits in all 50 states. 

Here's the list of signatories, including some who signed later, after the original memo.

<table>
{% assign states = site.states | sort: 'state' %}
{% for state in states %}
    <tr>
        <th><a href="/fraud-summary-by-state/{{ state.state | slugify}}/">{{ state.state }}</a></th>
        <td>
            {% assign repsForState = reps | where: 'State', state.state %}
            <ul>
                {% for person in repsForState %}
                <li>{{ person.Name }}</li>
                {% endfor %}
            </ul>
            {% assign numReps = repsForState | size %}
            {% if numReps == 0 %}
            <ul class="muted"><li>None as yet</li></ul>
            {% endif %}
        </td>
    </tr>
{% endfor %}
</table>

Although the number of those who have signed is growing, it is still only a tiny percentage of all state legislators in the U.S. As of Sept 30, 2021, there were 7,383 state legislative seats across the country --- the majority of which are Republican, according to [Ballotpedia](https://ballotpedia.org/Partisan_composition_of_state_houses).


<style>
    td ul {
        margin-bottom: 0;
    }
</style>