---
title: Why do people believe the 2020 election was rigged?
faq_weight: 100
last_updated: 11 Jul 2021
---

This website outlines most of the major anomalies, irregularities and claims of fraud during the November 2020 election. We suggest you explore the pages of this site to learn more. 

But in summary, the trust of many Americans in the government and election results was deeply damaged by several key factors:

1. How the entrenched bureaucracy and national security departments subverted Trump from day-one of his presidency

2. That the press often colluded in this

3. That election rules were changed  
(in some cases mere days or weeks prior to the 2020 election)

4. That Big Tech censored any vocal opposition

5. That political violence was legitimized and encouraged

6. That Trump was banned from social media

Political commentator Tucker Carlson explained these broader issues well, and how they fuelled the increasing levels of mistrust:

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vh1cgt/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

<small>Or [View Video on Twitter](https://twitter.com/TPostMillennial/status/1413880551258443777){:target="_blank"}</small>

*Video transcript, including Twitter thread:* 

It wasn't just the way votes were counted or the voting machines that shook people's faith in our democracy, it was the preceding four years and how the ruling class behaved during those four years. Darryl Cooper wrote a remarkable series of tweets in which he tried to explain why so many Trump voters believe the last election was rigged. He really crystalized it. We'd like to read some of it now:


{:.small}
> Here are the facts - actual, confirmed facts - that shape their perspective:
>
> The <abbr title="Federal Bureau of Investigation">FBI</abbr>/etc spied on the 2016 Trump campaign using evidence manufactured by the <abbr title="Hillary Clinton, who competed against Trump in the 2016 election">Clinton</abbr> campaign. We now know that all involved knew it was fake from Day 1 (see: Brennan's July 2016 memo, etc). 
>
> These are <abbr title="Referring to people who prefer a conservative type of government">Tea Party people</abbr>. The types who give their kids a pocket Constitution for their birthday and have Founding Fathers memes in their bios. The intel community spying on a presidential campaign using fake evidence (incl forged documents) is a big deal to them. 
>
> Everyone involved lied about their involvement as long as they could. We only learned the <abbr title="Democratic Party">DNC</abbr> paid for the manufactured evidence because of a court order. Comey denied on TV knowing the DNC paid for it, when we have emails from a year earlier proving that he knew. 
>
> This was true with everyone, from CIA Director Brennan & Adam Schiff --- who were on TV saying they'd seen clear evidence of collusion with Russia, while admitting under oath behind closed doors that they hadn't --- all the way down the line. In the end we learned that it was ALL fake. 
>
> At first, many Trump people were worried there must be some collusion, because every media & intel agency wouldn't make it up out of nothing. When it was clear that they had made it up, people expected a reckoning, and shed many illusions about their government when it didn't happen. 
>
> We know as fact: 
> 
> 1. The Steele dossier was the sole evidence used to justify spying on the Trump campaign, 
> 2. The FBI knew the Steele dossier was a <abbr title="Democratic Party">DNC</abbr> op, 
> 3. Steele's source told the <abbr title="Federal Bureau of Investigation">FBI</abbr> the info was unserious, 
> 4. they did not inform the court of any of this and kept spying. 
>
> Trump supporters know the collusion case front and back. They went from worrying the collusion must be real, to suspecting it might be fake, to realizing it was a scam, then watched as every institution --- agencies, the press, Congress, academia --- gaslit them for another year. 
>
> Worse, collusion was used to scare people away from working in the administration. They knew their entire lives would be investigated. Many quit because they were being bankrupted by legal fees. The <abbr title="Department of Justice">DoJ</abbr>, press, and government destroyed lives and actively subverted an elected administration. 
>
> This is where people whose political identity was largely defined by a naive belief in what they learned in Civics class began to see the outline of a Regime that crossed all institutional boundaries. Because it had stepped out of the shadows to unite against an interloper. 
>
> GOP propaganda still has many of them thinking in terms of partisan binaries, but A LOT of Trump supporters see that the Regime is not partisan. They all know that the same institutions would have taken opposite sides if it was a Tulsi Gabbard vs Jeb Bush election. 
>
> It's hard to describe to people on the left (who are used to thinking of government as a conspiracy... Watergate, COINTELPRO, WMD, etc) how shocking and disillusioning this was for people who encourage their sons to enlist in the Army, and hate people who don't stand for the Anthem. 
>
> They could have managed the shock if it only involved the government. But the behavior of the corporate press is really what radicalized them. They hate journalists more than they hate any politician or government official, because they feel most betrayed by them. 
>
> The idea that the press is driven by ratings/sensationalism became untenable. If that were true, they'd be all over the Epstein story. The corporate press is the propaganda arm of the Regime they now see in outline. Nothing anyone says will ever make them unsee that, period. 
>
> This is profoundly disorienting. Many of them don't know for certain whether ballots were faked in November 2020, but they know for absolute certain that the press, the FBI, etc would lie to them if they were. They have every reason to believe that, and it's probably true. 
>
> They watched the press behave like animals for four years. Tens of millions of people will always see Kavanaugh as a gang rapist, based on nothing, because of CNN. And CNN seems proud of that. They led a lynch mob against a high school kid. They cheered on a summer of riots. 
>
> They always claimed the media had liberal bias, fine, whatever. They still thought the press would admit truth if they were cornered. Now they don't. It's a different thing to watch them invent stories whole cloth in order to destroy regular lives and spark mass violence. 
>
> Time Mag told us that during the 2020 riots, there were weekly conference calls involving, among others, leaders of the protests, the local officials who refused to stop them, and media people who framed them for political effect. In Ukraine we call that a color revolution. 
>
> Throughout the summer, Democrat governors took advantage of COVID to change voting procedures. It wasn't just the mail-ins (they lowered signature matching standards, etc). After the collusion scam, the fake impeachment, Trump people expected shenanigans by now. 
>
> Re: "fake impeachment", we now know that Trump's request for Ukraine to cooperate w/the DOJ regarding Biden's $ activities in Ukraine was in support of an active investigation being pursued by the FBI and Ukraine AG at the time, and thus a completely legitimate request. 
>
> Then you get the Hunter laptop scandal. Big Tech ran a full-on censorship campaign against a major newspaper to protect a political candidate. Period. Everyone knows it, all of the Tech companies now admit it was a "mistake ...but, ya know, the election's over, so who cares?"
>
> [It] goes without saying, but: If the NY Times had Donald Trump Jr's laptop, full of pics of him smoking crack and engaging in group sex, lots of lurid family drama, emails describing direct corruption and backed up by the CEO of the company they were using, the NYT wouldn't have been banned. 
>
> Think back: Stories about Trump being pissed on by Russian prostitutes and blackmailed by Putin were promoted as fact, and the only evidence was a document paid for by his opposition and disavowed by its source. The NY Post was banned for reporting on true information. 
>
> The reaction of Trump people to all this was not, "no fair!" That's how they felt about Romney's "binders of women" in 2012. This is different. Now they see, correctly, that every institution is captured by people who will use any means to exclude them from the political process. 
>
> And yet they showed up in record numbers to vote. He got 13 million more votes than in 2016, 10 million more than Clinton got! As election night dragged on, they allowed themselves some hope. But when the four critical swing states (and only those states) went dark at midnight, they knew. 
>
> Over the ensuing weeks, they got shuffled around by grifters (swindlers) and media scam artists selling them conspiracy theories. They latched onto one, then another increasingly absurd theory as they tried to put a concrete name on something *very real*. 
>
> The media and Big Tech did everything to make things worse. Everything about the election was strange --- the changes to procedure, unprecedented mail-in voting, the delays, etc --- but rather than admit that and make everything transparent, they banned discussion of it (even in <abbr title="Direct/private messages, used on social networks">DMs</abbr>!). 
>
> Everyone knows that, just as Donald Trump Jr's laptop would've been the story of the century, if everything about the election dispute was the same, except the parties were reversed, suspicions about the outcome would've been *Taken Very Seriously*. See 2016 for proof. 
>
> Even the courts' refusal of the case gets nowhere with them, because of how the opposition embraced mass political violence. They'll say, with good reason: What judge will stick his neck out for Trump knowing he'll be destroyed in the media as a violent mob burns down his house? 
>
> It's a fact, according to Time Magazine, that mass riots were planned in cities across the country if Trump won. Sure, they were "protests", but they were planned by the same people as during the summer, and everyone knows what it would have meant. Judges have families, too. 
>
> Forget the ballot conspiracies. It's a fact that governors used COVID to unconstitutionally alter election procedures (the Constitution states that only legislatures can do so) to help Biden to make up for a massive enthusiasm gap by gaming the mail-in ballot system. 
>
> They [the election manipulators] knew it was unconstitutional, it's right there in plain English. But they knew the cases wouldn't see court until after the election. And what judge will toss millions of ballots because a governor broke the rules? The threat of mass riots wasn't implied, it was direct. 
>
> 1. The entrenched bureaucracy & security state subverted Trump from Day 1, 
> 1. The press is part of the operation, 
> 1. Election rules were changed, 
> 1. Big Tech censors opposition, 
> 1. Political violence is legitimized & encouraged, 
> 1. Trump is banned from social media. 
>
> They were led down some rabbit holes, but they are absolutely right that their government is monopolized by a Regime that believes they are beneath representation, and will observe no limits to keep them getting it. Trump fans should be happy he lost; it might've kept him alive.

{:.caption}
Source: [Twitter thread by @martyrmade](https://threadreaderapp.com/thread/1413165168956088321.html){:target="_blank"} ([original Tweets here](https://twitter.com/martyrmade/status/1413165168956088321){:target="_blank"})