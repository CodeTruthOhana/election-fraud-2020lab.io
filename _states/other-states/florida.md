---
title: Florida
heading: Election Fraud in Florida
state: Florida
abbr: fl
last_updated: 23 May 2023
---


{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=nil
    registrationsOver100=false
    extra1=nil
    exampleCounty=nil
    exampleDate=nil
%}

{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

The list of counties that used Dominion vs ES&S machines can be found in [this PDF](https://img1.wsimg.com/blobby/go/5e6e749c-0ce3-4cba-8ecd-6366b9c7fdfd/voting-systems-in-use-by-county-4publication-w.pdf). We believe similar issues also occurred with ES&S.


## Canvassing Results

{% include canvass/florida %}

{% include canvass/link %}


## AuditEngine

In late 2021, AuditEngine released findings on their digital audit of ballot images across three counties in Florida. We haven't yet been able to review their detailed results, but they may be of interest to researchers.

[Visit AuditEngine.org](https://auditengine.org){:.button}


{% include zuckerberg_interference.html %}

{% include trend-analysis-summary %}


## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Florida

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which includes a section on {{ page.state }}

* ElectionFraud20.org has also reviewed the voting and registration trends from 2000 to 2020 and noted several anomalies in both the voter rolls and the ballot counts in Brevard County, Florida, among others. [Read our Florida Case Study](/trend-analysis/registration-analysis/florida/) and the preceding articles that explain the method and the observations.


{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}


### Footnotes & References
