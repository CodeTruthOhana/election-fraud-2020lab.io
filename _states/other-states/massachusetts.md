---
title: Massachusetts
heading: Election Fraud in Massachusetts
state: Massachusetts
abbr: ma
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Massachusetts as the state with the 2nd highest number of unexpected Biden votes

* Dr. Shiva Ayyadurai ran for a Massachusetts US Senate seat in 2020, and believes he witnessed fraud in the process. He also describes the retaliation directed at him after he questioned the State Election Director of Massachusetts as to why Massachusetts had violated 52 USC 20701 by deleting ballot images. See his [response to Maricopa County](https://www.scribd.com/document/530454189/Dr-Shiva-Replies-To-Maricopa-County-Officials) (p.22). He later announced his plan to run for Governor of Massachusetts. [^1]

{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

Dr. Shiva Ayyadurai, an election integrity advocate and key auditor in the [Maricopa County Forensic Audit](/in-detail/maricopa-arizona-forensic-audit-report/), announced on Dec 3, 2021 that he is running for Governor of Massachusetts, pushing for a strong emphasis on election integrity and his pillars of *Truth, Freedom & Health*, and is also keen to assist other interested candidates with similar values run for office in their region. [^1]

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}


### Footnotes & References

[^1]: YouTube: "[Dr.SHIVA LIVE: I Am Running For Governor - Workers UNITE!](https://www.youtube.com/watch?v=B13csdEcE9w)", Dec 3, 2021. [Alternate link](https://t.me/LibertyOverwatchChannel/6074).