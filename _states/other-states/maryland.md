---
title: Maryland
heading: Election Fraud in Maryland
state: Maryland
abbr: md
last_updated: 4 May 2022
---

{% include state_stub_1 %}

## Tally Discrepancies

In a number of states, the tallies of total votes have not matched the number of voters who voted, as per the voter rolls. This was the case in Maryland with the State Board of Elections reporting **2,961,437 votes cast**, yet **3,038,031 voters were recorded as having voted** according to city and county voter rolls.

*What caused the discrepancy of 76,594 votes (2.5%)?*

On Jan 26, 2022 the officers of the Baltimore City Republican Central Committee (BCRCC) unanimously called for a full forensic and independent audit of the 2020 election saying simply "the numbers don’t add up":

![Maryland: Call for Forensic Audit](/images/maryland/gop-press-release.jpg)

{:.caption}
Source: [EIN Press Wire](https://www.einpresswire.com/article/561576753/call-to-action-request-to-governor-larry-hogan-baltimore-city-republican-party-requests-audit-of-2020-maryland-election), Jan 26, 2022



{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

{% include trend-analysis-summary %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Maryland as the state with the 5th highest number of unexpected Biden votes

* ElectionFraud20.org has also reviewed the voting and registration trends from 2000 to 2020 and noted several anomalies in both the voter rolls and the ballot counts in Maryland, among others. [Read our Maryland Case Study](/trend-analysis/registration-analysis/maryland/) and the preceding articles that explain the method and the observations.

{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
