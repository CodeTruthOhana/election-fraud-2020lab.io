---
title: North Carolina
heading: Election Fraud in North Carolina
state: North Carolina
abbr: nc
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

## Statement from Lynn Bernstein, Founder, Transparent Elections NC

{:.small}
> For the 2020 primary election, I was the lead observer for the Wake County Democratic Party and was deeply troubled by what I observed; a lack of transparency, lax security measures, no bi-partisan chain of custody of some critical election materials, election results being prematurely revealed, no legislative oversight, illegal processing of absentee ballots, weak post election (self) audits and the list goes on. After going through every oversight channel I could think of and getting nowhere, I went public about what I saw and was swiftly fired from being an observer for my party — I do not regret being a whistleblower.
>
> "My observations for the 2020 Primary election do not include my county’s processes on election night because the Wake Election Director would not allow the public to enter the building despite there being a quorum of Board of Elections members present on election night. We were threatened with arrest if we did not leave the property immediately! This is another egregious violation of state law.
> 
> "NC continues to engage in illegal and ill-advised practices and will continue doing so unless the public starts to take notice and demand meaningful observation and evidence. Many of the Election Assistance Commission (EAC) [^L2] and National Conference of State Legislators (NCSL) [^L3] recommended best practices are not followed by counties and should be explicitly mandated by NC law. A failure to mandate adherence puts the state and counties in legal jeopardy, as candidates could convincingly claim that there was ample opportunity for fraud.
>
> "Finally, NC must stop violating Federal law by destroying ballot images. [^L4] The ballot images, cast vote records, and list of voter records (along with the metadata) should be released to the public along with all other election evidence. Our democracy depends on a transparent, secure, and publicly verified process."
>
> <small>Source: [Letter to Seth Keshel dated Sep 16, 2021](/assets/pdf/north-carolina/Redacted-Copy-of-Letter-to-Seth-Keshel-9_16_21-HandDelivered.pdf)</small>

Transparent Elections NC is a non-partisan grassroots group of volunteers who are making elections better in NC by working with elections officials to ensure that elections are transparent, secure, robustly audited, and publicly verified. Find out how you can get involved via [their website](https://www.transparentelectionsnc.org/) or [Telegram channel](https://t.me/TransparentElections_NC).

{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=0.994
    extra1=nil
    registrationsOver100=true
    exampleCounty=nil
    exampleDate=nil
%}

{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

### Barcode Transparency Issues

Voters in Mecklenburg, Cherokee, Jackson, Davidson, Davie, Warren, and Perquimans Counties are not allowed to vote using a hand marked paper ballot (HMPB), but are forced to use an ExpressVote machine to cast a vote, which prints the vote in English and also into a barcode. The [DS200](https://verifiedvoting.org/election-system/ess-ds200/) and [DS450/DS850](https://verifiedvoting.org/election-system/ess-ds850-ds450/) machines only count the barcode, completely ignoring the English text. [^6]

If an attacker manipulated one of these machines to print a false barcode, the voter would have no way of telling that their vote was being miscounted.


### Use of Uncertified Machines

The NC State Board's [website on Voting Systems](https://www.ncsbe.gov/voting/voting-equipment) states, *"All voting systems used in North Carolina are certified by the federal Election Assistance Commission (EAC) and the State Board of Elections after undergoing mandatory testing by nationally accredited laboratories. All systems used in North Carolina have been tested, used, and audited in other states."*

This was not actually followed in 2020. [^5]

The state of NC themselves certified an ES&S system, version 3.4.1.1, but this system was never actually certified by the EAC. The EAC *did* certify 3.4.1.0, but not the updated version. The DS200s machines for that system have *modems* which would allow for internet connectivity, in breach of law. [^5]

This is the version that Wake County and many other counties in NC are still using. When counties purchased DS200s years ago, they came with wired modems. It was previously legal to send results via modem, but has since been banned. [^5]

Jerome Lovato, (now former) Director for Voting System Testing and Certification at the EAC [confirmed via email](https://t.me/TransparentElections_NC/22) that version 3.4.1.1 is *not EAC Certified*.

[Learn more about the DS200 ballot scanning device](https://verifiedvoting.org/election-system/ess-ds200/)


{% include zuckerberg_interference.html %}


## Weakened Voter ID Laws

On Sept 17, 2021, two North Carolina judges struck down a law that required photo identification to vote, saying the measure “was enacted with the unconstitutional intent to discriminate against African American voters.” [^1] This is despite a Rasmussen poll in March that found that 75% of US voters believe ID should be required, with only 21% opposed. [^2]

Ballotpedia has [an article](https://ballotpedia.org/Arguments_for_and_against_voter_identification_laws) covering the arguments for and against, including which states require ID.


## Suspicious Modification of Voter Roll Databases

Voter roll databases are "snapshotted" at each election and the data made available to the public for review and verification, see [here](https://www.ncsbe.gov/results-data/voter-registration-data#historical-data) and [here](https://dl.ncsbe.gov/index.html?prefix=data/Snapshots/). While all other voter rolls from the last 3-4 years have a file modification date within a few days of the snapshot, the 2020 voter roll data file was modified 155 days after the supposed snapshot, raising questions about what was modified in this file and why.

![North Carolina suspicious voter roll modification date](/images/north-carolina/voter-roll-dates.gif)

The above information was released on our website Dec 10, 2021. Approximately seven weeks later, around Jan 26-29, 2022, *all the historical voter roll snapshots were modified*:

![North Carolina suspicious voter roll modification date](/images/north-carolina/snapshot-comparison-optim.png)

Many of these snapshots now have a different file-size proving that the contents were changed. We find this highly suspicious and concerning that historical data is being manipulated.

Research into the NC voter rolls is continuing. If you have any further information, please [contact us](/trend-analysis/inquiries/).


## How to Check Your Voter Registration Record

Residents of North Carolina can use the [Phantom Voter Project](http://vrcleaner.voterintegrityproject.com/VRCleaner/VRCleaner01.php) website to search for their own address and confirm whether there are any anomalies, such as whether additional voters are registered to the address.


## Other Reports

* US Congresswoman Sandy Smith (NC-1) has written a detailed article "[North Carolina Needs an Audit and Here’s Why](https://bigleaguepolitics.com/north-carolina-needs-an-audit-and-heres-why/)" which delves further into the irregularities that need to be investigated in the state. You can also [follow her on Twitter](https://twitter.com/SandySmithNC).

* Unlike the mainstream parties, the Libertarian Party of NC is stepping out and requesting a source code review of our electronic voting systems. NC legislators gave party chairs this authority because they wanted these independent source code reviews done. Joe Garcia is a former NYPD computer forensics detective and decided to do this because an independent source code review has never been done on ES&S's system and he wants to do his part for election security. See [this tweet](https://twitter.com/bernstein_lynn/status/1473359897068670978?s=20) from Lynn Bernstein, Transparent Elections NC.


{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References

[^1]: The Epoch Times: "[North Carolina Judges Strike Down Voter ID Law, Claiming It’s Racist](https://www.theepochtimes.com/mkt_morningbrief/north-carolina-judges-strike-down-voter-id-law-claiming-its-racist_4003810.html)", Sep 18, 2021

[^2]: Rasmussen Reports: "[75% Support Voter ID Laws](https://www.rasmussenreports.com/public_content/lifestyle/general_lifestyle/march_2021/75_support_voter_id_laws)", Mar 17, 2021

[^L2]: Election Assistance Commission: [6 TIPS For Conducting Election Audits](https://www.eac.gov/sites/default/files/eac_assets/1/6/EAC_6TipsForConductingElectionAudits_508_HiRes.pdf)

[^L3]: National Conference of State Legislators: Election Security: [State Policies 8/2/2019](https://www.ncsl.org/research/elections-and-campaigns/election-security-state-policies.aspx)

[^L4]: [Justice Department Guidance: July 28, 2021](https://www.justice.gov/opa/pr/justice-department-issues-guidance-federal-statutes-regarding-voting-methods-and-post)

[^5]: According to Transparent Elections NC: [Telegram post](https://t.me/TransparentElections_NC/19), Oct 1, 2021. [This PDF](https://www.scribd.com/document/529671094/ES-S-Election-Machines-3-4-1-0-vs-3-4-1-1) ([alternate link](https://t.me/TransparentElections_NC/20)) explains the difference between 3.4.1.0 vs 3.4.1.1: "The [ES&S] Unity 3411 is a modification of the Unity 3410, to allow for analog landline modeming of unofficial election night results."

[^6]: Transparent Elections NC: [Telegram post](https://t.me/TransparentElections_NC/23), Oct 2, 2021.