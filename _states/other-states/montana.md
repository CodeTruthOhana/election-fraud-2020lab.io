---
title: Montana
heading: Election Fraud in Montana
state: Montana
abbr: mt
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

## Missoula County: Missing Envelopes & Election Mismanagement

A group of locals formed the *Missoula County Election Integrity Project* to investigate irregularities in the Nov 2020 election. A team of twenty people conducted a hand recount of signature envelopes and audited other aspects of the election.

They discovered that 4,592 ballots (6.33%) cast in Missoula County in the 2020 General Election did not have matching signature envelopes. Based on [state law](https://leg.mt.gov/bills/mca/title_0130/chapter_0130/part_0020/section_0410/0130-0130-0020-0410.html) it is *illegal* to accept these votes, yet so far no one has been prosecuted.

When asked by Attorney Quentin Rhoades, a lawyer for the audit group, what accounted for the discrepancies, Missoula County Elections Administrator Bradley Seaman reportedly *“appeared extremely nervous and had no explanation”*, according to a RealClear Investigations [report](https://www.realclearinvestigations.com/articles/2021/03/24/a_river_of_doubt_runs_through_mail_voting_in_big_sky_country_769321.html) filed by investigative reporter John R. Lott.

A detailed report released by the group on Oct 19, 2021, shown below, states that:

> The outcome of the investigation suggests strongly that the Elections Office either suffers from fundamental incompetence or has been derelict in its duties to maintain up to date and accurate voter rolls and electronic records. Whether accidentally or on purpose, the result is a padding of invalid voter registration, providing ample room for mischief by wrong-doers and doubt in the public as to election integrity. At best, the Elections Office has not performed well as a steward of the voter rolls."

<iframe class="scribd_iframe_embed" title="Montana Election Synthesis With Exhibits 2021-10-20" src="https://www.scribd.com/embeds/544926668/content?start_page=1&view_mode=scroll&access_key=key-6LgTF2qGEzIBnQ99Q0cp" tabindex="0" data-auto-height="true" data-aspect-ratio="0.7729220222793488" scrolling="no" width="100%" height="700" frameborder="0"></iframe>

The [Montana Election Integrity Project](https://www.mtelectionintegrity.com/news/missoula-county-needs-a-full-forensic-audit-of-november-2020-election) has further coverage on Missoula County.


## Montana Election Integrity Project

Realising that the election irregularities extended beyond Missoula County, the *[Montana Election Integrity Project](https://www.mtelectionintegrity.com/)* formed to investigate the entire state. They have detailed coverage on their website of the irregularities and anomalies in the 2020 General Election, with contributions from [Seth Keshel](/seth-keshel-reports/) and [Dr. Douglas Frank](/dr-frank-reports/) (see below), helping determine precisely where the anomalies occurred.

On Sept 22, 2021, they held an event *Take Back Montana: 2020 Election Analysis* which featured Seth Keshel, Professor David Clements, and Dr. Douglas Frank, presenting their latest findings and election analysis. The three hours of presentations is captured in the video below.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vkbk6c/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.small.muted} 
[Watch on Rumble](https://rumble.com/vmxqb2-take-back-montana-2020-election-analysis.html). The [previous version](https://www.youtube.com/watch?v=Fc8E3_Qvl6o) hosted on YouTube was subsequently censored.


{% include seth_state_page_summary %}

{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

{% include dr-frank/state-summary
    correlation=0.996
    registrationsOver100=true
    extra1="Dr. Frank shared his analysis of all 56 Montana counties at the Montana Election Integrity livestream and in-person event mentioned above, [Take Back Montana: 2020 Election Analysis](https://www.youtube.com/watch?v=Fc8E3_Qvl6o) (from 1hr 53min mark)."
    exampleCounty="Missoula"
    exampleDate="September 21, 2021"
%}

{% include zuckerberg_interference.html %}

{% include trend-analysis-summary %}

{% include canvass/get-involved %}

{% include news %}


## Reporting Election Law Violations

MT Secretary of State Christi Jacobsen has [a webpage here](https://sosmt.gov/elections/fairelections/) that explains how to report election law violations. We have not utilized this channel, but would be interested in hearing from anyone who has. Was it successful? Let us know in the comments below.


{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References

