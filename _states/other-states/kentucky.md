---
title: Kentucky
heading: Election Fraud in Kentucky
state: Kentucky
abbr: ky
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=nil
    registrationsOver100=true
    extra1=nil
    exampleCounty=nil
    exampleDate=nil

    suffix="
He also reports that:

  * **Large numbers of *voters are missing* from the registration rolls --- officials cannot tell you exactly who voted**

  * **In four counties, there were *more votes* for president than there were ballots**

  * **Kentucky registration rolls appear to be *centrally manipulated***
"
%}

{% include bloated_voter_rolls %}

[Learn more about {{ page.state }}'s Voter Rolls](/dr-frank-reports/{{ page.state | slugify }}/){:.button}


{% include electronic_voting_machines %}

{% include trend-analysis-summary %}

{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

Dr. Douglas Frank reports that he had significant trouble getting access to the specific 2020 election data he needed for his analysis, despite both parties having access to it:

  *"Hmmm..... What does that tell you about the parties? If they aren't helping, they are complicit. What is sad and shocking to me is how many in the establishment know what is going on, but look the other way. They expect us... the citizens... to do their work."*  
  
  {:.caption}
  Source: [Telegram post](https://t.me/FollowTheData/950), Oct 3, 2021



### Footnotes & References
