---
title: Idaho
heading: Election Fraud in Idaho
state: Idaho
abbr: id
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=nil
    registrationsOver100=false
    extra1=nil
    exampleCounty=nil
    exampleDate=nil
%}

{% include electronic_voting_machines %}

{% include trend-analysis-summary %}

<div class="info" markdown="1">
## Get Involved 

Election Integrity Idaho is a grass roots, volunteer organization helping locals get involved in auditing the 2020 election and securing future ones. Find further information about the election in Idaho on their website.

[Visit ElectionIntegrityIdaho.org](https://electionintegrityidaho.org/){:.button}
[Telegram Channel](https://t.me/ElectionIntegrityIdaho){:.button}
</div>

<!--
{% include canvass/get-involved %}
-->

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
