---
title: Utah
heading: Election Fraud in Utah
state: Utah
abbr: ut
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=nil
    registrationsOver100=false
    extra1=nil
    exampleCounty=nil
    exampleDate=nil
%}

{% include electronic_voting_machines %}


## Grassroots Efforts

Professor David Clements interviewed Jenn Orton and Sophie Anderson about the grassroots efforts to improve elections in the state, despite intimidation from the FBI.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vr81qt/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vtu7uf-utah-corruption-governor-threatens-citizens-over-election-integrity.html). Published Feb 1, 2022.


Numerous researchers have made requests to election officials for copies of election data including voting machine logs and databases, however officials have declined these requests under claims that these are not "public records". [See their response](https://electionfraud20.org/publink/show?code=XZ5lTpZb6YSRa88OlpUSI74yAVkWReOsoxk).


{% include trend-analysis-summary %}

{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
