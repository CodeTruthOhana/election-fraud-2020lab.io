---
title: New Mexico
heading: Election Fraud in New Mexico
state: New Mexico
abbr: nm
last_updated: 4 May 2022
---

{% include state_stub_1 %}



<!-- 
THIS CASE SEEMS TO HAVE BEEN WITHDRAWN. Probably not hugely relevant. 

   - Late (and illegal) changes to ballot collection methods


*From The American Spectator's article "[Election 2020: Can Classic Fraud Evidence Save The Day?](https://spectator.org/election-2020-can-classic-fraud-evidence-save-the-day/)":*

On December 14 Trump attorneys filed a complaint in federal district court against New Mexico election officials, seeking an injunction pendent lite (“during the litigation”) against state officials, rescinding their certification of New Mexico’s 5 Electors. 

The complaint, as was the case in Wisconsin, is framed as a pure question of law — hence, no fact-finding proceeding is needed. Essentially, Democrat election officials made changes to the law as passed by the legislators, in the guise of interpretation. 

Put simply, before 2019, there were three recognized ways that voters could exercise the franchise by using absentee ballots: 

1. Mailing them in; 
2. Walking them in to the county clerk’s office; or 
3. Walking them into a polling place

In 2019, New Mexico election officials added a fourth way by which to vote absentee: voting by “secured container.” These containers were to be monitored by a video surveillance system and were to be ready at least 90 days before the general election. New Mexico’s top election official extended this idea to include drop boxes, a term not found in the law. As of this writing, the court has not responded.


{% comment %}
### Footnotes & References

[^1]: The American Spectator, [Election 2020: Can Classic Fraud Evidence Save The Day?](https://spectator.org/election-2020-can-classic-fraud-evidence-save-the-day/), John C. Wohlstetter, January 4, 2021
{% endcomment %}

-->



## The Audit Force Voter Fraud Report

A grassroots group --- America's Audit Force --- published a detailed 261-page report in October 2021 that highlighted the many disturbing anomalies with voter rolls, mismatched ballot counts, machine vulnerabilities, third-party interference and other areas of concern. 

Quoting from [the executive summary](https://dow9ovycsk6w7.cloudfront.net/media_items/68735-EXECUTIVE_SUMMARY_ONLY.pdf?1633882929):

{:.small}
> This report presents clear and convincing evidence highlighting the most likely paths
where systemic fraud is taking place, and are as follows:
>
> 1. The outcome of the election for certain races and issues is decided ahead of time
> 2. Estimates are made to determine how many “phantom votes” must be cast to
overcome the will of the people and install the selected candidates. “Phantom
votes” may be cast on behalf of genuine eligible voters on the rolls or fictitious
entries not tied to a real person.
3. The registration database is inflated leading up to the election to create an
adequate credit line of phantom voters and also keep the predicted total turnout
within a reasonable range.
4. The voting system is pre-programmed to perform the monitoring and manipulation
activities required to achieve the desired outcome on election day. The individual
tabulators may be connected to the internet, but don’t need to be to accomplish
this.
5. The voter rolls are backfilled with enough phantom votes to match the predetermined outcome.
6. In the months following the election, some percentage of phantom registrants are
removed
>
> These conclusions are supported by the evidence uncovered
during the current investigation, and falls into four categories:
>
> 1. **Patterns in the data reveal calculations at work and could not have come
from natural, human behavior**
     - The relationship between population, registration, and votes is too closely
correlated to have occurred naturally. Portions of the registration database
and vote totals are calculated.
     - The voter registration database shows fraudulent activity has been
occurring for years and is automated with an equation to produce desired
outcomes.
     - The election data broadcast by the media on election night show votes were
being calculated and not counted in every state
>
> 2. **There are serious anomalies throughout voter registration database and
election results.**
     - **More people are registered and voted than even live in most New
Mexico counties for certain age groups.**
     - Voter registration trends do not correlate with election results. These trends
have been used for decades to accurately predict election results. The fact
that trends did not correlate with election results in 2020 provides
independent evidence that all was not right in 2020.
     - 50,000 voters are listed as having registered after election day; voters as
old as 120 voted in November.
     - Registration and turnout rates in all counties exceed historical norms and
are high enough to indicate fraud. Some counties had more people voting
than even live in the county for certain age groups.
     - There are serious anomalies in the data for absentee ballot voting.
     - Tabulator tapes checked in nine counties do not match the total votes
reported by the SOS.
     - The voter registration rolls are currently being canvassed across the state.
Preliminary estimates of results indicate a minimum error rate of 6 percent
up to 40 percent depending on the county.
>
3. **Vulnerable voting registration database and sloppy policies and practices
have left our election system corrupt and exposed.**
     - 1,198 third-party groups were given enhanced access to add voters to the
registration database by the Secretary of State (SOS), Maggie Toulouse
Oliver.
     - Third-party organizations sent ridiculous numbers of voter registration and
absentee ballot forms to New Mexico residents, encouraging fraud. The
mailers were coercive and sent without any attempt reach only to eligible
voters.
     - Numerous addresses have excessive numbers of voters and preliminary
canvassing results indicate many of these are fraudulent.
     - Secretary of State is not performing her duty to purge voter rolls of
deceased voters.
     - In some counties, poll watchers and presiding judges are being cut out of
the election process and not allowed meaningful access or supervision of
the election.
>
4. **Voting machines provided by Dominion Voting Systems have illegal
features and are vulnerable to hacking.**
     - The “adjudication” feature was never contemplated by the legislature and
produces illegal error rates and results in anomalous outcomes in counties
where it was used.
     - Election equipment is possibly connected to the internet, which may include
tabulators. Examination of the same voting machines used in New Mexico
revealed cellular modems hidden in the hardware. The Secretary of State
does not deny the tabulators may have capability to connect to the internet.
     - Dominion Voting Systems’ software fractionalizes votes in violation of state
law.
     - Dominion Voting Systems provides tens of thousands of pre-printed official
ballots in addition to test ballots. It is unclear what these are used for.
It is the solemn duty of county government officials to ensure elections are free of
corruption. This report demonstrates overwhelming evidence that our elections are not in
compliance with protections recognized and afforded by the New Mexico State and U.S
Constitution

[Read the full report](https://dow9ovycsk6w7.cloudfront.net/media_items/68749-NM_Voter_Fraud_Report_with_Appendices.pdf?1633970140){:.button}


## Otero County Forensic Audit

Following the October 2021 report, above, the grassroots team consisting of engineers, technical experts, legal experts and other willing volunteers were able to gain the legal means and permission of the county commissioners and clerk to audit the ballots, machines, and canvass the voters themselves. The county Sherriff and District Attorneys have also provided willing assistance and helped ensure the process is legal, well-grounded and will result in the necessary evidence for further action. [^1]

[Dr Shiva](https://t.me/vashiva) --- MIT PhD and inventor of email --- and his organization EchoMail is currently performing an analysis of ballot images and envelope signatures. 

{% include canvass/new-mexico %}

{% include canvass/link %}


### Opposition to the Audit

The chairs of the federal US Congress Committee on Oversight and Reform sent [a threatening letter to Dr Shiva](https://t.me/NewMexicoAuditForce/1371) and a [copy](https://t.me/NewMexicoAuditForce/1376) to the US Attorney General—although when an official federal committee references The Daily Beast and other legacy propaganda outlets as the source of their concerns it reveals how little objective grounds they have for interfering. The committee’s letter was riddled with inaccuracies, slander, partisan bias, and projects onto the auditors that they are undermining democracy by doing a thorough investigation. A rebuttal of several of the points is [available here](https://t.me/NewMexicoAuditForce/1352).

Threats have also been made that the electronic machines will be forcibly decertified if they are opened for forensic inspection—although many residents would gladly welcome that consequence, provided they are not replaced with new ones.

The auditors believe that the county clerk has done her best to run the election correctly, but being forced to outsource management of the complex Dominion machines that involve 400 customizable settings opens their elections to manipulation.

The audit is due to conclude in mid-April 2022 and we’ll be sure to share the results.

[More Findings from Otero NM](https://www.mixonium.com/public/post/11532){:.button} [New Mexico Audit Force](https://t.me/NewMexicoAuditForce){:.button}


{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References

[^1]: This interview with Nick Moseder includes a report on the forensic audit, see: "[Interview with "New Mexico Audit Force" David & Erin Clements](https://rumble.com/vxpfd7-interview-w-new-mexico-audit-force.html)", Mar 17, 2022