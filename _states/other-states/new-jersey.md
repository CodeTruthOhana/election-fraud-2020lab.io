---
title: New Jersey
heading: Election Fraud in New Jersey
state: New Jersey
abbr: nj
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}


## 66,504 Ballots Rejected

Below is a screenshot from the New Jersey state webpage:

![](/images/dr-frank/new-jersey/rejected.jpg)

As [Dr. Doug Frank](/dr-frank-reports/) pointed out:

{:.small}
> Even though it is only about 1.4% of the ballots, shouldn't it bother us that the ballots of 66,506 New Jersey voters were rejected?
> 
> What kind of a system disenfranchises that many voters?
> 
> On the other hand, perhaps they should have been rejecting more... since there could have been as many as 10% of the ballots from phantom voters.
> 
> Regardless, this performance is unacceptable by any normal standard, and I doubt something like this could have happened if people were voting in person.
> 
> Can you imagine the tumult that would have resulted if over sixty thousand people were turned away at the polls?
>
> {:.small.muted}
> Source: [Telegram Post](https://t.me/FollowTheData/1383), Dec 19, 2021



{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

{% include trend-analysis-summary %}


## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * [2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf) which includes a section on New Jersey

  * [2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf) which lists New Jersey as the state with the 10th highest number of unexpected Biden votes

* [TMS Solutions Election Research: New Jersey Voting History](https://www.election-research.com/index.html#issue/3) --- a brief report that shows how over the last 5 elections, several of the race percentages remained almost completely fixed despite some fluctuations in other races


{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
