---
title: Missouri
heading: Election Fraud in Missouri
state: Missouri
abbr: mo
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=nil
    registrationsOver100=false
    extra1=nil
    exampleCounty=nil
    exampleDate=nil
%}

## Missouri Election Integrity Hearing

The following video (approx. 3 hours) is from Missouri's Election Integrity Hearing, held on Aug 24, 2021:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vj19cs/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vlnfi2-missouri-election-integrity-hearing-8-24-21.html)

We've not yet had a chance to produce a short summary of the hearing, but if you'd like to assist with that, please contact our team on [Telegram]({{ site.data.globals.telegramLink }}) or via the comment section at the end of this page.


{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

{% include trend-analysis-summary %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Missouri

{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
