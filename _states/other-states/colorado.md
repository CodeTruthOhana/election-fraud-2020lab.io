---
title: Colorado
heading: Election Fraud in Colorado
state: Colorado
abbr: co
last_updated: 4 May 2022
---

Colorado proudly brand themselves as the "Gold standard in elections". [^12] However, as you'll soon see, with the number of irregularities and total opposition to any kind of transparency, Colorado is *not* any kind of gold standard in integrity. More collusive and underhanded activity is being brought to light every week; but here's a summary of what we know about Colorado so far.

{% include toc %}


## Election Integrity Report

In March 2021, a team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report *2020 Presidential Election Contrast Analysis* which lists Colorado as the state with the 3rd highest number of unexpected Biden votes.

[Read the PDF Report](https://election-integrity.info/Contrast_Report.pdf){:.button}


{% include election_integrity_scorecard %}


## Lax Voter ID Laws

Through a combination of automatic voter registration (where everyone with a driver's license is registered to vote), and poor ID verification methods (where election judges in voting centers were not allowed to ask for alternative forms of ID in order to confirm a voter's eligibility), holes in the wall of election integrity have been allowed to widen, and opportunities for fraud have increased. [^13]


## Use of Non-Certified Voting Machines

According to investigative journalist Holly at Altitude, [^14] Dominion Voting Machines were certified in the state in June 2019 based on a Pro V&V Test Report, despite Pro V&V's EAC accreditation having expired more than 2 years earlier, in Feb 2017. The certification was not renewed until after the election, in Feb 2021. This also affected ClearBallot Voting Systems, the vendor used in Garfield and Douglas counties, the only two that don't use Dominion. That means:

* Both Clear Ballot and Dominion voting machines used in Colorado’s 2020 elections --- which we believe amounts to 100% of all election machines used in the state --- were *not legally certified* under Colorado law.

* All Colorado elections conducted since June, 2019 using those systems may not be legal.

* Secretary of State Griswold and Colorado County Clerks have certified potentially-illegal elections. 

For further details and explanatory videos, see our article *[Voting Machines Lacking EAC Accreditation](/in-detail/vstl-labs-not-eac-accredited/)*.


## Suppression of Audits & Public Testimony

Despite 1,400 petitions submitted to Colorado Secretary of State Jena Griswold, she put a ban on all independent election audits in the state. [^3] [^4] [^5] [^19b] Not only did she ignore the requests, but also cut short the hearings in which members of the public could give testimony about election fraud and irregularities. [^1]

> The Colorado Secretary of State’s Office today adopted emergency elections rules prohibiting third-parties [read: auditors] from accessing voting equipment in the state of Colorado. ‘Colorado’s elections are considered the safest in the nation,’ Griswold claims.” 
>
> <small>--- Colorado Secretary of State Jena Griswold's Press Release [^3]</small>

Colorado's elections are *not* the safest in the nation, as you'll see below.


## Tina Peters vs Secretary of State Jena Griswold

Tina Peters, a Clerk in Mesa County, Colorado, took a different approach after being contacted by citizens about notable irregularities. She was keen for an honest and open investigation into the election which *she* was responsible for, yet is undergoing heavy persecution from state and federal authorities for not adopting official state narratives. [^1]

In May 2021, Tina was alerted to the fact that [Dominion Voting Machines](/in-detail/dominion-voting-machines/) was requiring Colorado to apply a "software update" to their voting machines, which they call a "trusted build". During this they disallowed any county staff or independent observers to be present. She was concerned that crucial forensic data might be lost during the update, and so took hard drive images prior to the update occurring. Her concerns were valid, as explained below. [^2] [^7]

During a flight to Sioux Falls for the Cyber Symposium ([read our overview here](/in-detail/cyber-symposium-mike-lindell/)), authorities raided her office, barring other staff from observing what went on. Tina claims they were looking to pin blame on her for the leaking of voting machine passwords which only the Secretary of State actually has access to.  [^2]

Watch Tina's story, given at the Cyber Symposium, August 10, 2021:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vid0jv/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

A more detailed interview with Tina and Sherronna is available from [Truth and Liberty](https://truthandliberty.net/episode/tina-peters-and-sherronna-bishop-searching-for-the-truth/), with further coverage via [Ashe in America](https://asheinamerica.com/category/mesa-county/), the [Mesa County Clerk Peters Rally](https://www.americasmom.net/?wix-vod-video-id=619143106efa4c50b86bd7bb20f30cbb&wix-vod-comp-id=comp-ktm3qb48), [American Faith](https://americanfaith.com/mesa-county-clerk-tina-peters-says-her-office-was-wrongly-raided-by-colorado-secretary-of-state/) and [USEIP](https://useip.org/blog/).

On Nov 16, 2021, Tina Peters, a 66-year-old who lives alone, had her home raided by FBI officers with combat gear, automatic weapons, and a battering ram. All her electronic devices, USB drives, and storage devices were confiscated. [^18] [^20] [^21] She has not yet been charged with any crime.

Tina needs assistance with battling the mounting legal battles from those who want to silence her and destroy the evidence she has collected. Contributions can be made at [StandWithTina.org](https://StandWithTina.org).



{% include electronic_voting_machines %}


## The Money Trail

Apparently, [Dominion Voting Systems](/in-detail/dominion-voting-machines/), through their lobbyist Brownstein Hyatt Farber Schreck, have made at least 30 donations to Colorado Secretary of State Jena Griswold's election campaign, which would indicate a strong conflict of interest in her decision-making. [^19] [^19d] Griswold has already displayed a strong, defence stance over Dominion, granting them cover, and permitting them to destroy election records, in violation of law, as we outline below. Griswold also has connections with the Lincoln Project, and with George Soros. [^20] 

Dominion also donated to Colorado Attorney General Phil Weiser's re-election campaign. [^19d] The AG has yet to investigate the election laws broken by Dominion.

We believe these connections and any possible malfeasance should be thoroughly investigated. 

See also *[Blackmailed Into Renewing Dominion Contract](#blackmailed-into-renewing-dominion-contract)*, below.


## Forensic Evidence of Dominion Deleting Election Data

Following [initial revelations](/in-detail/cyber-symposium/codemonkeyz-technical-forensic-session/) at the Cyber Symposium, forensic experts reviewed two hard drive images taken from a Dominion EMS Server in Mesa County. The two images were from the same machine, one taken *prior* to a "system update" applied by Dominion, and the other taken afterward.

The hard drive images can be downloaded from [BitTorrent Link 1](magnet:?xt=urn:btih:dc654b50ec08a8ad5d8f6275f9cd4fcae29686c1&dn=CnuDA4EHJS0glXNC.zip&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce) and [BitTorrent Link 2](magnet:?xt=urn:btih:ef534e78bbe71b3908ccf074d6d40077a3a63074&dn=ic9WLQaUKTRWV2Sv.zip&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce).

Their report clearly demonstrates that 695 log files, databases, and thousands of other files and other crucial data were removed during this "update", in breach of both state and federal laws that require election data to be maintained for at least 22 months following an election. [^8]

![Mesa County Dominion Forensic Report Summary](/images/colorado/forensic-summary.jpg){:.image-border style="padding: 10px 0 20px"}

The forensic examination and report was prepared by [Doug Gould](https://useipdotus.files.wordpress.com/2021/09/doug-gould-bio.pdf), of CyberTeamUS.

<!-- Previous draft version was here: https://asheinamerica.files.wordpress.com/2021/09/the-mesa-report.pdf -->

[View the Full PDF Report](https://useipdotus.files.wordpress.com/2021/09/21.09.21-amended-exhibit-f-ex-f-1-1.pdf){:.button}

{:.highlight}
> It seems clear that in Mesa County, **Dominion has actively erased both election data (in breach of law)** as well as forensic evidence that could be used to audit the 2020 election. It's likely this has occurred in other counties and states also.

The report also identified open access points on the Dominion voting systems through which votes and vote tallies can be ***secretly altered without detection***. 

USEIP has more detail in [their press release](https://useip.org/2021/09/21/bombshell-report-proves-state-and-federal-election-crimes-committed-county-commissioner-leaks-to-press-instead-of-reporting-crimes/).

The forensic report was signed in the manner of an affidavit (under penalty of perjury) and handed to Mesa County District Attorney Rubinstein. He is obligated under Colorado law to investigate, but has done nothing of the kind, instead choosing to investigate Clerk Peters. [^18]

A [follow-up report](https://www.scribd.com/document/531800293/Mesa-County-Database-and-System-Analysis) by Jeffrey O'Donnell, Database and Systems Analyst, in October 2021, expanded on the numerous security vulnerabilities, including the potential for votes to be "flipped" or manipulated within the machines --- *without leaving a trace*. [^17]


## Canvass Results Revealing Phantom Voters

{% include canvass/colorado %}

{% include canvass/link %}


## Blackmailed Into Renewing Dominion Contract

Mesa County Commissioners were threatened with litigation from Dominion unless they agreed to extend the contract with Dominion Voting Systems all the way through to 2029. Thirty-four Mesa County residents spoke to the commission on Aug 24, *all* of whom asked them to abstain from renewing the contract, yet the concerns were ignored and the Commissioners voted 3-to-0 to sign the contract. 

With a fresh contract for millions of dollars in hand, Dominion swiftly dropped their litigation. [^10]


{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=0.993
    extra1=nil
    registrationsOver100=true
    exampleCounty=nil
    exampleDate=nil
%}


## Lawsuit Filed Against SoS Jena Griswold

On Nov 19, 2021, a lawsuit was filed against Secretary of State Jena Griswold. It centers around three levels of state corruption:

1. Election officials knowingly destroyed election records in violation of both state and federal laws. 

2. Election officials certified voting equipment based on a test lab that was not accredited, again in violation of law. 

3. SOS issued “emergency” decrees to block the independent audits that citizens have been asking for, stating that only election officials can audit themselves. 

[View Press Release](https://useip.org/2021/11/19/griswold-is-in-hot-water/){:.button} [View Court Filing](https://causeofamerica.org/wp-content/uploads/Hanks-Et-All-v.-Jena-Griswold.pdf){:.button}


## SCORE Voter Registration Database Vulnerabilities

Retired US Air Force Colonel Shawn Smith has raised concerns about [SCORE](https://www.coloradosos.gov/pubs/elections/SCORE/SCOREhome.html), Colorado's Statewide Voter Registration System [^15], along with it's connection to the interstate system [ERIC](https://ericstates.org/). He says that the numbers of voters being registered in the system appear to indicate padding or "stuffing" of the voter rolls. For example, between 2016 and 2020, in El Paso County alone, active Democrat voters increased 11%, active Republican voters increased 0.5% and unaffiliated voters increased by over 40%. This is a highly unusual trend that may indicate fake or "phantom" entries are being added to the database for the purpose of casting fraudulent ballots. Inflated rolls is a significant issue that is occurring in multiple states. [^11]

Shawn has counted over 200 cyber vulnerabilities in the SCORE system. [^11] He believes that while the officials tasked with securing the SCORE system may be well-meaning, they're not equipped to withstand the threats of foreign nations intent on attacking US elections.

After requesting information from the state officials, Shawn was given a list of internet IP addresses that made connections to the SCORE system around the time of the election. Apparently a number of these IP addresses were located in foreign nations, which should never have occurred. The state office likely regrets sharing this information and refuses to give any further details. [^19c]

There is also little transparency in SCORE's closed system. Voters are unable to see what signatures are stored on their record. [^16]


{% include bloated_voter_rolls %}


#### Voter Roll Growth Anomalies

Potentially-related to the SCORE issues noted above, USEIP shared this chart from DataJeff that shows that in the lead-up to an election, Colorado's voter rolls increase in size far quicker than the population is growing, and then are cleaned out immediately after:

![](/images/colorado/voter-roll-chart.jpg)

{:.caption}
Source: [Telegram Post](https://t.me/useip/1349), Mar 18, 2022

Unless the state has a rational explanation for why this occurs, it raises further suspicion that fake or "phantom" entries are being illegally injected for the purpose of casting fraudulent ballots. 


## USEIP

The [U.S. Election Integrity Project](https://useip.org/) (USEIP) was established in Colorado in November 2020 in response to blatant election fraud. Their aim is to enable election integrity groups to share knowledge and educational resources.

[Find out more and Get Involved](https://useip.org){:.button}

USEIP gave a presentation to the Republican Study Committee in Colorado in April, 2021 that outlined the major security vulnerabilities in election systems. [View the presentation slides and related documents here](https://useip.org/colorado/). They also post detailed news and analysis on issues in Colorado on [their blog](https://useip.org/blog/).

USEIP members including Retired US Air Force Colonel Shawn Smith and Sherronna Bishop met with Mesa County Commissioners in early September 2021, attempting to explain the [critical security vulnerabilities](https://useipdotus.files.wordpress.com/2021/09/useip-co-election-integrity-briefing_2.pdf) in their Dominion Election Systems. The meeting which was thought to be private, was instead attended by hostile media, Dominion representatives and other officials in apparent collusion. The security concerns were mostly ignored. [^6]


{% include trend-analysis-summary %}


## Other Reports & Updates

* Retired US Air Force Colonel Shawn Smith presented scathing election integrity concerns to the Secretary of State on August 3. [Read the transcript here](/in-detail/shawn-smith-statement-colorado/). He also presented at [Mike Lindell's Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/), along with Tina Peters, and [clearly demonstrated on video](https://rumble.com/vl7y6g-cyber-symposium-simulation-of-how-your-votes-can-be-flipped.html) how insecure the voting machines are.

* Charts posted on Telegram purport that the number of mail-in ballots received from unaffiliated voters matches the number of ballots for Republican voters *almost exactly*. This appears to indicate that someone was tracking the mail-in ballots for Republicans and matching them exactly with ballots from unaffiliated voters (possibly "phantom voters" added to the rolls for the purposes of fraud).

  [![Colorado Mail In Ballot Chart 2020 vs 2016: El Paso and Douglas Counties](/images/seth-keshel/co-mailchart1.jpg)](/images/seth-keshel/co-mailchart1.jpg)

  [![Colorado Mail In Ballot Chart 2020 vs 2016: Mesa County](/images/seth-keshel/co-mailchart2.jpg)](/images/seth-keshel/co-mailchart2.jpg)

* [Ashe in America](https://asheinamerica.com/), by USEIP Co-Founder, is a blog that covers many of the above issues from Colorado in far more detail. We recommend reading their in-depth articles, a number of which are listed under *[Further Updates](#further-updates)* below.

* Election monitor Harvie Branscomb has also flagged numerous issues with election integrity in Colorado since 2015, including voter signature anomalies, and posts about it on [his website](https://electionquality.com/)


## Colorado's "Gold Standard"

Despite the above mentioned major integrity concerns, leaders continue to boast about the security of their systems:

*"Colorado is the ‘Gold Standard’ of safe and secure voting.”* --- Governor Polis  
*“Colorado’s election model is the nation’s gold standard.”* --- Secretary of State Griswold  
*“...arguably the best system in the country.”* --- ELP CCR Broerman  
*“Our votes are not being manipulated.”* --- Congressman Ken Buck  
*“There’s no evidence.”* --- CO Rep Fields  
*“Dominion...tested [small, risk-limiting audits] in 62 (CO) counties at least 807 times.”* --- Former SoS Williams  
*“No voting systems connected to the internet”* --- Nobody who has checked  
*“No election fraud has been proven in court”* --- The Media


<div class="info" markdown="1" style="font-size: 110%">
**Do you live in Colorado?**  
Contact your County Clerk, make sure they have read the [recent forensic report](https://asheinamerica.files.wordpress.com/2021/09/the-mesa-report.pdf) from Mesa County, and ensure they take forensic images of all Dominion machines *before* they apply the next software update, to prevent the loss of crucial forensic data.
</div>


{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}


### Footnotes & References

[^1]: According to Retired Air Force Colonel and election integrity activist [Shawn Smith](/in-detail/shawn-smith-statement-colorado/) speaking at Mike Lindell's Cyber Symposium, Aug 10, 2021. See Rumble video on this page, or [alternate link here](https://rumble.com/vkz6p5-colorado-tina-patriot-peters.html).

[^2]: Tina Peters, speaking at Mike Lindell's Cyber Symposium, Aug 10, 2021. See Rumble video on this page, or [alternate link here](https://rumble.com/vkz6p5-colorado-tina-patriot-peters.html).

[^3]: Secretary of State Press Release: "[Colorado Secretary of State’s Office Adopts Emergency Rules for Voting Systems](https://www.sos.state.co.us/pubs/newsRoom/pressReleases/2021/PR20210617Rules.html)", Jun 17, 2021

[^4]: Ashe In America: "[jena griswold panics and bans independent auditors from colorado: over the target](https://asheinamerica.com/2021/06/17/jena-griswold-panics-and-bans-independent-auditors-from-colorado-over-the-target/)", Jun 17, 2021

[^5]: Colorado Herald: "[Colorado Secretary of State Griswold bans voting audits, raises further suspicions of voters](https://thecoloradoherald.com/2021/colorado-secretary-of-state-griswold-bans-voting-audits-raises-further-suspicions-of-voters/)", Jun 19, 2021

[^6]: U.S. Election Integrity Plan: "[Anti-Election Audit Heavyweights Attack Mesa Locals For Demanding Election Integrity](https://useip.org/2021/09/02/anti-election-audit-heavyweights-attack-mesa-locals-for-demanding-election-integrity/)", Sep 2, 2021

[^7]: Truth and Liberty: [Interview with Tina Peters and Sherronna Bishop](https://truthandliberty.net/episode/tina-peters-and-sherronna-bishop-searching-for-the-truth/), Sep 20, 2021.

[^8]: Federal law requires records to be kept for 22 months. Colorado state law requires an additional 3 months (25 months total).

[^10]: According to Tina Peters, see video [^7] above, from the 45min 40sec mark.

[^11]: Interview with Shawn Smith, "[Culture Impact Team: Election Fraud Panel](https://youtu.be/j7sOUWQV5tU?t=2292)", at Church For All Nations, Mar 5, 2021. Relevant sections are at 43min mark and 1hr 49min mark.

[^12]: A Colorado poll watcher was first introduced to Colorado as being "The gold standard in elections" during her election training, but as she soon learned, the tagline was repeated a little too often, especially when anyone would dare question the system. She soon discovered that this branding was an attempt to ignore or cover up the state's role as "The Election Fraud Test Kitchen".  

    Read her account at Holly at Altitude: "[Colorado: The Election Fraud Test Kitchen](https://hollyataltitude.com/2021/01/27/colorado-the-election-fraud-test-kitchen/)", Jan 27, 2021. She covers the history of how Colorado adopted the issues that are flagrant in the state today.
    
[^13]: Holly at Altitude: "[Colorado: The Election Fraud Test Kitchen](https://hollyataltitude.com/2021/01/27/colorado-the-election-fraud-test-kitchen/)", Jan 27, 2021. She covers the history of how Colorado adopted the issues that are flagrant in the state today.

[^14]: Holly at Altitude: "[Only We Can Audit Ourselves](https://hollyataltitude.com/2021/06/22/only-we-can-audit-ourselves/)", Jun 22, 2021

[^15]: As a summary of SCORE's complex background, Shawn Smith reports that SCORE was first developed in Colorado in 2007, under a contract with Sabre, which was bought by Electronic Data Systems (EDS), then bought by Hewlett Packard, parts were then bought by DIMS, and then by Runbeck (the ballot printer in Phoenix). It gets very complicated. See [^11] from 1hr 49min mark.

[^16]: See [^11] from 1hr 54min mark.

[^17]: Jeff O'Donnell also explained his findings on the Conservative Podcast: "[Special Emergency Election Report w/Jeff O'Donnell and Joe Oltmann](https://rumble.com/vnm4xm-breaking-special-emergency-election-report-wjeff-odonnell-and-joe-oltmann.html)", Oct 11, 2021.

[^18]: Brannon Howse Live: "[Exclusive: FBI Raids Home of County Clerk Tina Peters in Attempt to Cover Up Voter Crimes?](https://frankspeech.com/tv/video/exclusive-fbi-raids-home-county-clerk-tina-peters-attempt-cover-voter-crimes)", Nov 16, 2021. Tina Peters and Shawn Smith are introduced at 14min 40sec mark.

[^19]: See item [^18], at 4:30min mark. Additionally, Tina Peters reports that Jena Griswold is also funded by George Soros, see [^20] at 42min mark.

[^19b]: See item [^18], at 48min mark.

[^19c]: See item [^18], at 53min mark.

[^19d]: See item [^18], at 54min 40sec mark.

[^20]: Bannon's War Room: "[FBI Attempts To Intimidate Moms, Dads, and Gold Star Mothers](https://frankspeech.com/tv/video/fbi-attempts-intimidate-moms-dads-and-gold-star-mothers), Nov 17, 2021. Tina Peters is interviewed from 41min 15sec mark, for approximately 10mins.

[^21]: A panel of 4 ex-FBI agents that spoke to Bannon Howse expressed their utter disgust at the unethical practices and lack of integrity in the current FBI. It's covered in two 1-hour episodes of Bannon Howse Live, on FrankSpeech.com, [here](https://frankspeech.com/tv/video/four-fbi-agents-warn-marxist-revolution-america-and-doj-and-fbi-part-one) and [here](https://frankspeech.com/tv/video/four-fbi-agents-warn-marxist-revolution-america-and-doj-and-fbi-part-two). Nov 15-17, 2021.

