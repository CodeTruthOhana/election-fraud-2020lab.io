---
title: Indiana
heading: Election Fraud in Indiana
state: Indiana
abbr: in
last_updated: 4 May 2022
---

{% include state_stub_1 %}

## The Indiana First Action Report

On March 21, 2022, the grassroots group *Indiana First Action* released an hour-long video presentation of their research and findings surrounding the 2020 election:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vvhh1p/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vy3n3n-expose-on-indianas-elections.html)

{:.small}
Timestamp | Topic Covered
-|-
07:30 | Cyber Summary
13:00 | Voter Roll Analysis Executive Summary
19:00 | Canvass Summary
26:40 | Legislator Interviews with Curt Nisly and John Jacob
38:00 | Who IFA spoke with / How do we fix this?
43:00 | Take the Pledge
46:00 | County by County Analysis
54:30 | Past 4 Elections Overlay Analysis
57:00 | Mail in vs. Early Walk in vs. Election Day result analysis
1:03:45 | Registration File Anomalies
1:09:30 | CTCL (Center for Tech & Civic Life) push in Indiana
1:11:15 | One Day One Vote

From the door-to-door canvassing performed in Hamilton and Johnson counties, they found that 34-46% of addresses surveyed had some kind of election-related anomaly whether it was a voter who didn't exist at the address, a vote that was not correctly recorded, or some other kind of concerning record-keeping error.

{% include canvass/link %}


{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=nil
    registrationsOver100=false
    extra1=nil
    exampleCounty=nil
    exampleDate=nil
%}

{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

{% include zuckerberg_interference.html %}

{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
