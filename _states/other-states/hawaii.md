---
title: Hawaii
heading: Election Fraud in Hawaii
state: Hawaii
abbr: hi
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

{% include trend-analysis-summary %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which discusses how Hawaii unexpectedly gained a significant number of overall votes, despite a decrease in population

{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
