---
title: Tennessee
heading: Election Fraud in Tennessee
state: Tennessee
abbr: tn
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=0.996
    registrationsOver100=true
    extra1=nil
    exampleCounty=nil
    exampleDate=nil
%}

{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

{% include trend-analysis-summary %}

{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
