---
title: Louisiana
heading: Election Fraud in Louisiana
state: Louisiana
abbr: la
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
