---
title: Pray for Election Integrity
last_updated: 24 Aug 2021
comments_enabled: true
---

Ask our loving God to act in dealing with corruption, restoring justice and integrity in the nation, and prayerfully consider who you should share information with and what your role might be. 

### America First Audits Prayer Warrior Group

> Father, You know our hearts, You know our doubts and You know how to build our faith in You! When so many things look dark and hopeless, we turn our eyes to You and believe You are answering. You’re never too late. What looks one way to us isn’t how You see it. You’re always looking for opportunities to build our faith. 
>
>We lift up audits in all 50 states and place them in Your life giving hands. We believe America will be saved by You. Blow life into all who are being Your hands and feet to bring about election integrity. Let Your freedom ring over us! In Jesus name.

> Help us to be sensitive to You Lord in all we do as we work towards bring our great nation to You through audits, and help us to restore our elections to be honest, free and fair so that we the people will elect Godly leaders.  Help us to be the light, in Jesus Name, Amen.

The above prayers are just some of the *many* being shared in the America First Audits Prayer Warrior Group. The group channel on [Telegram](https://telegram.org/) connects those who wish to pray for election integrity and the efforts to undertake transparent audits across the nation. You will need either the mobile app, or the desktop app for PC/Mac/Linux.

Join via **<https://t.me/joinchat/ZUOl1QKriC04MDRh>**


### Statement of Humility

We know that we may not have got everything right on this site. We're still learning. Some allegations that we've covered may yet prove to be untrue, and we'll gladly correct those when we discover them. But the mounting evidence combined with great media, tech, and government censorship swayed us to get this information out, even if imperfect, rather than wait until everything is 100% verified.

We've attempted to do this with prayerful consideration and caution. Accept our sincere apologies if any misinformation has made its way onto this website. And let us know so that we can resolve it before it spreads further. Join us in praying that the truth gets out, and the lies get exposed, in Jesus' name.

*--- Si Williams & contributors*
