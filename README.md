# US 2020 Election Fraud at a Glance

### electionfraud20.org

This repository on Gitlab contains the text content and source code for [electionfraud20.org](https://electionfraud20.org), as well as collaboration and discussion tools.

If you're interested in helping out, you might like to:

1. **Share** the site with your friends, family and social media followers to help them easily access a factual overview

2. Flag typos, inaccuracies and suggest improvements in the **[Issues tab](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/issues)**

4. Contribute changes to the content and/or code. You can browse the [source files](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/tree/master/_states) here in the repository, and by clicking the "*Edit*" button, Gitlab will prompt you to "*Fork*" the repository (meaning it will create your own personal copy that you can work on). When you're done with a set of changes, you can submit a "*Pull request*" to apply your changes to the main website.

   (If you intend on making large-scale changes, it's helpful to post your intentions in the **[Issues tab](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/issues)** first, so they can be discussed and time is not wasted.)

----

## Understanding the Source Code

* The website is built and hosted on [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)

* The software that runs in the background is called [Jekyll](https://jekyllrb.com/). Jekyll takes the content from markdown files, and combines them with HTML templates and produces a working website, hosted for free. Every time a file is changed, Gitlab will automatically recompile and publish the updated site, within 60-90 seconds. 

* You don't need any special tools or software to edit the site content - you can do it all in your web browser via Gitlab (although you will only get a rough preview - not 100% accurate)

* If you want to make more substantial changes to the code with an accurate preview, you can run your own copy of the website on your local computer, see instructions below

* To make changes, you may like to refer to the following documents:

  * [Guide to Markdown formatting](https://docs.github.com/en/free-pro-team@latest/github/writing-on-github/basic-writing-and-formatting-syntax)

  * [Jekyll Tutorial](https://jekyllrb.com/docs/step-by-step/01-setup/) - including the special features and functions you can use within markdown pages and templates

  * [Guide to Liquid](https://jekyllrb.com/docs/liquid/) - the templating language that Jekyll uses

  * [Guide to Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)

  * ...or feel free to explore the source code to learn from the existing examples
  
* Note that we run Cloudflare in front of the site, for security and performance benefits. Pages may be cached by Cloudflare for several hours. You can view the latest site directly from Gitlab at <https://election-fraud-2020.gitlab.io/>


## To Run Your Own Local Copy

1. Install the [Ruby development environment](https://jekyllrb.com/docs/installation/), via this link

2. Using the command line, install Jekyll and Bundler with these commands:

        gem install jekyll bundler

3. Install dependencies from Gemfile:

        bundle install

1. Build the site and start the Jekyll local development server

        bundle exec jekyll serve --livereload

2. Open [http://localhost:4000](http://localhost:4000) in your web browser

3. 🎉

There is no build script for deploying changes. Just commit & push your code changes, and Github will handle the compilation (of markdown, HTML and SASS) as well as deployment.

## Acknowledgements

* "[Ampersand](https://app.stackbit.com/create?theme=https://github.com/stackbithq/stackbit-theme-ampersand&utm_source=project-readme&utm_medium=referral&utm_campaign=user_themes)" Jekyll theme and starter source code provided by [Stackbit](https://stackbit.com)

* Shield icon by [Freepik](https://www.flaticon.com/authors/freepik) from [flaticon.com](https://www.flaticon.com/)

* Free hosting provided by [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)

## Contact

While it's best to raise issues and feedback via Gitlab, if you feel the need to connect privately over email, reach out to electionfraud20 (at) si.anonaddy.com.
