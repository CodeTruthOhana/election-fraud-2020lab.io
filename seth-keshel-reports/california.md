---
title: California Election Analysis by Seth Keshel
state: California
abbr: ca
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}


First takeaway on California – although California trended Democrat in registrations by 2%, Trump pushed it slightly right even in certified totals.  This is only done with winning low-propensity voters, registered Democrats, independents, especially working class Hispanics in Southern California.

Trump gained 1,520,000 votes, 580k more than record Republican gain, but Democrats have *DOUBLED* their vote total since 2000, with Biden nearly 3 million over Obama in 2008.

These are worst counties for excess Biden votes as predicted by party ID, population growth, and registration:

Alameda | 50k
Contra Costa | 50k
Fresno | 30k
Kern | 20k
L.A. | 500k
Orange | 100k
Riverside | 80k
San Bernardino | 40k
San Diego | 80k
Santa Clara | 60k
Ventura | 25k

Proportionally, Los Angeles, Orange, Riverside, San Bernardino, and San Diego absolutely dwarf other counties for new registered voters.  Are these phantom factories?  Est. 1,346,000 excess Biden votes, would put state at 23.3% margin if only excess votes considered.  I believe California to be much tighter.

Best audit targets: Kern, Shasta, Placer.


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/2>


{% include seth_worst_counties_for_state heading=true %}


## San Diego County

{% include tg-embed url="https://t.me/RealSKeshel/4525" %}



## Possible Voter Roll Manipulation

A number of states, both key competitive states, and not, have machined/trimmed/manipulated their voter rolls for the desired outcomes.  Here are some examples:

CALIFORNIA – San Francisco didn’t seem to care much, expanding the rolls by just 9,000 voters (1.8%); but Orange, Riverside, San Bernardino, and San Diego were all about this election, with between 215k to 297k net new registered voters.

Previous highs since 2008:  

Orange: | 75k  
Riverside: | 106k  
San Bernardino: | 36k  
San Diego: | 90k

A deeper look into California has me questioning my own assessment of the state, as Trump improved tremendously in most of the major counties in the state, especially San Diego, LA, Orange, Riverside, and San Bernardino.  California, as you know, is vital for the national popular vote propaganda piece for Democrats, and somehow manages to turn everything around it blue and still double its own Democrat votes in two decades with a now stagnant population growth.  Can’t be both.

{:.caption}
Source: <https://t.me/RealSKeshel/788>


## Seth's View on Recall Election Results

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/1353" data-width="100%"></script>


{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}


## Further Updates

{% include tg-embed url="https://t.me/RealSKeshel/7233" %}



{% include seth_reports_methodology %}

{% include raw_data %}


## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists California as the state with the highest number of unexpected Biden votes

{% include canvass/get-involved %}

{% include state_telegram_channels %}
