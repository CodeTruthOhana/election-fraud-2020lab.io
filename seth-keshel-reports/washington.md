---
title: Washington State Election Analysis by Seth Keshel
state: Washington
abbr: wa
last_updated: 28 Feb 2022
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

{% include seth_state_details.html %}

Washington has a violent, large city generating large Trump vote gains in suburbs and heavy white working class coalition shifts around state in fishing and timber industries. How did Trump gain 363k votes and get knocked back another 4% after 2008/2012/2016 Republican nominees couldn’t get out of 1.2 million vote range?

Based on predictive trends, there's an estimated 228k excess Biden votes before counting King County, led by:

Clark | 30k
Pierce | 30k
Snohomish | 40k
Spokane | 25k
Thurston | 10k
Whatcom | 10k

Washington Republicans and Democrats bled a lot to third parties in 2016 so I’ve been generous to Biden total for 2020 and am likely too favorable to him.  For King County, there's an estimated 105k excess from just available trends, not accounting for past “engineering” of county.  If correct, there's an estimated 333k excess statewide votes and accurate Biden margin to be +12.0%, only considering excess votes.

Best audit targets are Clark, Spokane, Yakima, Chelan, and Pierce.


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/6>


{% include seth_worst_counties_for_state heading=true %}


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/6>



## Possible Voter Roll Manipulation

A number of states, both key competitive states, and not, have machined/trimmed/manipulated their voter rolls for the desired outcomes.  See [New Hampshire](../new-hampshire/), [Alaska](../alaska/), [California](../california/), and [Arizona](../arizona/).

There are other states with bloated rolls, like [Florida](../florida/) and [Texas](../texas/). [Colorado](../colorado/) doesn’t show much movement in the two parties but tons of new registered indies/others, along with **Washington** and [Oregon](../oregon/). [Pennsylvania](../pennsylvania/) and [North Carolina](../north-carolina/) appear to have let their party rolls run clean, showing a tremendous beating getting ready to be done by Trump, but pulled things off differently.  

There appears to be a lot of phantom voters on the rolls -- fake records.

{:.caption}
Source: <https://t.me/RealSKeshel/788>


## Clallam County: The Only True Bellwether?

As Seth has noted, Clallam county is the only bellwether county that voted for Biden in 2020. But even this county looks suspect when you look at the numbers more closely:

In 2008 Obama won convincingly, but in 2012 and 2016 the Democrats lost votes. Then they had a big change of mind and decided to vote for Biden in 2020. Really? Also, the turnout increase (10K) on the previous election is uncharacteristically huge.

[View Stats](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/issues/12){:.button}


## Grays Harbor County

{% include tg-embed url="https://t.me/RealSKeshel/3240" %}



## Unexpected Surge in Mail-In Ballots

{% include tg-embed url="https://t.me/RealSKeshel/3237" %}


## Seth Interviewed in Washington

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vrmubv/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Feb 6, 2022. [Watch on Rumble](https://rumble.com/vu90ex-stopping-election-fraud-starts-with-you-with-army-captain-seth-keshel.html).


## Washington State Audit Updates

<iframe id="preview" style="border:0px;height:500px;width:500px;margin:5px;box-shadow: 0 0 16px 3px rgba(0,0,0,.2);" src="https://xn--r1a.website/s/auditwa"></iframe>

Seth met with elected officials in Washington state on Sunday 15 August, 2021:

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/932" data-width="100%"></script>

"Tonight, rolled right up into one of the most corrupt states in the union and worked with some of the best and brightest to shine light in Washington. 

"Special thanks to some courageous Washington conservative elected officials, Jovan, Glen M., Tam, and Kasey E., and others, for your hard work in putting everything on."

Seth appears at the 32min mark in the following video:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WgY4bgKW2Hw?start=1897" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



{% assign events = site.data.events | where: 'state', page.abbr %}
{% include events %}

{% include seth_reports_methodology %}

{% include raw_data %}


## Other Reports & Updates

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Washington as the state with the 6th highest number of unexpected Biden votes

{% include canvass/get-involved %}

{% include state_telegram_channels %}
