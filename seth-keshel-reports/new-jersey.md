---
title: New Jersey Election Analysis by Seth Keshel
state: New Jersey
abbr: nj
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{% include seth_state_details.html %}


Ever so slightly, New Jersey has been trending away from Democrats and responded big-time in vote increase (281k) for Trump in 2020.  Very conservative estimate (I always go light) is 327k excess Biden votes in New Jersey.

Biden’s gain in New Jersey from 2016 is 460k, simply not possible with the Trump surge and declining population.  Voter registration trends suggest New Jersey moves left, but the ACTUAL VOTING BEHAVIOR from 2012, 2016, and now 2020 suggests the registration rolls are packed with 'phantom' voters registered as Democrat to support outcomes.

**Worst counties:**  
Ocean, Monmouth, Bergen (30k)  
Burlington, Camden, Gloucester, Morris (25k)  
Essex (20k)

If Biden is 327k heavy, accurate margin is 9.4%, 53.8% to 44.4%, and 398k votes.  I would not be surprised if this state was under 5%, and potential flip if cyber is involved.  They stopped count here on election night – the reason is clear now.

**Best GOP audit targets:**  
Ocean, Monmouth, Hunterdon, Sussex


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/107>

----

{% include seth_worst_counties_for_state heading=true %}

{% include seth_reports_methodology %}


## Further Updates

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/1971" data-width="100%"></script>

{% include tg-embed url="https://t.me/RealSKeshel/5504" %}


{% include raw_data %}

## Other Reports

A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on New Jersey

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists New Jersey as the state with the 10th highest number of unexpected Biden votes

{% include canvass/get-involved %}

{% include state_telegram_channels %}
