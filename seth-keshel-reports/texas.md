---
title: Texas Election Analysis by Seth Keshel
state: Texas
abbr: tx
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}

## Texas Overview

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhj1zv/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}


{% include seth_state_details.html %}


Estimating 675k excess votes for Biden, conservatively estimated.  This would still give him a record high Democrat vote gain, as he was up 1.38 million from Clinton, with Trump also up at a record number 1.21 million.  Counties over 50k listed in spreadsheet screenshot.

Since there is no party registration data available, my estimate is based on the 20-year pattern plus political trend.  Trump trended Hispanic Texas heavily Republican, but somehow “lost” Tarrant County and lost big margins in suburbs despite huge ~30% gains or more in all of them. Democrat activity is obvious, many suburbs over 60% growth, huge urbans over 40%.

Put simply, massive Trump gains in San Antonio, Fort Worth, and Houston don’t agree historically with a corresponding massive Democrat jump in those counties.

If accurate on 675k excess Biden votes, Trump margin should have been roughly 55.4 to 43.1 (12.3%) with a margin of 1.3 million votes instead of 631k.

**Best targets for audits (Republican-led):** Tarrant, Collin, Denton, Wichita, Taylor, Bell


### Estimates of Excess Votes by County 

Based on Population Trends and 20-year history, and GOP growth correlation:

Bexar | 60k
Brazoria | 8k
Brazos | 6k
Collin | 50k
Dallas | 50k
Denton | 45k
Ellis | 8k
El Paso | 15k
Fort Bend | 30k
Galveston | 10k
Guadalupe | 8k
Harris | 80k
Hays | 12k
Hidalgo | 10k
Kaufman | 6k
Lubbock | 10k
McLennan | 5k
Montgomery | 20k
Nueces | 7k
Tarrant | 75k
Taylor | 4k
Travis | 50k
Webb | 5k
Wichita | 3k
Williamson | 30k

The following chart displays how in 12 Texas counties that voted Republican in 2016, Trump made large gains in votes of between 16 and 41% over the previous election, and yet Biden somehow swept in and increased his overall votes by *a whopping 42 to 74%* in a single election cycle, flipping these 12 to blue. Does this seem realistic?

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-summary.jpg)


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/66>


Many months later, Seth's estimates on excess votes matched almost exactly with [preliminary results](https://t.me/RealSKeshel/2740) from an election audit in Texas.


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/66>



## County Party Leanings

The following map shows the party leanings of each Texas county, first from the "official" certified results:

[![Seth Keshel Texas County Party Leanings - Democrat vs Republican](/images/seth-keshel/tx-leanings-1.jpg){:width="470"}](/images/seth-keshel/tx-leanings-1.jpg)

And now adjusted with Seth's analysis and heat map:

[![Seth Keshel Texas County Party Leanings - Democrat vs Republican](/images/seth-keshel/tx-leanings-2.jpg)](/images/seth-keshel/tx-leanings-2.jpg)

{:.caption}
Source: [Telegram Post](https://t.me/RealSKeshel/2575), Dec 19, 2021


{% include seth_worst_counties_for_state heading=true %}


## Seth's Presentation to Tarrant County

Here is the complete presentation from July 29, 2021 in Dallas/Fort Worth. National focus followed by Texas focus, followed by action items for believers. 

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhw8r1/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>


## GOP Nominee Vote Growth

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table3.jpg)

{:.small}
> I enjoyed this slide the other day - I hadn't made it until I was thinking of a more effective way to present Trump's Texas performance.
> 
> Everyone knows about the trend with the Hispanic working class, with El Paso, West Texas, South Texas all trending heavily Republican, some counties more than 30%.  
> 
> As an analyst, validity must be given to the final outcome - if real, then a 5.6% statewide margin and the closest popular vote finish in 24 years has to come from somewhere.  
> 
> The TX GOP has been upheld by the suburban counties being heavily Republican.  So did Trump lose the white vote?  THIS IS PERCENTAGE OF VOTES GAINED OVER PREVIOUS ELECTION.
> 
> You tell me - here are the two RINOs after Bush (who won TX by a mile), Trump 1.0 with his Cruz grudge in 2016 (9% win)... and now Trump 2020, in major GOP suburbs/counties."

#### Follow-Up

{:.small}
> Some people asked for more clarity on this forwarded post/graphic. Happy to oblige. 
> 
> With Trump’s known improvements with Hispanics in TX, including in major metros, the only possible way for the margin to tighten by 3.5% from 2016, and to have the popular vote win shrink by 200,000, is to get clobbered in the suburbs. 
> 
> This graphic shows GOP nominee vote growth % from previous election. RINOs McCain and Romney are either down, flat, or modestly gaining in population boom counties in 2008-12 and Trump had a third party throwaway/Cruz grudge problem in 2016. 
> 
> Look at the final column to see the Trump % vote gain and see how impossible it is for Democrats to eat up so much ground in the suburbs. 
> 
> “But they’re growing!”
> 
> They have been large for a long time, and have been growing for decades, with no sudden Democrat boom to counter an historic GOP vote gain boom. 
> 
> Make sense? See below forwarded message."


{:.caption}
Source: <https://t.me/RealSKeshel/682>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2254" data-width="100%"></script>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2265" data-width="100%"></script>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2267" data-width="100%"></script>

{% include tg-embed url="https://t.me/RealSKeshel/3664" %}



## Tarrant County

#### When the Presidential Election is the Odd One Out

Here is a comparison of 27 election races in Tarrant County, Texas. When *every other election* swung to Republicans, how likely is it that the 2020 Presidential election should be the only one to turn blue?

[![](/images/seth-keshel/tx-chart.png)](/images/seth-keshel/tx-chart.png)

{:.caption}
Source: <https://t.me/RealSKeshel/974> and [here](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/issues/15)


#### Tarrant Precinct Heat Map

The following PDF shows a map of which precincts in the county had the most abnormal results in 2020:

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2099" data-width="100%"></script>

{% include tg-embed url="https://t.me/RealSKeshel/6160" %}



## Dallas Fort Worth Counties Combined

{% include tg-embed url="https://t.me/RealSKeshel/3698" %}


## Bastrop County

{% include tg-embed url="https://t.me/RealSKeshel/3595" %}


## Bell County

<iframe src="https://electionfraud20.org/publink/show?code=XZa2JFZfmYrmO1tTizXfkpS3IAMnS4Yh2tV" width="100%" height="630"></iframe>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2484" data-width="100%"></script>


## Bexar County

{% include tg-embed url="https://t.me/RealSKeshel/3257" %}


## Cameron County

{% include tg-embed url="https://t.me/RealSKeshel/4315" %}



## Collin County

{% include tg-embed url="https://t.me/RealSKeshel/3688" %}

{% include tg-embed url="https://t.me/RealSKeshel/6168" %}


## Dallas County

{% include tg-embed url="https://t.me/RealSKeshel/3207" %}

{% include tg-embed url="https://t.me/RealSKeshel/6161" %}


## Denton County

<iframe src="https://electionfraud20.org/publink/show?code=XZ32JFZm3gpGmg47kFff6YkeU6W85qdacyk" width="100%" height="630"></iframe>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2481" data-width="100%"></script>


## Ellis County

<iframe src="https://electionfraud20.org/publink/show?code=XZG2JFZwSLjpf3rNIjbarXtLYx2XFxdj2py" width="100%" height="630"></iframe>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2493" data-width="100%"></script>


## El Paso County

{% include tg-embed url="https://t.me/RealSKeshel/3722" %}

{% include tg-embed url="https://t.me/RealSKeshel/3750" %}



## Fort Bend County

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2386" data-width="100%"></script>

{% include tg-embed url="https://t.me/RealSKeshel/6158" %}


## Gillespie County

{% include tg-embed url="https://t.me/RealSKeshel/2997" %}


## Harris County

{% include tg-embed url="https://t.me/RealSKeshel/3152" %}

{% include tg-embed url="https://t.me/RealSKeshel/6155" %}


## Hays County

{% include tg-embed url="https://t.me/RealSKeshel/4374" %}



## Henderson County

{% include tg-embed url="https://t.me/RealSKeshel/4765" %}


## Hidalgo County

{% include tg-embed url="https://t.me/RealSKeshel/4357" %}


## Lubbock County

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vnqjdr/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/RealSKeshel/1853), Oct 26, 2021

{% include tg-embed url="https://t.me/RealSKeshel/4301" %}


## Nueces County

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/913" data-width="100%"></script>

{% include tg-embed url="https://t.me/RealSKeshel/3511" %}


## Parker County

{% include tg-embed url="https://t.me/RealSKeshel/5336" %}


## Randall County

{% include tg-embed url="https://t.me/RealSKeshel/2928" %}


## Tom Green County

{% include tg-embed url="https://t.me/RealSKeshel/4124" %}


## Travis County

{% include tg-embed url="https://t.me/RealSKeshel/3188" %}



## Williamson County

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2229" data-width="100%"></script>

Our team here at ElectionFraud20.org has also analyzed some additional stats for Williamson County, being discussed in our Gitlab discussion area:

  [View the draft report](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/issues/16){:.button}


## Wichita County

Seth calls it the biggest layup in the state for spotting excess Biden votes, and explains why with 32 years of information.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vjlm2t/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/RealSKeshel/1243), Sep 7, 2021

{% include tg-embed url="https://t.me/RealSKeshel/4244" %}



## The Texas Sponge Map

{% include tg-embed url="https://t.me/RealSKeshel/3667" %}
{% include tg-embed url="https://t.me/RealSKeshel/3668" %}
{% include tg-embed url="https://t.me/RealSKeshel/3669" %}
{% include tg-embed url="https://t.me/RealSKeshel/3670" %}
{% include tg-embed url="https://t.me/RealSKeshel/3671" %}



## Possible Voter Roll Manipulation

A number of states, both key competitive states, and not, have machined/trimmed/manipulated their voter rolls for the desired outcomes.  See [New Hampshire](../new-hampshire/), [Alaska](../alaska/), [California](../california/), and [Arizona](../arizona/).

There are other states with bloated rolls, like [Florida](../florida/) and **Texas**. [Colorado](../colorado/) doesn’t show much movement in the two parties but tons of new registered indies/others, along with [Washington](../washington/) and [Oregon](../oregon/). [Pennsylvania](../pennsylvania/) and [North Carolina](../north-carolina/) appear to have let their party rolls run clean, showing a tremendous beating getting ready to be done by Trump, but pulled things off differently.  

Phantoms.

{:.caption}
Source: <https://t.me/RealSKeshel/788>


## Why Are Audits So Important In Texas?

Seth explains on Bannon's War Room, Sep 13, 2021:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vihidh/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

He also provided an update on the so-called "full forensic audits" in Dallas, Harris, Collin and Tarrant Counties. According to Seth "those audits don't 'cut the mustard' ... Don't get too excited about them." He also announces that he's decided not to run for office in Texas:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vkohij/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram post](https://t.me/RealSKeshel/1563), Sep 30, 2021

Here's the link to the [OAN interview with Steve Toth](https://rumble.com/vn30dk-the-real-story-oan-auditing-texas-with-steve-toth.html) that Seth referred to in the above video.


## Preliminary Texas Audit Results

Seth's original prediction of 675,000 excess votes was further validated in Jan 2022 with the release of preliminary audit findings in Texas:

{% include tg-embed url="https://t.me/RealSKeshel/2740" %}

{% include tg-embed url="https://t.me/LibertyOverwatchChannel/6235" %}



{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}


## Support the Audit

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/521" data-width="100%"></script>

![Texas - Support SB1](/images/seth-keshel/tx-support-audit.jpg){:style="border: 1px solid"}

<small>[Source](https://t.me/TXAuditNews/576)</small>


{% include seth_reports_methodology %}

{% include raw_data %}


## Response from Texas Officials

According to Seth ([here](https://t.me/RealSKeshel/1572)), he has been in contact with Former Senator and Governor-candidate Don Huffines and Republican Party Chairman Allen West who have been willing to discuss Seth's analysis and calls for audits. 

Governor Greg Abbott has so far been unresponsive.

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/1573" data-width="100%"></script>

{% include tg-embed url="https://t.me/RealSKeshel/2568" %}


## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which includes a section on {{ page.state }}

* [Dallas, Texas 2020 Election Fingerprints](https://wwrkds.net/wp2/dallas-tx-election-fingerprint/), by wwrkds


## Further Updates

{% include tg-embed url="https://t.me/RealSKeshel/6152" %}



{% include canvass/get-involved %}

{% include state_telegram_channels %}
