---
title: Rhode Island Election Analysis by Seth Keshel
state: Rhode Island
abbr: ri
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Rhode Island has a big white working class population that has been drifting away from Democrats for a decade, and accelerated rapidly toward Trump in 2016.  Both Trump and Biden have strong gains here in a state that is tiny, with little population growth overall.

They also stopped the count here – it reminds me of NJ to a degree.  Biden is at least 45k votes heavy.  Obama -17k in reelection, Clinton -27k in 2016 as Trump gained 23k.  This year, it looks like this could have been a single digit race.

They are blowing it over everywhere, 20k excess in Providence, reversing a trend that gets worse when GOP gains.  

If Biden is 45k heavy, his margin would be 56.6% to 43.2%, or just 62k votes.  If there is electronic vote flipping present and I’m light, this state could have been down to the wire.

Best Audit Targets: everything but Bristol


{:.caption}
Source: <https://t.me/RealSKeshel/715>

----

{% include seth_worst_counties_for_state heading=true %}

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
