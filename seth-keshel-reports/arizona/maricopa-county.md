---
title: Maricopa County Election Analysis by Seth Keshel
breadcrumb: false
parent_page: /seth-keshel-reports/arizona/
last_updated: 7 Dec 2021
---

*View [Seth's Arizona Report](../) for an overview of what occurred in the state.*

See the following chart for more alarming figures -- I’ve already done a lot of research here. 

> Could Biden have really increased the Democrat votes by a ***whopping 48.07%*** over a single election cycle? Maricopa County is highly suspect.

<div class="force-wide" markdown="1">
![Seth Keshel Maricopa County Trends, Arizona](/images/seth-keshel/az-table.jpg){:.image-border width="1000" style="padding: 0px 15px 12px"}
</div>

This tells you the tale of the tape in Maricopa County.

Orange, San Diego, Harris, King, and Miami-Dade Counties are the only 5 counties within 100,000 Democrat votes of the total in Maricopa County (703k) in 2016.  This is how each of them grew.  Miami-Dade was thrown out because they had a drop-off in Democrat votes thanks to the Trump gain.

The rest of it speaks for itself.

Sound possible?

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/10> and <https://t.me/RealSKeshel/962>

Seth also provided an initial review of the results from the Maricopa Forensic Audit (Sep 25, 2021):

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vkcivp/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

He also joined Flyover Conservatives for their 40min podcast episode [Captain Seth Keshel: AZ Audit Update](https://rumble.com/vn4nnp-captain-seth-keshel-az-audit-update-part-2-flyover-conservatives.html) to expand further on the audit (Sep 29, 2021).


#### Maricopa Precincts

<iframe src="https://electionfraud20.org/publink/show?code=XZtE3JZifleuQo3dSRBmeEu85gwhjsTYzoV" height="600" width="100%"></iframe>

<!--
<iframe class="scribd_iframe_embed" title="Maricopa Election Precinct Heat Map With Metro Breakout" src="https://www.scribd.com/embeds/545203635/content?start_page=1&view_mode=slideshow&access_key=key-iSgWezkyad48M5vBs9ts" tabindex="0" data-auto-height="true" data-aspect-ratio="1.2941176470588236" scrolling="no" width="100%" height="600" frameborder="0"></iframe>
-->

<style>
  .cols { column-width: 11.7em; font-size: 70% }
</style>

##### Cleanest Precincts [^p1]

[^p1]: Telegram post: <https://t.me/RealSKeshel/1848>

<p markdown=1 class="cols">
  0519 PUEBLO  
  0580 SCOTTSDALE  
  0340 KALER  
  0322 INDEPENDENCE  
  0571 SAN VICTOR  
  0098 CENTENNIAL  
  0570 SAN SIMON  
  0170 DEERVIEW  
  0561 SALOME  
  0437 MORRISTOWN  
  0065 BULLARD  
  0067 BUNKER PEAK  
  0625 SPUR CROSS  
  0583 SENTINEL  
  0662 TABLE MESA  
  0288 HARQUAHALA  
  0648 SUNFLOWER  
  0673 TORTILLA FLAT  
  0297 HERMOSA  
  0136 CORDES  
  0475 PALMDALE  
  0315 HOPE  
  0620 SOUTHERN  
  0582 SENNIA VISTA  
  0360 LASSEN  
  0402 MARIVUE  
  0613 SOUTH MTN HIGH  
  0312 HOLLYHOCK  
  0598 SIERRA VISTA  
  0427 MITCHELL PARK  
  0188 DESSIE  
  0407 MARYVALE  
  0068 BURGESS  
  0274 GREENFIELD  
  0649 SUNLAND  
  0719 WESTRIDGE  
  0124 COLTER  
  0047 BETHUNE  
  0280 GUADALUPE  
  0658 SURPRISE  
  0575 SANTA MARIA  
  0037 AVONDALE  
  0630 STARLIGHT  
  0678 TREVOR BROWNE  
  0424 MINNEZONA  
  0290 HARRISON  
  0295 HAYDEN HIGH  
  0491 PECK  
  0545 ROADRUNNER  
  0389 LYNWOOD  
  0204 DUNBAR  
  0160 CULVER  
  0433 MONTE VISTA  
  0308 HOLIDAY GARDENS  
  0367 LEWIS  
  0102 CHAVEZ  
  0306 HILTON  
  0326 ISAAC  
  0238 FORT MCDOWELL  
  0493 PEE-POSH  
  0465 ORME  
  0564 SAN LUCY  
  0607 SMP 3  
  0606 SMP 2  
  0605 SMP 1  
  0028 ARDMORE  
  0634 STRADA  
  0070 CACTUS GLEN  
  0703 WACKER PARK  
  0150 COYOTE LAKES  
  0478 PALO CRISTI  
  0186 DESERT VISTA  
  0200 DREYFUS  
  0489 PEAK VIEW  
  0621 SPANISH GARDEN  
  0285 HAPPY VALLEY  
  0393 MAGGIE  
  0193 DOBSON PARK  
  0544 RIVIERA  
  0072 CALAVAR  
  0633 STONEGATE  
  0014 ALSUP  
  0510 PIONEER  
  0062 BUCKEYE  
  0343 KIMBERLY  
  0230 EVANS  
  0486 PARK MEADOWS  
  0330 JASMINE  
  0452 OAKHURST  
  0284 HAPPY TRAILS  
  0061 BROOKS FARM  
  0603 SKY HAWK  
  0428 MOBILE  
  0291 HASSAYAMPA  
  0029 ARLINGTON  
  0417 MESA GRAND  
  0576 SANTAN  
</p>

##### Filthiest TRUMP REPUBLICAN Precincts in Maricopa [^p2]

[^p2]: Telegram post: <https://t.me/RealSKeshel/1849>

These were precincts won by Romney, where Trump won them by more in 2016. They should still be trending Republican based on party registration elsewhere.

<p markdown="1" class="cols">
  0182 DESERT OASIS  
  0704 WADDELL  
  0234 FESTIVAL  
  0713 WEST POINT  
  0659 SURPRISE FARMS  
  0664 TARTESSO  
  0551 ROSE  
  0038 AZTEC SPRINGS  
  0737 WILLOW CANYON  
  0637 SUN AIRE  
  0404 MARLEY PARK  
  0663 TARO  
  0270 GRANITE FALLS  
  0579 SARIVAL  
  0325 INDIAN WELLS  
  0162 DAISY  
  0053 BLOOM  
  0641 SUN LAKES COUNTRY CLUB  
  0483 PARADISE  
  0335 JUNIPER  
  0145 COUNTRY GABLES  
  0626 ST CHRISTOPHER  
  0629 STARDUST  
  0405 MARTIN  
  0709 WATSON  
</p>

##### Filthiest Democrat Precincts in Maricopa [^p3]

[^p3]: Telegram post: <https://t.me/RealSKeshel/1850>

Where Democrat votes are over 65% and trends look very abnormal.

<p markdown=1 class="cols">
  0192 DOBBINS RANCH  
  0224 ESCALANTE  
  0324 INDIAN SPRINGS  
  0556 RUBY  
  0040 BALSZ  
  0498 PEPPERWOOD  
  0374 LOMA LINDA  
  0247 GARDEN GROVES  
  0608 SOLANO  
  0403 MARLETTE  
  0332 JENTILLY  
  0099 CENTRAL HIGH  
  0707 WARNER  
  0207 DWIGHT  
  0476 PALMER  
  0686 TWIN BUTTES  
  0482 PARADA  
  0448 NORTH HIGH  
  0314 HONDA  
  0681 TULANE  
  0387 LUKE  
  0481 PAPAGO PARK  
  0071 CAIRO  
  0248 GARDENS  
  0166 DARROW  
  0406 MARYLAND  
  0725 WHITTIER  
  0675 TOWNLEY  
  0349 KOMATKE  
  0299 HICKIWAN  
</p>

##### Top 10 Filthiest Precincts In Maricopa [^p4]

[^p4]: Telegram post: <https://t.me/RealSKeshel/1851>

<div markdown=1 style="font-size: 70%" class="paragraph-margin-bottom">

  0314 HONDA  
  Classification: Dem Machine  
  -18.03% margin loss for Trump, 4 new Trump votes, 527 new Dem votes
  
  0248 GARDENS  
  Classification: Dem Machine  
  -17.59% margin loss for Trump, 29 new Trump votes, 356 new Dem votes
  
  0381 LONGMORE  
  Classification: Competitive  
  -15.66% margin loss for Trump, 138 new Trump votes, 423 new Dem votes
  
  0145 COUNTRY GABLES  
  Classification: MAGA  
  -14.56% margin loss for Trump, 71 new Trump votes, 183 new Dem votes
  
  0477 PALO BLANCO  
  Classification: Republican  
  -14.29% margin loss for Trump, 218 new Trump votes, 511 new Dem votes
  
  0549 ROLLING HILLS  
  Classification: Competitive  
  -14.01% margin loss for Trump, 284 new Trump votes, 937 new Dem votes
  
  0310 HOLLIS  
  Classification: Competitive  
  -13.79% margin loss for Trump, 111 new Trump votes, 610 new Dem votes
  
  0057 BONANZA  
  Classification: Republican  
  -13.39% margin loss for Trump, 39 new Trump votes, 201 new Dem votes
  
  0252 GENE AUTRY  
  Classification: Republican  
  -13.37% margin loss for Trump, 467 new Trump votes, 502 new Dem votes
  
  0055 BLUEBIRD  
  Classification: Republican  
  -13.15% margin loss for Trump, 411 new Trump votes, 1,015 new Dem votes

</div>


{:.info}
See also: [Maricopa Forensic Audit Results](/in-detail/maricopa-arizona-forensic-audit-report-results/) for a summary of the results, video presentations and links to the complete reports.



### Footnotes & References
