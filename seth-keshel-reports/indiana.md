---
title: Indiana Election Analysis by Seth Keshel
state: Indiana
abbr: in
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Some surprises.  Marion (Indianapolis) trends clean, with a vote total near Obama’s 2008 number with a decent population growth and Dem trend.  Hamilton County is marked yellow at about 8k heavy, with recent reports in news about issues.

Lake County is most fraudulent, with about 25k minimum excess for Biden (with huge Trump gain, the trend, and population decline – that doesn’t work out).

Honor roll:

Lake (25k)  
Hamilton (8k)  
Porter (6k)  
Hendricks (4k)  
Johnson (4k)

If Biden is 74k heavy, an accurate Trump margin is 19.0%, or 58.5% to 39.5%, and 562k votes.

Best GOP county audit targets – Hamilton, Hancock, Porter, Johnson, Clark

Most important to audit: Lake


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/101>

----

{% include seth_worst_counties_for_state heading=true %}

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
