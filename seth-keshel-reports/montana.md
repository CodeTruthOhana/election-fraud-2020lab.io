---
title: Montana Election Analysis by Seth Keshel
state: Montana
abbr: mt
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Trump added 64k votes in 4 years, a GOP record, but curiously, Biden, while losing by 16.4%, gained 67k votes as the state registered just 58k new voters.

I think target here was actually senate race which was an 11% race between Bullock and Daines; also a gubernatorial race.  Keep in mind, there is a big scrap in Missoula County right now over about 6,000 ballots thought to be illegally cast.

Montana does not register by party, so using working class/population growth trends.

Red – Obviously Ugly  | (6 counties)  
Yellow – Suspect/Likely Fraud  | (19 counties)  
Green – Clean |  (31 counties)

Estimates for excess votes based on pop. growth/trends:

Cascade | 3k
Flathead | 5k
Gallatin | 7k
Lewis and Clark | 3k
Missoula | 6k
Yellowstone | 6k

I estimate 46k excess votes statewide.  If accurate, and counting ONLY excess Biden vote growth, Trump should have won this state about 61.6% to 35.6%, or 26.0% - in other words, a shellacking that tracks accurately with the progression from 2012 to 2016.


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/23>


![](/images/seth-keshel/mo-chart.jpg)

The election fraud in Montana from 2020 is very easy to see in this trend graph.

Notice the opposing trends (how the lines move in opposing directions), something that is present since the high turnout era began in 2004.  

Obama ran it tight in 2008 (lost by only 2.4%) and then the Republican/GOP trend picked up steam, with a 13.7% Romney win, 20.4% Trump win...

And then we see a GOP record vote gain of 64k this year... Only to be met with a record Democrat gain of 67k... after two consecutive elections of losing votes -- in spite of population growth of 100k per decade.

This makes Montana one of the most fraudulent states in terms of percentage of fraudulent votes. My light estimate is 46k fraudulent votes, about 8% of all votes.

{:.caption}
Source: [Telegram post](https://t.me/RealSKeshel/1423), Sep 22, 2021

----

{% include seth_worst_counties_for_state heading=true %}

{% assign events = site.data.events | where: 'state', page.abbr %}
{% include events %}


## Further Updates

{% include tg-embed url="https://t.me/RealSKeshel/4563" %}


{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
