---
title: Illinois Election Analysis by Seth Keshel
state: Illinois
abbr: il
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{% include seth_state_details.html %}

Many have been asking me to get working on non-contested states so we can get audit groups organized.  Here is a hotly requested one.

Trump is +301k from 2016, but loses the state by 80k extra thanks to Biden's 382k gain.

Important to note that Biden is +53K from Obama (from IL) 2008 total, while Trump is +416k from McCain and +311k from Romney... In a state with a 0.1% DECREASE in population since 2010.

102 Counties  
10 Red (Obviously Rigged)  
16 Yellow (Likely/Minor Rigged)  
76 Green (Clean) - see Greene and Jersey for textbook Rust Belt Shift examples since 2008

Most of the machinery is connected to Chicago, no surprise.  Smaller network in center of state/Champaign area.

Stronger one across river from St. Louis.

I estimate Biden is 195K too heavy before we count Cook, where he is +97K over Obama '08.  The question for Cook is "HOW MUCH?"

Likely not a Trump state, but single digits blue.


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/20>

----

{% include seth_worst_counties_for_state heading=true %}

{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Illinois

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which discusses how Illinois unexpectedly gained a significant number of overall votes, despite a decrease in population

* Illinois Leaks reports that DuPage County signed a contract with Dominion Systems which includes a clause that they will resist all Freedom of Information (FOIA) requests, where possible, and inform Dominion of all such occurrences. [Read their report](https://edgarcountywatchdogs.com/2021/01/dupage-county-clerk-signed-anti-transparency-contract-with-dominion-voting-systems-dominion-encouraged-county-to-resist-disclosure-of-information/).


{% include canvass/get-involved %}

{% include state_telegram_channels %}
