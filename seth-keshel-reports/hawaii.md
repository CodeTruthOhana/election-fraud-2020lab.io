---
title: Hawaii Election Analysis by Seth Keshel
state: Hawaii
abbr: hi
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

{% include seth_state_details.html %}


Looking at Hawaii trends we see that Obama set the previous record at 325k votes, with Republicans topping out at 121k. The main voting center of the state has only grown by 2% in population over the entire decade, yet Biden somehow smashed that previous record. Were people just waiting for someone like Biden this entire time?

Trump’s gain of 68k in Hawaii is a GOP record.  Hawaii added 76k new voters since 2016, and when viewing the trend, there has been a tightening since 2008, with loss of Democrat votes all around since Obama’s first run.  Historically, Republican incumbents run stronger in Hawaii during re-election (Reagan won it and George W. Bush had 45%).

Estimated number of excess (potentially fraudulent) votes by county:

Hawaii | 10k
Maui | 10k
Kauai | 5k
Honolulu | 30k

With Trump receiving 197k votes, I would estimate that Biden is 55k higher than would be thought possible --- and this is generous to Biden, assigning all third party totals from 2016 to his column.  A more accurate margin considering *only* these excess votes (not considering any Trump votes potentially removed) is about 21.9%, with Trump near 40% in Honolulu.

All counties should be audited.


![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/8>


{% include seth_worst_counties_for_state heading=true %}


## Charting the Trends

{% include tg-embed url="https://t.me/RealSKeshel/3470" %}

{% include tg-embed url="https://t.me/RealSKeshel/3471" %}

{% include tg-embed url="https://t.me/RealSKeshel/3472" %}


## Kauai County

{% include tg-embed url="https://t.me/RealSKeshel/4245" %}

{% include tg-embed url="https://t.me/RealSKeshel/4125" %}



{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}


## Further Updates

{% include tg-embed url="https://t.me/RealSKeshel/7238" %}


{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which discusses how Hawaii unexpectedly gained a significant number of overall votes, despite a decrease in population

{% include canvass/get-involved %}

{% include state_telegram_channels %}
