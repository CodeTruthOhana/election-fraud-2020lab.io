---
title: Oklahoma Election Analysis by Seth Keshel
state: Oklahoma
abbr: ok
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}


Something seems off when Trump gains 71k new votes in Oklahoma, and Biden cranks out 84k, especially when *every single county trends more Republican in registration*, with that indicator forecasting wider margins each election since 2008.

Biden is at least 39k heavy per trend/population/registration analysis.  Oklahoma County (OKC) is the big issue and had a contested house race.  Republicans outregistered Democrats 5 to 3 after Trump won it by 10.5% in 2016, but Trump somehow barely held the county.  The numbers suggest electronic fraud, since the Democrat gain of 30k is an all-time high.

Tulsa is also ugly, Democrats down over 300 net on the rolls, Republicans up over 5k new registrants, Democrats gain record 21k.  Cleveland County, Democrat net loss in registrations, Republicans +5.6k, record gain of 10k (7k previous).

Biden appears to be 39k heavy in Oklahoma.

If 39k heavy, Trump would be 67.0% to 30.6%, or 555k.

Most in need of audits – Oklahoma, Tulsa, Cleveland


{:.caption}
Source: <https://t.me/RealSKeshel/709>


{% include seth_worst_counties_for_state heading=true %}


## Oklahoma County Heat Map

<!-- Heat map to be embedded here later, when time permits -->

{% include tg-embed url="https://t.me/RealSKeshel/2838" %}



{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}


## Further Updates

{% include tg-embed url="https://t.me/RealSKeshel/7226" %}


{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
