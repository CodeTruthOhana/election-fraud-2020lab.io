---
title: Utah Election Analysis by Seth Keshel
state: Utah
abbr: ut
last_updated: 8 Feb 2022
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Utah is one of hardest to gauge, thanks to poor record keeping (Utah registers by party, but not available across counties), the presence of a serious 3rd party challenger in 2016, and Romney’s home state skew.  Most accurate comps go back to 2008.

I think Utah was a target by central planners – Biden has 45k more than Trump ’16; perhaps they anticipated the same 3rd party behavior.  Trump is 125k past Romney ’12, but Biden’s gains are 250k from Clinton, when previous high gain was Obama ’08 with 87k.  Nearly all third party throwaways came from Republicans in 2016.

Red | Obviously Ugly | (5 counties)  
Yellow | Suspect/Likely Fraud | (8 counties)  
Green | Clean | (16 counties)

Estimates for excess votes based on population growth/trends:

Salt Lake | 50k
Utah | 30k
Davis | 20k
Weber | 9k
Washington | 7k

I estimate 131k excess votes.  If accurate, and counting ONLY excess Biden vote growth, Trump should have won this state about 63.7% to 31.6%, or by 32.1%.  Biden's gain in Salt Lake City was a massive 114k, when the previous largest gain was 31k by Obama in 2008. This should be looked into.


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/25>


## Historical Growth

![](/images/seth-keshel/ut-graph.jpg)

These are votes by party per election. Red Republican, Blue Democrat, Purple Other. 

* Look at how unpopular Trump was in 2020 compared to Romney 2012 (sarcasm).

* Now look at that *massive* Democrat increase after they were stuck three elections in a row. It likely should have been much less.

* The large jump in 2016’s third-party vote was likely 85% from Republicans who didn't want to vote for Trump.


{% include seth_worst_counties_for_state heading=true %}


## Unexpected Surge in Mail-In Ballots

{% include tg-embed url="https://t.me/RealSKeshel/3237" %}



## Events

* Seth presented in Farmington, Utah on Sept 2, 2021. See [his Telegram post](https://t.me/RealSKeshel/1160) and [this follow-up video](https://t.me/RealSKeshel/1206).

  <script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/1202" data-width="100%"></script>

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
