---
title: Arizona Election Analysis by Seth Keshel
state: Arizona
abbr: az
last_updated: 23 May 2023
---

{:.info}
*See also the main article on [Arizona]({% link _states/arizona.md %}), for documented election integrity issues, further reports, and updates on the Forensic Audit.*

{% include seth_report_intro.html %}

{% include toc %}

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/v1c185y/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/v1ene36-episode-16-arizona-state-overview.html)


![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}


{% include seth_state_details.html %}

## Summary

* No GOP candidate has *ever* exploded in votes like Trump in Arizona with a gain of 410,000 in a state that has been blue just once since 1948.  

* Trump set a GOP record in gaining of 248,000 new votes in Maricopa County, and yet still lost it (first Republican loss since 1972).

* Biden gained a massive 511,000 votes in one election cycle, which blows away the previous record when Kerry added 209,000 in the first election of the higher turnout era in 2004, and yet Biden only won by 10,500 votes, which suggests they knew exactly how many votes they needed to dump into the mix to win

Red | Obviously Ugly | 2 counties
Yellow | Suspect/Likely Fraud | 4 counties
Green | Clean | 9 counties

Estimates for excess (potentially fraudulent) Biden votes based on population growth and new party registration data:

Maricopa | 175k
Pima | 35k

If Biden is 220,000 votes heavy in Arizona, counting only excess votes, Trump would have carried the state 52.4% to 45.8%, for a margin of 6.6%, which aligns with a Baris poll about 2 weeks before the election.

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/10>


{% include seth_worst_counties_for_state heading=true %}


## Vote Surge in 2020

The following chart shows the surge in votes for Donald Trump in Arizona in 2020.

![](/images/seth-keshel/az-chart.jpg)

If election forecasting were a game of Blackjack, and the dealer had his one card face up, and that card was showing 409,000 more Trump votes than in 2016, and you saw a graph like this showing the GOP flat but still winning Arizona in every election of the high-turnout era (2004 to present)...

*How much money would you have bet on Trump holding Arizona?*

Remember that Obama gained 141k votes and Clinton gained 136k.

What would you expect Biden's result to be?

{:.caption}
Source: [Telegram Post](https://t.me/RealSKeshel/3067)



## Maricopa County

[View Seth's Maricopa County Report](maricopa-county/){:.button}

See also: [Maricopa Forensic Audit Results](/in-detail/maricopa-arizona-forensic-audit-report-results/) for a summary of the Cyber Ninjas forensic audit, including video presentations and links to the complete reports.


## Pima County

[View Seth's Pima County Report](pima-county/){:.button}


## Cochise County

Cochise County, AZ is home to Fort Huachuca - an Army Intel Center and my former 2x duty station.

Here is an example of a county that is 4-5k heavy in Biden votes, potentially half the "margin" in AZ.

[![](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/uploads/ec09e2d195e8798aead156950ae5b7d4/image.png)](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/uploads/ec09e2d195e8798aead156950ae5b7d4/image.png)

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vjbycs/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/RealSKeshel/844), Aug 10, 2021

{% include tg-embed url="https://t.me/RealSKeshel/3317" %}



## Gila County

{% include tg-embed url="https://t.me/RealSKeshel/3285" %}


## Yavapai County

{% include tg-embed url="https://t.me/RealSKeshel/3196" %}


## Further County Breakdowns

We've also analyzed some additional stats for counties in Arizona, being discussed in our Gitlab discussion area:

  [View the draft report](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/issues/14){:.button}


{% comment %}
## County-Level Stats

{% include county_stats.html state=page.state %}
{% endcomment %}

## Video Updates on Arizona

Seth's presentation on Arizona, with some specifics on Pima County, AZ:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/voa6ac/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

Interview on Bannon's War Room:

<div class="responsive-iframe"><iframe sandbox="allow-scripts" security="restricted" title="Military Intelligence Proves Stolen Election In Multiple States" src="https://rumble.com/embed/vhcu76/?pub=4#?secret=YNzQhcao1Q" data-secret="YNzQhcao1Q" frameborder="0" allowfullscreen></iframe></div>

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vi4efz/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

*If anyone is able to assist with a written or dot-point summary of the above videos, we'd love to add those to this page. Connect on [Telegram]({{ site.data.globals.telegramLink }}) or [Gitlab]({{ site.data.globals.gitlabIssuesLink }}) if you can assist.*


## Possible Voter Roll Manipulation

A number of states -- both key competitive states and not -- have machined/trimmed/manipulated their voter rolls for the desired outcomes.  Here are some examples:

**ARIZONA**

**Maricopa County** | 2004-08 | +179k | (+64k D, +16k R)
| 2008-12 | +87k | (-34k D, - 6k R)
| 2012-16 | +344k | (+108k D, +86k R)
| 2016-20 | +433k | (+196k D, +148k R)
**Pima County** | 2004-08 | +49k | (+21k D, +10k R)
| 2008-12 | -1k | (-13k D, -4k R)
| 2012-16 | +48k | (+21k D, +9k R)
| 2016-20 | +96k | (+52k D, +22k R)

Katie Hobbs is almost certainly stuffing voter rolls with phantoms, not just with Maricopa, but Pima, too.  Arizona needs to do a thorough canvassing – we have a historically red state with historic Republican gains in registration, that has been growing a lot for decades, with numbers this lopsided.  Maricopa looks like it may have also been manipulated in 2016.

{:.caption}
Source: <https://t.me/RealSKeshel/788>


## Further Reports

{% include tg-embed url="https://t.me/RealSKeshel/4158" %}

{% include tg-embed url="https://t.me/RealSKeshel/5803" %}

{% include tg-embed url="https://t.me/RealSKeshel/6793" %}

{% include tg-embed url="https://t.me/RealSKeshel/6799" %}

{% include tg-embed url="https://t.me/RealSKeshel/6817" %}

{% include tg-embed url="https://t.me/RealSKeshel/6822" %}

{% include tg-embed url="https://t.me/RealSKeshel/6823" %}

{% include tg-embed url="https://t.me/RealSKeshel/6886" %}

{% include tg-embed url="https://t.me/RealSKeshel/6917" %}

{% include tg-embed url="https://t.me/RealSKeshel/6918" %}




{% include seth_reports_methodology %}

{% include raw_data %}


## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Arizona

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which includes a section on {{ page.state }}

* See also the main [Arizona](/fraud-summary-by-state/arizona/) article for more

{% include canvass/get-involved %}

{% include state_telegram_channels %}

{% include county_stats_script.html state=page.state %}


