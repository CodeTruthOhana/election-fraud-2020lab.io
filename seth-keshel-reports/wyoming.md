---
title: Wyoming Election Analysis by Seth Keshel
state: Wyoming
abbr: wy
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

First [Idaho](../idaho/), now Wyoming -- these two states avoided the simulation of 2020 and give an accurate view of what the true dynamics of the election were.  Biden has fewer votes in Wyoming than Obama did in 2008, when he lost the state by 32%.  

Contrast this with a vote total in [Utah](../utah/), nearly double Obama’s total from 2008, and a very strong vote gain in [Montana](../montana/), and you see that Wyoming was not part of the game plan.

Yellow – Suspect/Likely Fraud | (1 county)  
Green – Clean | (22 counties)

**Albany County**, home to University of Wyoming, is the only one that pings on my yellow list, and perhaps only for about 1,000 votes, enough to flip the county from red to blue, despite a Trump gain and a slight GOP registration edge over the past four years, in a county won by the last two Republican nominees.

A very clean, nice example of another Rocky Mountain state trending in the 2008-2012-2016 fashion, as predicted by party registration totals.


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/31>

----

{% include seth_worst_counties_for_state heading=true %}

## Further Updates

{% include tg-embed url="https://t.me/RealSKeshel/7226" %}


{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
