---
title: Tennessee Election Analysis by Seth Keshel
state: Tennessee
abbr: tn
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}


![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

{% include seth_state_details.html %}

Very troubling map of Tennessee.  

Trump received 330k more votes than in 2016, a GOP record increase even above the low turnout '00 election to '04.  For comparison: Obama increased +51k from Kerry, down 127k, Clinton down 90k, even with MASSIVE GROWTH in Tennessee.  Now Biden is up 273k, with at least 101k excess.  The state was a wipeout, even giving Biden a modern record vote gain for Democrats.

Shelby County (Memphis) isn’t too far from the trend.  Davidson should have huge Democrat growth and does, flagging it for 15k excess (lenient).  Previous high gain for Democrats is 16k; Clinton up 5k, and that came with 13k Republican loss in 2016.  Trump is up 16k, with Biden up 51k – though I’d expect a large surge there.

The most significant areas are in proximity to Davidson, with Knox being another spot with some spread.  Hamilton is obvious, at least 10k heavy.

**Best Trump county audit targets:** Hamilton, Knox, Rutherford.

If Biden is 101k heavy, Trump margin would be 62.7% to 35.3%, or 809k.


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/118>


{% include seth_worst_counties_for_state heading=true %}


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/118>


## Seth's Presentation in Murfreesboro, TN

The following video from an election integrity event in Murfreesboro, Tennessee (Jan 22, 2022) includes a presentation from Seth Keshel, beginning at the 1:40:30 mark.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vqhnkk/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vt3tog-election-integrity-dr-frank-professor-clements-and-captain-seth-keshel-1-22.html)

A further [6min interview with Seth](https://tv.gab.com/channel/justadanminute/view/election-integrity-seth-keshel-61f09265e523fab9bd3ad4b7) was also recorded after the event.


## Further Updates

{% include tg-embed url="https://t.me/RealSKeshel/3037" %}

{% include tg-embed url="https://t.me/RealSKeshel/5011" %}



{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
