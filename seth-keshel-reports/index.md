---
title: Election Analysis by Seth Keshel
breadcrumb: "[Follow the Data](#colophon) ›"
last_updated: 20 Nov 2022
comments_enabled: false
covered_telegram_posts_up_until: 4 Mar 2022
---

Former US military intelligence officer and statistical analyst Seth Keshel reportedly predicted the 2016 election outcome accurately in all fifty states.

Following the November 2020 Presidential Election he analyzed the trends of voter registrations versus actual votes and discovered alarming anomalies in a number of counties, ones that defy typical historical trends. He has published reports by county and by state that indicate which counties' vote counts align with the trend in voter registrations and which have small or large divergences.

Seth has been an advisor to Former Lt. General Michael Flynn and Federal Lawyer Sidney Powell.

{% include toc %}

In the following pages you will find maps and charts that flag each county as red, yellow or green, based on whether the 2020 vote totals aligned with the trends, or diverged in statistically unlikely ways:

{% include seth_chart_legend.html %}

![USA Election Fraud Map](/images/seth-keshel/usa-map.jpg)

{:.caption}
Seth has also posted a revised map, see [this Telegram post](https://t.me/RealSKeshel/7230).

Where a large divergence is detected (red), this does not in itself prove fraud, but is a clear flag that the numbers should be investigated further.

Likewise, just because something is green doesn’t mean there are no issues with the county. The analysis considers trends, and Seth considers it "bulletproof" when he has access to party registration numbers. If a county follows the same trends as the past years and current numbers suggest, it goes as green. Audits of green counties might be considered lower priority.


## Reports by State

{:.small.muted}
Click a column heading to sort by that column.

{% include seth_reports_list_v2.html %}


The table above shows the official vote counts, plus the estimated  number of excess Biden votes --- this is the number of votes that are oddly above what the trends would suggest. These are based on trend analysis in the modern political era, considering population growth/decline, recent voter history, and registration information, including registration by party.

Seth explains that his estimates are always lenient, and do not account for cyber-attacks that flip votes more subtly in ways that avoid statistical detection.

{:.more-padding}
**Flipped States** | Based on this data, if fraudulent votes are removed, Trump would have actually won these states: <br>[Pennsylvania](pennsylvania/), [Michigan](michigan/), [Wisconsin](wisconsin/), [Nevada](nevada/), [Arizona](arizona/), [Georgia](georgia/), [Minnesota](minnesota/), and thus retained the Presidency.
**Potentially-Flipped States** | It's also possible that Trump may have won additional close-call states if there was additional fraud such as cyber-attacks occurring: [New Mexico](new-mexico/), [Virginia](virginia/), [Colorado](colorado/), [New Jersey](new-jersey/), [New Hampshire](new-hampshire/).
**Close Calls** | These states would still have likely been won by Biden, but may have been closer than what was reported: [Washington](washington/), [Oregon](oregon/), [Rhode Island](rhode-island/), [Connecticut](connecticut/), [Hawaii](hawaii/).
**Cleanest State** | [Iowa](iowa/)
**Mostly-Clean States** | [Idaho](idaho/), [Wyoming](wyoming/), [Arkansas](arkansas/), [South Dakota](south-dakota/), [North Dakota](north-dakota/), [Mississippi](mississippi/)
**Worst States** | [California](california/), [New York](new-york/), [New Jersey](new-jersey/), [Arizona](arizona/), [Washington](washington/), [Oregon](oregon/), [Massachusetts](massachusetts/), [Pennsylvania](pennsylvania/), [Michigan](michigan/), [Wisconsin](wisconsin/), [Georgia](georgia/), [Nevada](nevada/), [Minnesota](minnesota/)


## Top 100 Worst Counties

Here is Seth's graphic showing, in his opinion, the Top 100 counties we must secure Full Forensic Audits in in order to trust or restore the integrity of our election system:

[![Top 100 worst counties, election fraud](/images/seth-keshel/top-100.jpg)](/images/seth-keshel/top-100.pdf)

<div class="small paragraph-margin-bottom" style="column-width: 11.7em" markdown="1">

1. MARICOPA <small>[AZ]({% include seth_link_by_short_name state="AZ" %})</small>
2. PIMA <small>[AZ]({% include seth_link_by_short_name state="AZ" %})</small>
3. CHEROKEE <small>[GA]({% include seth_link_by_short_name state="GA" %})</small>
4. COBB <small>[GA]({% include seth_link_by_short_name state="GA" %})</small>
5. DEKALB <small>[GA]({% include seth_link_by_short_name state="GA" %})</small>
6. FORSYTH <small>[GA]({% include seth_link_by_short_name state="GA" %})</small>
7. FULTON <small>[GA]({% include seth_link_by_short_name state="GA" %})</small>
8. GWINNETT <small>[GA]({% include seth_link_by_short_name state="GA" %})</small>
9. GENESSEE <small>[MI]({% include seth_link_by_short_name state="MI" %})</small>
10. KENT <small>[MI]({% include seth_link_by_short_name state="MI" %})</small>
11. LIVINGSTON <small>[MI]({% include seth_link_by_short_name state="MI" %})</small>
12. MACOMB <small>[MI]({% include seth_link_by_short_name state="MI" %})</small>
13. OAKLAND <small>[MI]({% include seth_link_by_short_name state="MI" %})</small>
14. WASHTENAW <small>[MI]({% include seth_link_by_short_name state="MI" %})</small>
15. WAYNE <small>[MI]({% include seth_link_by_short_name state="MI" %})</small>
16. ANOKA <small>[MN]({% include seth_link_by_short_name state="MN" %})</small>
17. DAKOTA <small>[MN]({% include seth_link_by_short_name state="MN" %})</small>
18. HENNEPIN <small>[MN]({% include seth_link_by_short_name state="MN" %})</small>
19. RAMSEY <small>[MN]({% include seth_link_by_short_name state="MN" %})</small>
20. SAINT LOUIS <small>[MN]({% include seth_link_by_short_name state="MN" %})</small>
21. WRIGHT <small>[MN]({% include seth_link_by_short_name state="MN" %})</small>
22. DOUGLAS <small>[NE]({% include seth_link_by_short_name state="NE" %})</small>
23. SARPY <small>[NE]({% include seth_link_by_short_name state="NE" %})</small>
24. CLARK <small>[NV]({% include seth_link_by_short_name state="NV" %})</small>
25. WASHOE <small>[NV]({% include seth_link_by_short_name state="NV" %})</small>
26. ALLEGHENY <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
27. BERKS <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
28. BUCKS <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
29. CHESTER <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
30. DELAWARE <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
31. ERIE <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
32. L’WANNA <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
33. LUZERNE <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
34. M’GOMERY <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
35. N’HAMPT. <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
36. PHIL. <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
37. W’MORELAND <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
38. YORK <small>[PA]({% include seth_link_by_short_name state="PA" %})</small>
39. DANE <small>[WI]({% include seth_link_by_short_name state="WI" %})</small>
40. MILWAUKEE <small>[WI]({% include seth_link_by_short_name state="WI" %})</small>
41. ROCK <small>[WI]({% include seth_link_by_short_name state="WI" %})</small>
42. WAUKESHA <small>[WI]({% include seth_link_by_short_name state="WI" %})</small>
43. MADISON <small>[AL]({% include seth_link_by_short_name state="AL" %})</small>
44. ANCHORAGE <small>[AK]({% include seth_link_by_short_name state="AK" %})</small>
45. FRESNO <small>[CA]({% include seth_link_by_short_name state="CA" %})</small>
46. KERN <small>[CA]({% include seth_link_by_short_name state="CA" %})</small>
47. ORANGE <small>[CA]({% include seth_link_by_short_name state="CA" %})</small>
48. S. BER’DINO <small>[CA]({% include seth_link_by_short_name state="CA" %})</small>
49. DOUGLAS <small>[CO]({% include seth_link_by_short_name state="CO" %})</small>
50. EL PASO <small>[CO]({% include seth_link_by_short_name state="CO" %})</small>
51. BREVARD <small>[FL]({% include seth_link_by_short_name state="FL" %})</small>
52. HILLSBOROUGH <small>[FL]({% include seth_link_by_short_name state="FL" %})</small>
53. LAKE <small>[FL]({% include seth_link_by_short_name state="FL" %})</small>
54. PASCO <small>[FL]({% include seth_link_by_short_name state="FL" %})</small>
55. VOLUSIA <small>[FL]({% include seth_link_by_short_name state="FL" %})</small>
56. HONOLULU <small>[HI]({% include seth_link_by_short_name state="HI" %})</small>
57. ADA <small>[ID]({% include seth_link_by_short_name state="ID" %})</small>
58. LAKE <small>[IL]({% include seth_link_by_short_name state="IL" %})</small>
59. LAKE <small>[IN]({% include seth_link_by_short_name state="IN" %})</small>
60. JOHNSON <small>[KS]({% include seth_link_by_short_name state="KS" %})</small>
61. SEDGWICK <small>[KS]({% include seth_link_by_short_name state="KS" %})</small>
62. KENTON <small>[KY]({% include seth_link_by_short_name state="KY" %})</small>
63. ST. TAMMANY <small>[LA]({% include seth_link_by_short_name state="LA" %})</small>
64. CUMBERLAND <small>[ME]({% include seth_link_by_short_name state="ME" %})</small>
65. HARFORD <small>[MD]({% include seth_link_by_short_name state="MD" %})</small>
66. MIDDLESEX <small>[MA]({% include seth_link_by_short_name state="MA" %})</small>
67. ST. CHARLES <small>[MO]({% include seth_link_by_short_name state="MO" %})</small>
68. YELLOWSTONE <small>[MT]({% include seth_link_by_short_name state="MT" %})</small>
69. ROCKINGHAM <small>[NH]({% include seth_link_by_short_name state="NH" %})</small>
70. OCEAN <small>[NJ]({% include seth_link_by_short_name state="NJ" %})</small>
71. BERNALILLO <small>[NM]({% include seth_link_by_short_name state="NM" %})</small>
72. ERIE <small>[NY]({% include seth_link_by_short_name state="NY" %})</small>
73. NASSAU <small>[NY]({% include seth_link_by_short_name state="NY" %})</small>
74. SUFFOLK <small>[NY]({% include seth_link_by_short_name state="NY" %})</small>
75. BRUNSWICK <small>[NC]({% include seth_link_by_short_name state="NC" %})</small>
76. MECKLENBURG <small>[NC]({% include seth_link_by_short_name state="NC" %})</small>
77. NEW HANOVER <small>[NC]({% include seth_link_by_short_name state="NC" %})</small>
78. WAKE <small>[NC]({% include seth_link_by_short_name state="NC" %})</small>
79. FRANKLIN <small>[OH]({% include seth_link_by_short_name state="OH" %})</small>
80. OKLAHOMA <small>[OK]({% include seth_link_by_short_name state="OK" %})</small>
81. JACKSON <small>[OR]({% include seth_link_by_short_name state="OR" %})</small>
82. MARION <small>[OR]({% include seth_link_by_short_name state="OR" %})</small>
83. KENT <small>[RI]({% include seth_link_by_short_name state="RI" %})</small>
84. HORRY <small>[SC]({% include seth_link_by_short_name state="SC" %})</small>
85. RUTHERFORD <small>[TN]({% include seth_link_by_short_name state="TN" %})</small>
86. BEXAR <small>[TX]({% include seth_link_by_short_name state="TX" %})</small>
87. COLLIN <small>[TX]({% include seth_link_by_short_name state="TX" %})</small>
88. DALLAS <small>[TX]({% include seth_link_by_short_name state="TX" %})</small>
89. DENTON <small>[TX]({% include seth_link_by_short_name state="TX" %})</small>
90. HARRIS <small>[TX]({% include seth_link_by_short_name state="TX" %})</small>
91. TARRANT <small>[TX]({% include seth_link_by_short_name state="TX" %})</small>
92. TRAVIS <small>[TX]({% include seth_link_by_short_name state="TX" %})</small>
93. SALT LAKE <small>[UT]({% include seth_link_by_short_name state="UT" %})</small>
94. UTAH <small>[UT]({% include seth_link_by_short_name state="UT" %})</small>
95. CHESTERFIELD <small>[VA]({% include seth_link_by_short_name state="VA" %})</small>
96. LOUDOUN <small>[VA]({% include seth_link_by_short_name state="VA" %})</small>
97. SPOTSYLVANIA <small>[VA]({% include seth_link_by_short_name state="VA" %})</small>
98. CLARK <small>[WA]({% include seth_link_by_short_name state="WA" %})</small>
99. PIERCE <small>[WA]({% include seth_link_by_short_name state="WA" %})</small>
100. SPOKANE <small>[WA]({% include seth_link_by_short_name state="WA" %})</small>

</div>

This was tough to narrow down from 132, so a few had to be kicked out.

Purple | the swing states in the electoral college: AZ, GA, MI, MN, NV, PA, WI
Red | Key audit targets in states won by Trump
Blue | Key audit targets in states won or "won" by Biden

In Red and Blue categories, he did not choose unrealistic targets (like Cook or Los Angeles Counties, which would be very difficult to audit).

{:.caption}
Source: [Telegram Post](https://t.me/RealSKeshel/1112), Aug 28, 2021


## What Makes This Work Accurate?

{:.small}
> The polls are not accurate. Anybody that understands anything about analytics realizes that polls suffer from something called "social desirability bias", where people don’t answer truthfully about which candidate they support or which party they support, which is how you end up with these 43-39 polls with 18 percent undecided. It doesn’t tell you anything. And they’re also made in some ways to suppress turnout, usually of conservatives. 
>
> "Polling is generally not an accurate thing it’s been that way for quite some time, but ***trend analysis*** is something that goes back decades as far back as these records are available, we’re talking about party registration data. It’s very telling in places like Florida, North Carolina, and Pennsylvania. That’s the most accurate predictor there is. And I was able to use data like that, including understanding dynamic shifts and trends to predict the 2016 election perfectly including, Pennsylvania, Michigan, and Wisconsin. And the data line over the same exact fashion this year was huge Republican expansion and gains in places like Florida, North Carolina, and Pennsylvania. So all the data suggests that these same states [were] turning more Republican. In Georgia, of course, we had a lot of issues on election day. But trend analysis is accurate, it’s plugged into real time, and it identifies behavior, registration data and population."
>
> <small>--- From [interview with Seth Keshel](https://www.thegatewaypundit.com/2021/08/captain-seth-keshel-explains-election-analysis-methods-1-issue/), on the Gateway Pundit, Aug 5, 2021</small>


## The Ten Points To True Election Integrity

Here is Seth's 10-point strategy to create clean elections in the future:

1. All voter rolls dumped and all legitimate voters re-registered with appropriate means of confirmation to eliminate illegal or double registering of voters. 

2. Ban on all electronic elections equipment: poll books, voting machines, tabulators. 

3. Paper and pen/pencil balloting, granted only with valid ID. 

4. Mail balloting illegal with exception of overseas military, legitimately disabled with appropriate approval and supervision, or professionals unable to vote on Election Day (doctors, pilots, etc).

5. No early voting. Counter with more and smaller precincts. 

6. Precincts under 1,000 voters. 

7. Ballot harvesting illegal and punishable by a decade in prison. 

8. Election Day as a state or national holiday. A populist compromise that will eliminate fraud through the means of having weeks or over a month to execute. 

9. Winners announced by midnight. Somehow FL and TX can tally 10mm+ votes 90 minutes after polls close but PA needs 4 days and CA 4 weeks. 

10. Ten-year prison terms for anyone caught restricting access for members of any party to observe election procedure (e.g. TCF Center, etc.), ballot harvesting, repeat scanning ballots, or any other means of distorting an election.

{:.caption}
Source: [Telegram Post](https://t.me/RealSKeshel/3218), Jan 10, 2022


## Other Reports

General Flynn and Seth Keshel collaborated on an article in The Western Journal: 

**[10 Indisputable Facts on the 2020 Election That Argue for Audits](https://www.westernjournal.com/gen-flynn-exclusive-10-indisputable-facts-2020-election-argue-audits/)**

He calls it "a no nonsense, cold, hard look at facts, trends, and indicators going as far back as 1892".



## Video Presentations 

> It's not that we're trying to scream a certain outcome right now, it's that we're wondering "Why is there no investigation into this? Why is nobody asking these questions?"
>
> <small>-- Seth Keshel, in [Doug Wade Interviews Seth Keshel](https://www.youtube.com/watch?v=xXMW9VNMPT4), Jan 2, 2021</small>

In addition to the state-specific videos shown in the list above, these other videos refer to the US election in general:

* [Doug Wade Interviews Seth Keshel](https://youtu.be/xXMW9VNMPT4), Jan 2, 2021. This provides a good overview of Seth's methodology, using Florida and Pennsylvania as examples. It also covers the anomalies in "bellwether" counties and states (e.g. Ohio, Iowa, Florida and North Carolina) that traditionally predicted who would win the Presidency with 92-100% accuracy, but despite them predicting a Trump win, the end result went to Biden.

* [Captain Seth Keshel: The Numbers Don't Lie](https://rumble.com/vjmyud-captain-seth-keshel-the-numbers-dont-lie.-the-election-was-stolen..html) - interview on The Professor's Record with David K. Clements, July 9, 2021, containing more background on Seth Keshel and his work with Lt. General Michael Flynn, Sidney Powell and Lin Wood. They then discuss the trends and results in Arizona, Michigan, Pennsylvania, Georgia, and Florida.

* [Seth explains the 6 different types of counties and their voting behavior](https://t.me/RealSKeshel/584), Telegram, July 26, 2021

* [Seth’s Presentation to Tarrant County](https://rumble.com/vkiewb-seth-keshel-behind-the-election-integrity-curtain-july-29-2021.html), Dallas/Fort Worth, Texas, July 29, 2021

* [The Capt. Seth Keshel Interview Part 1: Who Is Captain K?](https://rumble.com/vkzbp8-the-capt.-seth-keshel-interview-part-1-who-is-captain-k.html?mref=jfdjj&mc=f4qua), J Slay USA, Aug 10, 2021. See also [Part 2](https://rumble.com/vl06eu-the-captain-seth-keshel-interview-part-2-understanding-election-trends-and-.html) and [Part 3](https://rumble.com/vl2a0f-the-seth-keshel-interview-part-3-irrefutable....html?mref=jfdjj&mc=f4qua).

* [Seth Keshel's Presentation at Cyber Symposium: "Behind The Election Corruption Curtain"](https://rumble.com/vl12q1-mike-lindell-cyber-symposium-behind-the-election-integrity-curtain-seth-kes.html), Aug 11, 2021. Powerpoint slides can be downloaded from [this Telegram link](https://t.me/RealSKeshel/984).

* [Voter Trends Expose Signs Of Tampering In The Recent US Election: Alan Keyes with Seth Keshel](https://www.brighteon.com/4bea3814-147c-4243-acb9-055ae5c971b7), Aug 12, 2021.

* [The Daily 302 Show 2A with Guest Captain Seth Keshel](https://rumble.com/vlukw3-the-daily-302-show-2a-with-guest-captain-seth-keshel.html), Aug 29, 2021. They discuss the article with General Flynn, Kansas, Alaska, New Mexico, and other general issues.

* [Fake President: Keshel Reveals Indisputable Election Fraud Evidence](https://www.redvoicemedia.com/video/2021/10/fake-president-keshel-reveals-indisputable-election-fraud-evidence/), Oct 27, 2021. The Stew Peters Show.

* [Seth Keshel Describes the Six Major County Types](https://rumble.com/vqd2dt-election-integrity-seth-keshel-describes-the-six-major-county-types.html), Nov 26, 2021

* [Stew Peters Show: We The People Have ALL The Power, TODAY We Change The Game](https://rumble.com/vqwtk0-must-see-we-the-people-have-all-the-power-its-time-we-use-it.html), Dec 16, 2021. Seth is interviewed from the 24min mark.

* [Why The Keshel Rating System is Paramount To Exposing Election Fraud!](https://rumble.com/vsuta0-why-the-keshel-rating-system-is-paramount-to-exposing-election-fraud.html), Jan 18, 2022. Interview with Jovan Pulitzer.

* [Cross Talk: America First Patriot Seth Keshel Shares Cure to 2020 Election Corruption](https://www.redvoicemedia.com/2022/01/america-first-patriot-seth-keshel-shares-cure-to-2020-election-corruption/), Jan 25, 2022. 

* [The Fight Continues - Zak Paine Interviews Seth Keshel](https://rumble.com/vuc5mc-the-fight-continues-zak-paine-interviews-seth-keshel.html), Feb 7, 2022.

* [Seth's website](https://captk.com/#recent-appearances) also displays some of his recent appearances and interviews

* Many more videos are [available on Rumble.com](https://rumble.com/search/video?q=seth%20keshel)


{% assign events = site.data.events | where_exp: "e", "e.who contains 'Seth Keshel'" %}
{% include events title="Upcoming Events & Presentations" hidePastEvents=true %}

{% if events.size > 0 %}
Seth also lists upcoming events and appearances [on his website, here](https://captk.com/calendar), which may be more up-to-date than the list above.
{% else %}
## Upcoming Events & Presentations
Seth lists upcoming events and appearances [on his website, here](https://captk.com/calendar).
{% endif %}



## Identify Electoral Fraud Yourself Using Trend Analysis

Following Seth's trend analysis work, our independent team has dived further into the data and surfaced numerous irregularities. If you're interested in learning more about Seth's process and what else we've uncovered, see the following series of articles:

{% include article_series from="/trend-analysis/" title=false %}



## Raw Data

{% include state_election_stat_source %}

{% include us_election_atlas %}


## Statement by Donald J. Trump

> Highly respected Army intelligence captain, Seth Keshel, has just released his Report on National Fraud Numbers with respect to the 2020 Presidential Election. I don’t personally know Captain Keshel, but these numbers are overwhelming, election-changing, and according to Keshel, could be even bigger in that they do not account for cyber-flipping of votes. They show I won the election—by A LOT! Now watch the Democrats coalesce, defame, threaten, investigate, jail people, and do whatever they have to do to keep the truth from surfacing, and let the Biden Administration continue to get away with destroying our Country. The irregularities and outright fraud of this election are an open wound to the United States of America. Something must be done—immediately!"
>
> <small>August 4, 2021, via [Telegram](https://t.me/real_DonaldJTrump/13810) and [Gab](https://gab.com/realdonaldtrump/posts/106695256682894722)</small>


## Follow Seth's Updates

ElectionFraud20.org is not directly affiliated with Seth Keshel. We found his reports insightful, and have republished them online to make them accessible to a wider audience.

For the latest reports and updates from Seth, find him at:

* [@RealSKeshel](https://t.me/RealSKeshel) on Telegram, where he provides general updates about the election and subsequent audits. His early maps, stats and graphics were also posted under [Captain K’s Maps and Stats](https://t.me/CaptainKMapsandStats).

* [CaptK.com](https://captk.com/) -- personal website with calendar and booking information

* [Substack: Captain K's Corner](https://skeshel.substack.com/) -- articles and deeper analysis


{% include canvass/get-involved %}

----

<small>*Comments have been disabled for the time being as the discussion was not proving constructive, and we currently lack the resources to moderate it effectively. Feel free to send feedback via [Telegram](https://t.me/ElectionFraud20_org) or [Twitter](https://twitter.com/SiResearcher).*</small>