---
title: New Mexico Election Analysis by Seth Keshel
state: New Mexico
abbr: nm
last_updated: 9 Feb 2022
---

{% include see_main_article %}

{% include seth_report_intro.html %}

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhgg1p/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

{% include seth_state_details.html %}

The first thing "off" about New Mexico is that it bucked the statewide registration trend predictor.  Democrats are going out of business here as well in most counties except Bernalillo (Albuquerque; voter roll is down there by over 3k), Dona Ana, and Santa Fe.  Trump GOP cut 1.6% off Democrat registration advantage, which is several points smaller than when Bush won New Mexico in '04 (the last time it went Republican).

New Mexico provides party registration information so it is easy to see how trends progress over decades.  Albuquerque did trend 2 points Democrat but voter rolls are shrinking.  Dona Ana almost always trends with El Paso (8.7 points more red) counties (1984 exception) but went more blue than last time.

Some GOP counties have obvious shaving going on, especially San Juan and Valencia.  There appears to be a coordinated harvesting/fraud effort going on, mostly in the north.

Areas adjacent to the Texas Panhandle went highly Republican.

We need forensic audits in at least 8 counties. I had New Mexico tilt to Biden. A Trump upset is possible.

Strategic audit targets: San Juan and Valencia counties.


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/12>


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/12>


{% include seth_worst_counties_for_state heading=true %}


## Otero County

{% include tg-embed url="https://t.me/RealSKeshel/2890" %}

{% include tg-embed url="https://t.me/RealSKeshel/3219" %}

{% include tg-embed url="https://t.me/RealSKeshel/3343" %}



## County-Level Breakdowns

We've analyzed some additional stats for counties in New Mexico, including Union, Catron, Quay and Sierra, which are being discussed in our Gitlab discussion area:

  [View the draft report](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/issues/19){:.button}

  
## Further Analysis on New Mexico

[This video](https://rumble.com/vlukw3-the-daily-302-show-2a-with-guest-captain-seth-keshel.html), featuring Seth Keshel, discusses New Mexico, starting at 25min mark.



{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
