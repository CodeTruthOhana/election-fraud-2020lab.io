---
title: Kansas Election Analysis by Seth Keshel
state: Kansas
abbr: ks
last_updated: 15 Feb 2022
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Kansas is quite a bit worse off than [Nebraska](../nebraska/), with most of the talk focusing on **Johnson County**, which voted Democrat for the first time since 1916! Trump won the county by 8,000 votes in 2016, despite losing 21,000 votes compared with Romney in the previous election, and if the registration totals by party are accurate, these indicated a big shift left for 2020.  Trump still gained 18k back in 2020, not quite to Romney's total (158k), but Biden's gain is ridiculous -- 54k (41%) improvement in one election cycle!  In 2008 Obama gained 29k votes, but that was with a drop in Republican votes.  This leads us to reason that Johnson has *at least* 20,000 more Biden votes than the trends suggest is reasonable.

In **Sedgwick (Wichita)**, Trump set a record Republican vote gain for one election, only to be outpaced by a massive record Democrat gain. In Sedgwick, we estimate 15,000 more votes than would be expected. This county is a prime target for a forensic audit.

If Biden has 48,000 more votes here than he should, Trump should have won with 58.2% to Biden's 38.0%, or 249k votes.

**Best GOP county audit targets:** Sedgwick, Leavenworth

**Need to audit:** Johnson, Shawnee


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/112>


{% include seth_worst_counties_for_state heading=true %}


## Johnson County

Johnson County appears to have approximately 20,000 excess Biden votes than would be expected. The county was flipped in 2020 for first time since 1916!

Seth's video below contains a detailed overview of the 16 year trendline and registration numbers.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vjigc4/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/RealSKeshel/1235), Sep 6, 2021

The following heat map shows which precincts within Johnson County diverged the most from expected trends and should be investigated:

{% include tg-embed url="https://t.me/RealSKeshel/3393" %}


## Sedgwick County

Estimated 15,000 excess Biden votes.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vjjg4q/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/RealSKeshel/1239), Sep 6, 2021


## Further Analysis on Kansas

[This video](https://rumble.com/vlukw3-the-daily-302-show-2a-with-guest-captain-seth-keshel.html), featuring Seth Keshel, dives into some of the Kansas counties, starting at 15min 50sec mark.


{% include seth_reports_methodology %}

{% include raw_data %}


## Other News

* Affidavits are being collected in Kansas. They’re also hoping to check their machines for cell-tower modems. Anyone with an outstanding Kansas affidavit should sign and return it, see [The Jim Price Show](https://www.thejimpriceshow.com/).

* Steve Watkins, a former Republican congressman from Kansas, listed a postal box at a UPS store as his residence on a state voter registration form while living temporarily at his parents’ home during a 2019 municipal election. Watkins was charged with three felonies - voting without being qualified, knowingly voting with more than one advance ballot, and interfering with the investigation intending to obstruct. He entered into a diversion agreement where his prosecution will be deferred for six months. If he complies with the terms of the agreement and pays a $250 fee, the charges will be dropped. <small>([Source](https://www.heritage.org/voterfraud/10047))</small>


{% include canvass/get-involved %}

{% include state_telegram_channels %}
