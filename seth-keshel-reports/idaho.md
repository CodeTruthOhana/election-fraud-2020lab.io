---
title: Idaho Election Analysis by Seth Keshel
state: Idaho
abbr: id
last_updated: 8 Feb 2022
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

{% include seth_state_details.html %}


Idaho taught me a lot.  As I've said, it is almost clean as a whistle.  More on that shortly.

Idaho trended exactly as the registration by party data said it would, and this taught me a lot about [Utah](../utah/), [Arizona](../arizona/), and [Montana](../montana/).  More later.

Ada County, we have a record high Biden vote gain of 45k, when the previous record was 24k for Obama 08.  If we assume a fair 2016 amount for Democrats in Ada would be about 90k, then this is less ominous.  Trump got most 2016 sit-outs back; Republicans outregistered Democrats in Ada 37k-26k (1.6 to 1 ratio) in last four year… but the % trended slightly Democrat (1.6%).

Bottom Line: I feel Biden is about 15-20k heavy in Ada County.

Yellow | Suspect/Likely Fraud | (1 county)
Green | Clean | (43 counties)

This, so far, is my best example of PROPER TRENDING in the 2020 election.  Recommend audit for Ada County.  If Biden is 15k heavy, margin would be 64.9-31.9% (33.0%).


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/29>

{% include seth_worst_counties_for_state heading=true %}


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/29>


## Ada County

Seth's post below links to a detailed heat map of the county and lists the top-10 worst precincts:

{% include tg-embed url="https://t.me/RealSKeshel/2956" %}



{% assign events = site.data.events | where: 'state', page.abbr %}
{% include events %}


{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
