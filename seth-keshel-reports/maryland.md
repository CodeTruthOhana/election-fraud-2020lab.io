---
title: Maryland Election Analysis by Seth Keshel
state: Maryland
abbr: md
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

For a state overwhelmingly favoring Biden, there is a startling amount of excess Dem votes, including in overwhelmingly Republican territory.  I have met with MD integrity group previously and the numbers in Carroll and Harford are eye-popping.  Frederick went from 3 point Trump win to 10 point loss, with a Trump gain in votes.  Anne Arundel is +44k over 2016 for Biden at a massive record, with a modest Trump gain.

Statewide, Obama (x2) and Clinton were all in 1.6mm range.  Biden is up ~300k, with little explanation and solid GOP trends away from the massive Dem strongholds that crush the GOP in the south.  Estimate 128k heavy for Biden, at lenient estimate.

Baltimore City trended clean.  Doesn’t mean no fraud, just a clean trend.

Best Trump county audit targets – Harford (12k), Carroll (8k).

If Biden is 128k heavy, margin would be 63.8% to 33.6%, or 881k votes.

Seth also discusses the Maryland findings on [the Steel Truth podcast, July 27, 2021](https://www.steeltruth.com/?wix-vod-video-id=e721581c8379420292a227bd727fb3cb&wix-vod-comp-id=comp-kl5lh1zq).


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/120>

----

{% include seth_worst_counties_for_state heading=true %}

{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Maryland as the state with the 5th highest number of unexpected Biden votes

{% include canvass/get-involved %}

{% include state_telegram_channels %}
