---
title: Mississippi Election Analysis by Seth Keshel
state: Mississippi
abbr: ms
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhl57m/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Mississippi is one of the cleaner states overall, coming in only about 15k heavy on Biden, without the benefit of party registration to zero in on the numbers.  He’s up 11.1% from 2016, in line with states like [Iowa](../iowa/), [Idaho](../idaho/), [Arkansas](../arkansas/), that came in in line with party/registration trends.

Hinds County trends clean.  Other counties growing in population should see some growth, but none high enough to create serious demand for an audit.  What I’ve found here are more small counties with stagnant/declining population that have small gains that don’t make much sense – maybe meaning the voter rolls need to be cleaned up.

DeSoto County would probably be worth a look.

Biden appears to be 15k heavy statewide.


{:.caption}
Source: <https://t.me/RealSKeshel/711>

----

{% include seth_worst_counties_for_state heading=true %}


## Further Updates

{% include tg-embed url="https://t.me/RealSKeshel/7226" %}


{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
