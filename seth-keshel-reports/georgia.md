---
title: Georgia Election Analysis by Seth Keshel
state: Georgia
abbr: ga
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}


<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhic0n/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}


{% include seth_state_details.html %}


Much like Arizona, Trump set a high-turnout era boom with 373k added votes, solidifying support with standard conservatives and winning more black men.

Democrats on the other hand, though vote gain is expected with population growth, had been stuck in same 104k vote loop (Obama down 70k in 2012, Clinton up 104k), only for Biden to gain a massive 597k in one election to win the state by an eyelash.  14,000 fraudulent votes kept David Perdue from winning on 11/3 and sent him to runoff.

Pattern is heavy cheating in metro ATL counties and shaving in northern GA exurban and rural counties that are massively pro-Trump.  I estimate that 311k of Biden's votes are beyond what would be rationally expected, comprised of Gwinnett & Fulton 35k, DeKalb 30k, Cobb 25k.

If I'm accurate on 311k excess votes, Trump's margin should have been roughly 52.6% to 46.1% (6.5%) with a margin of 299k votes.

**Best targets for audits (Republican-len):** Carroll, Cherokee, Columbia, Coweta, Forsyth, Houston, Lowndes

[![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-summary.jpg)](/images/seth-keshel/{{ page.abbr }}-summary.jpg)



{:.caption}
Source: <https://t.me/CaptainKMapsandStats/71>

This chart shows the bizarre differences in Democrat vote results. Why did these four counties in Georgia have 20-50% more votes in 2020 than in 2016, when other strongly Democrat counties ("strongholds") barely even reach a 7% increase? 

![Democrat vote gain in 2020 - Georgia vs other strongholds](/images/seth-keshel/ga-chart.jpg)

{:.caption}
Source: <https://t.me/RealSKeshel/766>


----

## Georgia Voter Roll Analysis

*By Seth Keshel, Aug 23, 2021. [PDF source](https://t.me/RealSKeshel/1026).*

This pertains to voter rolls in four Georgia counties, dated 4 February 2021.  Those counties are Forsyth, Cherokee, Dekalb, and Gwinnett, which hold an estimated 82,000 excess Biden votes, roughly 70,000 higher than Biden’s certified win total in Georgia. 
 
Total registered voters by county as of Feb 4, 2021: 

Forsyth | 173,189 
Cherokee | 201,572 
DeKalb | 579,760 
Gwinnett | 631,354 
 
Why are we looking at these counts? Here are the average number of voters entered on a random day: 

Forsyth | 15.9 | (10,882 dates recorded) 
Cherokee | 16.0 | (12,623 dates recorded) 
DeKalb | 39.1 | (14,814 dates recorded) 
Gwinnett | 41.2 | (15,319 dates recorded) 
 
Now why do we have such a tremendous flood of registered voters, far surpassing the average daily entries, right before elections?  See the chart below.  Of course we could expect voters to be added at a higher rate leading up to elections, but every single day in October at numbers blowing away the average?  Note that this is not just a 2020 thing – it appears to be something right before every election for decades. 

![](/images/seth-keshel/ga-voter-roll1.jpg)

In Cherokee County, where I’ve estimated Biden to be 12,000 votes too high, 16 registered voters is the normal per day in which voters are registered, going back many decades.  We can adjust for a higher number leading up to elections.   
 
They look like this:

![](/images/seth-keshel/ga-voter-roll2.jpg){:width="180"}

Notice that voters are being registered every day.  Then how, in the same month, do we also find entries like this?  Given that voters are being registered daily, there does not appear to be a reason for a backlog:

![](/images/seth-keshel/ga-voter-roll3.jpg){:width="230"}

#### Age of Voters

Most voters who are first time registrants, unless they’re moving in from somewhere else (which GA does have at a higher-than-normal number), are going to be younger, from 18-30, typically.   
 
Why then, do these counties have such high average ages in the dates leading up to an election?  Note that this graphic below isn’t even 2020-centric.  The question to me is, how long has this sort of registration type been going on?

![](/images/seth-keshel/ga-voter-roll4.jpg){:width="370"}
![](/images/seth-keshel/ga-voter-roll5.jpg){:width="370"}
![](/images/seth-keshel/ga-voter-roll6.jpg){:width="370"}
![](/images/seth-keshel/ga-voter-roll7.jpg){:width="370"}


#### Final Assessment

While not smoking-gun-proof of conspiracy, these numbers, showing such a distortion from not only the typical registration patterns, but the ones leading up to elections that are near-normal, may be indicative of methodology for registering “phantom” voters – as in, voters that are not real, but who are mailed absentee ballots. 
 
These numbers are either indicative of outright malfeasance, or they can be easily explained by county officials and the Georgia Secretary of State.  They are so out of line with ordinary registration trends that they must be explained.  If you are local, demand answers for these unbelievable distortions in registration data.


{% include seth_worst_counties_for_state heading=true %}


## Bibb County

{% include tg-embed url="https://t.me/RealSKeshel/4243" %}



## Chatham County

The following graphic shows a map of which precincts in the county had the most abnormal results in 2020:

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2324" data-width="100%"></script>


## Cherokee County

The following PDF shows a map of which precincts in the county had the most abnormal results in 2020:

{% include tg-embed url="https://t.me/RealSKeshel/3780" %}


## Columbia County

{% include tg-embed url="https://t.me/RealSKeshel/4309" %}


## Dekalb County

{% include tg-embed url="https://t.me/RealSKeshel/4406" %}


## Forsyth County

{% include tg-embed url="https://t.me/RealSKeshel/3507" %}


## Fulton County

{% include tg-embed url="https://t.me/RealSKeshel/4350" %}


## Gwinnett County

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2032" data-width="100%"></script>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2046" data-width="100%"></script>


{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}

{% include seth_reports_methodology %}

{% include raw_data %}


## Further Updates

Seth gave a presentation in Kennesaw, Georgia, on August 24, 2021. See [his post](https://t.me/RealSKeshel/985).

{% include tg-embed url="https://t.me/RealSKeshel/6791" %}

{% include tg-embed url="https://t.me/RealSKeshel/6823" %}



## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Georgia

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Georgia as the state with the 8th highest number of unexpected Biden votes

* [Atlanta, Georgia Election Fingerprints](https://wwrkds.net/wp2/atlanta-georgia-election-fingerprints/), by wwrkds

* See also the main [Georgia](/fraud-summary-by-state/georgia/) article for more

{% include canvass/get-involved %}

{% include state_telegram_channels %}
