---
title: Massachusetts Election Analysis by Seth Keshel
state: Massachusetts
abbr: ma
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}


Democrat nominees have been stuck in the 1.9 million range for three consecutive elections, including mega-popular Obama.  Since 2016, 11 of the 14 counties have trended Republican in registration, including Suffolk (Boston).  The state is 1.5% more Republican than in 2016, which would suggest a tighter election result, depending on the 2016 third party distribution.  No counties have been red here since 1988, although trends suggest that that likely would have changed in 2020.

Biden has *at least* 235,000 more votes than the trends would reasonably suggest. Here are the highlights:

County | Excess Votes
----|----
Middlesex | 50k
Worcester | 35k
Norfolk | 30k
Plymouth | 30k
Essex | 25k
Bristol | 25k

Suffolk isn’t far off trend.  Dukes and Nantucket are clean, which offers some clarity on the ground-game of the fraud, since they’re not on the mainland.

If Biden is 235k heavy, Biden's real margin would be 63.2% to 34.4%, or 980k votes.


{:.caption}
Source: <https://t.me/RealSKeshel/678>

----

{% include seth_worst_counties_for_state heading=true %}

## Caroline Colarusso's Investigations

{:.small style="font-style: italic"}
> Seth and team,
> 
> Allow me to introduce myself my name is Caroline Colarusso and I was a candidate for Congress in 2020 for the fifth district of Massachusetts. I ran against the Asst. Speaker of the house Katherine  Clark. 
> 
> I have been communicating the serious concerns we have uncovered for the 11/3 election in Massachusetts to major Boston media outlets & Sirius radio. (See links below) 
> 
> Through FIOA and public requests request and through communications from whistle blowers we have been told there are in excess of **500,000** ballot applications returned to sender sitting in the Archives Building in Boston. We have recently gained access to examine these ballot applications via a public records request.  A total of 3.6 million ballot applications were mailed to so-called voters in Massachusetts. **Between 12 and 20 percent were sent to bad addresses.**
>
> ![](/images/seth-keshel/ma-letter.jpg){:width="575" height="1280"}

{:.caption}
Source: [Telegram Post](https://t.me/RealSKeshel/1167), Sept 1, 2021. (Apologies for the low quality image.)

{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Massachusetts as the state with the 2nd highest number of unexpected Biden votes

{% include canvass/get-involved %}

{% include state_telegram_channels %}
