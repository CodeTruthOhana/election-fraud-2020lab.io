---
title: Virginia Election Analysis by Seth Keshel
state: Virginia
abbr: va
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table3.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-summary.jpg)

{% include seth_state_details.html %}


Estimating 217k excess votes on a lenient evaluation.

At face value, overwhelming NOVA support for Biden created an even bigger partisan gap, even as the south and southwest became stronger for Trump.  Most of the excess votes were piled up 1-2k at a time throughout the southern and eastern portions of Virginia, with about 73k between Prince William, Loudon, and Fairfax (lenient estimates).  The vote stoppage in Virginia creates suspicion as to whether the state was being prevented from flipping, or just given some buffer room.

If accurate, Trump’s margin of loss in Virginia would have been 5.6%, or 51.8% to 46.2%, a margin of 235k votes.  If vote flipping is occurring, then the state could have been down to the wire.

Best targets for audits: Culpeper, Chesterfield, Fauquier, Frederick, York, Spotsylvania, Stafford Co.


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/59>

----

{% include seth_worst_counties_for_state heading=true %}


{% assign events = site.data.events | where: 'state', page.abbr %}
{% include events %}


{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Virginia

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Virginia as the state with the 7th highest number of unexpected Biden votes

{% include canvass/get-involved %}

{% include state_telegram_channels %}
