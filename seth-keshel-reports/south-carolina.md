---
title: South Carolina Election Analysis by Seth Keshel
state: South Carolina
abbr: sc
last_updated: 23 Feb 2022
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}


![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

{% include seth_state_details.html %}

I knew there were some issues in South Carolina, but didn’t think they were so widespread.  

Even allowing population and vote expansion, Biden appears to have an unexpected excess of 119,000 votes, with only 22 of 46 counties trending "clean" (meaning they align with registration trends).  A massive coastal operation seems to have brought Biden a +236k gain over Clinton, blowing away Obama’s 2008 mark of 200k gained.  Trump was also up in the state 230k, and it appears he won it by a mile.

This state has lots of target-rich GOP counties to go after for audits.

**Worst counties:**

County | Excess/Suspicious Votes
-|-
Charleston | 12,000
Greenville | 12,000
Horry | 12,000
Spartanburg | 12,000
Berkeley | 8,000
York | 8,000
Richland | 8,000

If Biden has 119k excess votes as estimated, an accurate Trump margin is 17.3%, or 57.9% to 40.6%, and 412k votes.

**Best GOP county audit targets:** | Beaufort, Berkeley, Dorchester, Greenville, Horr, Lexington, Spartanburg, York
**Most important to audit:** | Charleston

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/103>


## Stats By County

The following table shows:

1. The number of excess Democrat votes, beyond what the trends suggest are reasonable
2. The population growth for the county over the past decade
3. The number of new registered voters in the county prior to the 2020 election

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/103>



{% include seth_worst_counties_for_state heading=true %}


## Beaufort County, South Carolina

Seth gave the following presentation at Audit The SC 2020 Vote Rally, Beaufort County, SC, on Monday, August 30, 2021. 49min runtime.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vj9zir/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

Following the release of canvassing results in Feb 2022, Seth also gave the following short summary which correlates his earlier findings with the canvassing results:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vrnjbd/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vu9pef-seth.html)


## Charleston County, South Carolina

Seth gave the following presentation in Mt Pleasant, South Carolina, Tuesday, August 31, 2021. 55min runtime.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vjg0g7/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vm26l7-seth-keshel-election-fraud-and-audit-committee-august-31-2021.html)


## Canvassing Results

On Feb 5, 2022, South Carolina Safe Elections Group released their election canvassing results. [Learn more](/fraud-summary-by-state/south-carolina/#grassroots-canvassing-results) about their findings.



## Precinct-Level Data

Seth has produced detailed precinct-level heat maps for several dozen counties in other states but has found it difficult to do so for North and South Carolina:

{% include tg-embed url="https://t.me/RealSKeshel/3607" %}



{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}

## See Also

**Lynz Piper-Loomis** is running for South Carolina's 1st Congressional District. She has pledged strong support to face election fraud. Find her at [LynzSC.com](https://www.lynzsc.com/) or on Telegram at [@LynzPiperLoomis](https://t.me/lynzpiperloomis).