---
title: Minnesota Election Analysis by Seth Keshel
state: Minnesota
abbr: mn
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vht0nv/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

{% include seth_state_details.html %}

Trump is up 161k votes from 2016 as the state nearly dropped into the GOP column, but Biden is up 349k after two consecutive elections of fewer votes, including Clinton down 178k from Obama’s re-election.  They will gain votes in 2020 thanks to third parties returning.

Fair share of green counties here, but there are a ton of rural, tiny counties with little play in numbers.  Hennepin is a cesspool, and Ramsey a smaller version.  Biden is 2x Obama’s high turnout era record gain, plus another 30k.  I’ve afforded him Obama ’08 gain plus 25%, same as Ramsey, almost certainly high.  

If Biden is 228k heavy, Trump would be within 5k votes, at 0.1% margin.  I suspect I’m way light on Hennepin, and believe this was a 1-3 point Trump state.  Ellison told you at 4 PM on 11/3 he didn’t have the votes.

**Best targets for audits:** Anoka (12k), Carver (8k), Scott (9k), Wright 10k

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/83>


{% include seth_worst_counties_for_state heading=true %}


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/83>



### Further Points

* Minnesota resembles [Wisconsin](../wisconsin/); [Pennsylvania](../pennsylvania/)/[Michigan](../michigan/) resemble each other. Third party vote very swingy coming back to two parties in Minnesota.

* I have Trump down to the wire here, with Biden well over what I would call 200k excess expected votes. 

* I have been generous to Biden, allowing 125% of Obama’s historic high turnout era (2004-present) gains in Hennepin and Ramsey.

* St. Louis County is way too high for Biden under recent trends, enough for local Dem mayors to endorse Trump in Iron Range. 

* AG Ellison tweeted late in afternoon of 11/3 they didn’t have the votes to win the state, just before they won the Obama-Romney race margin, after Trump spent big time there and campaigned?

* [Wisconsin](../wisconsin/) and Minnesota were 7-8% Obama states in 2012 and Minnesota 2.3% left of Wisconsin in 2016. I have Wisconsin to Trump at 3.7%. I’m sure I’m light in Twin Cities and would expect it to tilt to Trump up to as much as 2.5%. 

* Lewis won the Senate seat with numbers like this.

{:.caption}
Source: <https://t.me/RealSKeshel/512>


## Charting the Abnormal Trend

{% include tg-embed url="https://t.me/RealSKeshel/3646" %}

{% include tg-embed url="https://t.me/RealSKeshel/3647" %}

{% include tg-embed url="https://t.me/RealSKeshel/3648" %}



## Crow Wing County

The following video contains Seth's presentation in Brainerd, Crow Wing County, Dec 9, 2021. Full event runtime: 3hrs 42min.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vnvw5h/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vqi29n-searching-tor-truth.html)

[Download Seth's Presentation Slides](https://drive.google.com/file/d/10ssx01QPnyh6jCmOrpCOGfxwh_Hai-4d/view)

The following PDF shows a map of which precincts in the county had the most abnormal results in 2020:

{% include tg-embed url="https://t.me/RealSKeshel/3787" %}


## Ramsey County

{% include tg-embed url="https://t.me/RealSKeshel/6187" %}



## Wright County

Here is Seth's Presentation in Monticello, Wright County, Feb 22, 2022. Seth begins at 6min 15sec mark. Full runtime: 3hrs 5min.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vszmre/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vvlstm-seth-keshel-live-from-monticello-minnesota.html)


## Additional Reports and Updates

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/565"></script>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/859" data-width="100%"></script>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2451" data-width="100%"></script>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2468" data-width="100%"></script>

{% include tg-embed url="https://t.me/RealSKeshel/2694" %}

{% include tg-embed url="https://t.me/RealSKeshel/2704" %}

{% include tg-embed url="https://t.me/RealSKeshel/7663" %}

{% include tg-embed url="https://t.me/RealSKeshel/7664" %}



{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}

{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Minnesota

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Minnesota as the state with the 9th highest number of unexpected Biden votes

{% include canvass/get-involved %}

{% include state_telegram_channels %}
