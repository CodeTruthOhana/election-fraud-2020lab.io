---
title: Voting Machines Lacking EAC Accreditation
breadcrumb: "[Further Articles](#colophon) › "
meta_desc: Laws were broken when the test lab for Dominion, ES&S and ClearBallot election equipment approved the systems for use in 2020 despite not being accredited.
last_updated: 9 Dec 2021
---

In 2021 it was discovered that [Arizona](/fraud-summary-by-state/arizona/), [Texas](/fraud-summary-by-state/texas/#lack-of-certification), [Colorado](/fraud-summary-by-state/colorado/#use-of-non-certified-voting-machines) and numerous other states continued to certify electronic voting systems via Voting System Test Laboratory *Pro V&V* despite it having no active accreditation during 2020. During this time it certified Dominion, ES&S and ClearBallot voting and tabulation machines for use in the Nov 2020 election. However their accreditation to do so was marred by two significant issues:

1. The certificate of accreditation had expired 3 years earlier, in Feb 2017. It was not renewed until Feb 2021.

2. The original certificate of accreditation was signed by an acting EAC Director, not the EAC Chair, as designated by law, meaning their original certificate was likely to be legally invalid.

The US Election Assistance Commission (EAC) tried to cover for this mishap, calling it an "administrative error", blaming COVID-19 restrictions, and then stating that accreditation "cannot be revoked unless the EAC Commissioners vote to revoke the accreditation", when actually this contradicts state and federal laws which stipulate that accreditation expires after 2 years unless specifically renewed.

![Table: VSTL Labs Not Accredited for Elections](/images/vstl-table.jpg)

{:.small.muted}
Table sourced from the video below

<div class="info emoji">
🚨 This brings into question whether any election conducted using equipment certified by Pro V&V since Feb 2017 was actually legal. 

County clerks and Secretaries of State may have certified potentially-illegal elections.
</div>

## Explanatory Video

The following 37min video dives more deeply into the issue, with a focus on Arizona:

<iframe width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/WffdpkUcjWHZ/" allowfullscreen></iframe>

{:.small}
[Watch on Bitchute](https://www.bitchute.com/video/WffdpkUcjWHZ/)


## Further Issues

* The Maricopa, Arizona County Board of Supervisors demanded that any audit of their election be done by an EAC-accredited Voting System Test Laboratory (VSTL), either Pro V&V or SLI Compliance. Yet Pro V&V was not properly accredited prior to the decision to audit, either, but instead was rushed through in time for the audit. 

  The board and many mainstream media outlets heavily criticized Cyber Ninjas for being chosen to perform the Senate's forensic audit of Maricopa County, despite not being EAC-accredited.

  [Learn more about the Maricopa Election Audits](/fraud-summary-by-state/arizona/#maricopa-county-election-audits)

* Several reports [^1] [^2] mention that Jessica Bowers, a former Dominion employee of 10 years, now works for the EAC as their Acting CIO/CISO, raising ethical and conflict-of-interest concerns.


## Further Resources

Some additional articles and videos covering this issue:

**EAC** | [VSTL Certificates & Accreditation](https://www.eac.gov/sites/default/files/voting_system_test_lab/files/VSTL%20Certificates%20and%20Accreditation.pdf)
| [Jerome Lovato Memo: Pro V&V EAC VSTL Accreditation](https://www.eac.gov/sites/default/files/voting_system_test_lab/files/Pro_VandV_Accreditation_Renewal_delay_memo012721.pdf), Jan 27, 2021
**Arizona** | [Long Form History of Election Problems: Accreditation](https://www.bitchute.com/video/6NyU95WdUaOQ/) (video)
 | [2018-2019 Election Equipment Not Certified](https://www.bitchute.com/video/1jHf94p7mob0/) (video)
 | [2020 Election Equipment Not Certified](https://www.bitchute.com/video/Jb7qnKdORVDB/) (video)
 | [WE CAUGHT THEM: Arizona’s Maricopa County Board of Supervisors Lied – EAC Updated Website after Gateway Pundit Report Discovered Their Auditors Were Uncertified](https://www.thegatewaypundit.com/2021/01/caught-arizonas-maricopa-county-board-supervisors-lied-eac-updated-website-gateway-pundit-report-discovered-auditors-uncertified/), Jan 28, 2021
 | [BREAKING EXCLUSIVE: Cyber Ninjas Did Not Contaminate Voting Machines Used in 2020 Election in Arizona – Machines Weren’t Properly Certified In the First Place](https://www.thegatewaypundit.com/2021/07/breaking-exclusive-cyber-ninjas-not-contaminate-voting-machines-used-2020-election-arizona-machines-werent-properly-certified-first-place/), July 15, 2021
**Colorado** | [Use of Non-Certified Voting Machines](/fraud-summary-by-state/colorado/#use-of-non-certified-voting-machines)
| [Holly at Altitude: Only We Can Audit Ourselves](https://hollyataltitude.com/2021/06/22/only-we-can-audit-ourselves/), Jun 22, 2021
**Texas** | [Electronic Voting Machines: Lack of Certification](/fraud-summary-by-state/texas/#lack-of-certification)
 | [ES&S Voting Machine Certification Failures](https://rumble.com/vphbiz-es-and-s-certification-failures.html) (video)


### Footnotes & References

[^1]: TheMarketsWork: "[The Small World of Voting Machine Certification](https://themarketswork.com/2020/11/20/the-small-world-of-voting-machine-certification/)", November 20, 2020

[^2]: The Gateway Pundit: "[How Can the Government Agency Certifying Elections (the EAC) Maintain Its Independence When Its CIO Previously Worked for 10 Years for Dominion Voting Systems?](https://www.thegatewaypundit.com/2021/01/can-government-agency-certifying-elections-eac-maintain-independence-cio-previously-worked-10-years-dominion-voting-systems/), January 28, 2021