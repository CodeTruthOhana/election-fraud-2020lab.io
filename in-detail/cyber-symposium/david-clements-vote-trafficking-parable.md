---
title: David K. Clements – A Vote Trafficking Parable
meta_desc: A look at vote trafficking and voter fraud cartels in the United States.
last_updated: 16 Aug 2021
comment_intro: What did you find most significant about David's presentation? Let us know in the comments below.
---

*Presentation given at Mike Lindell's Cyber Symposium, August 12, 2021, in Sioux Falls, South Dakota.*

Are there similarities between drug trafficking cartels and the "vote trafficking" that appears to have went on in the 2020 election? In this presentation, David K. Clements points out several important parallels.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vigk6e/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

#### Key Points

* He walks us through a "parable" -- two parallel stories. In one he describes a mysterious crime that he prosecuted whereby a man was found murdered in a burnt-out vehicle, and they had to slowly piece together the evidence of what had happened. He draws lots of parallels to the 2020 election and what is slowly being uncovered.

* Both started with a mystery, a “curious” event, featured eyewitness accounts, and were finally clinched with live recordings that documented the crime.

* There was a strange stopping of votes around 10:30pm on election night. A lot of news reports showed that counting stopped, it was kind of weird. Somewhat of a mystery.

* Then video evidence emerged of the exact same stack of ballots being scanned multiple time. Sacred ballots!

* In his murder case, he uncovered the AZ Boys, a drug trafficking operation, run by crime families. These were a regional operation, but they answered to a larger cartel.

* In the election, it appears that we're uncovered a vote trafficking organization. There are various players and levels within this, some less culpable than others:
  - John Poulos
  - Eric Coomer
  - Adrian Fontez
  - Maricopa Board of Supervisors
  They are trafficking our votes. But the traffickers work for a cartel.

> “It’s no longer just about who gets to vote, and making it easier for eligible voters to vote. It’s about who gets to *count* the vote.”
>
> --- Joe Biden

* Vote trafficking organizations appear to be running in:
    - Maricopa County, [Arizona](/fraud-summary-by-state/arizona/)
    - Fulton County, [Georgia](/fraud-summary-by-state/georgia/)
    - Philadelphia, [Pennsylvania](/fraud-summary-by-state/pennsylvania/)
    - Wayne County, [Michigan](/fraud-summary-by-state/michigan/)
    - [Wisconsin](/fraud-summary-by-state/wisconsin/)
    
    But they answer to someone else: China.

* [Captain Keshel’s heat maps](/seth-keshel-reports/) help us narrow in on the key counties in the key states.

* Drug traffickers aren’t in the business of murder. But they will commit crimes to cover their tracks and keep the operation running. Vote trafficking is similar, where crimes follow behind: paper trails, money laundering, fraud. In some cases, even murder.

* In the murder case, Richard died because he was a liability. They couldn’t control him. He was an addict, and was weak. He became a liability, and so his “family” snuffed him out. Similarly, they could not control Donald Trump --- he was a liability for the vote trafficking organization, and a liability for the cartel.
He didn’t just upset the cartel here in the US, but worldwide. He looked the vote traffickers in the eye and knew exactly what they were.

* Sometimes a cartel will signal to those lower down what will happen. Similarly, Bernie Sanders told us *exactly* what would happen, down to the exact states. They knew what they were going to do. Biden also presented lies and propaganda, telling us what they were going to do. But we were asleep. Have we woken up yet?

* Someone has to start sifting through the evidence, through the muck and the mire.
This is difficult because we are inundated with propaganda, through our screens, and we have been "fried".

* But God likes to expose the wicked and make fools of them, and it’s going to happen.

* So we have John Poulos, CEO of Dominion, a new kind of "hit man".

* John Halderman was quoted saying “You hear that voting machines are not connected to the internet. Unfortunately it’s not actually true.” The Antrim County Forensics report also confirms this. Many new voting machines come with 4G network modems, in order to upload results faster. Dominion have been shown to advertise internet connectivity, yet John Poulos says “there is no internet connectivity at all”.

> There is no internet connectivity at all.”
>
> --- John Poulos, Dominion Voting Systems CEO

* Poulos *purgery*, time and time again.

> Don’t worry about the election, Trump’s not gonna win. I made f**king sure of that!”
>
> --- Eric Coomer, Dominion Voting Systems Vice President

* Eric Coomer holds the patent for the feature known as adjudication. One of the tools in their toolchest.

* These are the men that pull the trigger (commit the murder).

* In the murder case, a key relative gave a confession about the crime. In cases like this you have to make an assessment about whether you think they're credible. In the Dominion vote trafficking organization, there was Melissa Carone, a contractor for Dominion. She says she saw that "They were rescanning the same ballots, recounting them 9-10 times." How many ballots? “At least 30,000.”

* To back up the case we have some beloved math nerds:

    * [Seth Keshel’s trend analysis](/seth-keshel-reports/)
    * Dr. Frank’s algorithms
    * [Draza Smith’s cruise control theory](https://rumble.com/vkgtqh-draza-smith-election-fraud-on-cruise-control.html)
    * [Edward Solomon's Geometric Proof](https://rumble.com/vdnfcf-edward-solomon-geometric-proof-for-georgia.html)

    ...we know that graph with the spike is bull!

* So how do they attempt to get away with it? *The cleanup.* 

    - The murderers cleaned the garage -- they wanted to make sure there was no evidence. Then placed the house for lease. 
    - The US election cartel asserted that it was “clean”. CISA Chief Christopher Krebs asserted it "was the most secure election in history".
    - They had to do other things to clear up. In Maricopa County, Arizona, there were shredded ballots found in a dumpster.
    - Mr Hickman, on Maricopa Board of Supervisors, had his house completely burnt down. Perhaps some kind of cover up was going on?

* So how do we catch the traffickers?

    * In the murder case they reconstructed the cranium and found heavy metals, a bullet. There's traces of evidence that we can find using forensic tools. They found cleaned up blood in the garage.
    * For the elections, Jovan Pulitzer is working on forensic analysis, looking at ballot paper under the microscope. His special tools reveal much.
    * We also examine the computer hardware: we can uncovering how the devices were talking to each other.

* When the evidence is growing, the criminals react with *desperation*
    * This symposium facility has been hacked. There are also Antifa members here. 
    * In the murder scene, the murderer kept hours of recordings on his phone, because he knew he was the last link, and if they killed him, they could absolve themselves of the murder, and he was paranoid.
    * We learned something from the brave election clerk from Mesa County, Colorado, Tina Peters. It wasn’t just *her* house that they raided. This is *our* house because it houses *our* vote.
    * We heard from the cyber team going through the forensic images, from Dominion.
    * We have evidence that’s very compelling.
    * The Colorado Secretary of State gave a press conference today, and she was shaking.

* Inside the Dominion voting machine forensic images, there were references to email servers in Senegal, in Africa. China throws money at African countries (like Senegal) and they setup their technical infrastructure there.

* We're now at a crossroads:

    * The drug traffickers were going after the law enforcement officials. They were both trying to catch each other first.
    * That’s also happening at the Symposium facility.
    * A message to the criminals: “It’s not too late to get some lenience, because we’re going to get you one way or the other.”
    * Some County Clerks don’t even know they’ve been groomed to carry out the crime of vote trafficking.
    * To those out there that are complicit: "We haven’t won yet, but we will."
    * We need you, the people, to be hardened prosecutors that won’t take “no” for an answer. Be dogged.

* Brian Kemp’s office people dying in mysterious explosions.

* David ran for senate some time ago, and in that race he was hacked. "I saw fraud, I was defamed, but they sued *me* for defamation, and tied up that case for years." They wanted to silence him.

* We need an attitude of perseverance.

* And we need to demand *full forensic audits*. *FULL FORENSIC AUDITS.* We need a  reconstruction of the murder of the vote. In all fifty states.

* Coward judges have used “legal standing” to fend off the cases. Legislatures said “the courts will decide”, and sent it away, and then later: “Oh now it’s too late.” Then we had the last chance on Jan 6.

* While some are more reluctant than others: God has laid your steps, and you are following him.

----

Many of David's video clips were sourced from Kanekoa the Great. See his [Rumble Channel](https://rumble.com/user/KanekoaTheGreat) or [Telegram Channel](https://t.me/KanekoaTheGreat).















