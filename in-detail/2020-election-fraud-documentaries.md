---
title: Documentaries Covering US 2020 Election Integrity & Fraud
last_updated: 22 Aug 2022
breadcrumb: "[Further Articles](#colophon) › "
comment_intro: Have a comment about one of the documentaries? Or have you discovered another one that is relevant? Share a comment below.
faq_title: What documentaries have covered the US 2020 election integrity issues?
comments_enabled: true
---

The following documentaries summarize the key claims and allegations of fraud during the election. The authors of this website have not independently verified all the claims and evidence presented, so continue to do your own research.

{% include toc %}


{% include 2000-mules-summary %}


## [S]election.Code

An August 2022 documentary covering the story of Mesa County, Colorado County Clerk Tina Peters and her discovery of anomalies within the voting machines under her purview, which led to persecution from state officials including Secretary of State Jena Griswold and an alarming raid on her house by the FBI.

Also features Colonel (Retired) Shawn Smith, data and security analyst Jeff O'Donnell, and grassroots activist Sherronna Bishop.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/v1e6etl/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

Runtime 1:01

[Learn more at SelectionCode.com](https://selectioncode.com){:.button}


## Rigged: The Zuckerberg Funded Plot to Defeat Donald Trump

*Rigged: The Zuckerberg Funded Plot to Defeat Donald Trump* investigates Facebook CEO Mark Zuckerberg’s role in the election. Rigged looks at the nearly $400 million Zuckerberg spent nationally in voter operation efforts, particularly in the battleground states of Wisconsin, Arizona, and Georgia. 42,000 votes in those three states are the reason Joe Biden is president.

Featuring exclusive interviews with President Donald J. Trump, Speaker Newt Gingrich, Senator Ted Cruz, Attorney General Jeff Landry, Congresswoman Claudia Tenney, Congressman Jody Hice, Kellyanne Conway, Ken Blackwell, Ken Cuccinelli, Cleta Mitchell, Michael Gableman, Janel Brandtjen and Scott Walter, Rigged follows the money and uncovers the startling facts behind Election 2020 in order to answer the question once and for all: *What happened?* And ensure it never happens again.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vxbo2x/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Alternate links here](https://rumble.com/search/video?q=Rigged%3A%20The%20Zuckerberg%20Funded%20Plot)

[Learn More at Citizens United Productions](https://citizensunitedmovies.com/pages/rigged){:.button}


## Kill Chain: The Cyber War on America's Elections

Released in March 2020, 8-9 months prior to the Nov 2020 election. From voter registration to counting ballots, data security expert Harri Hursti examines how hackers can influence and disrupt the U.S. election system. The film also features hackers at the conference DEF CON in their attempts to test the security of electronic voting machines.

The film also sheds some light on hacking attacks on the presidential election in 2016, with an exclusive on-camera interview with the hacker known as CyberZeist. CyberZeist penetrated the Alaska Division Of Elections' state vote tabulation computer system on 6th and 7th November, 2016, and on election day, 8th November, 2016. 

Here's the movie trailer:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/AwSVN_dgio8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Watch Full Film on HBO](https://www.hbo.com/video/documentaries/kill-chain/videos/kill-chain-the-cyber-war-on-americas-elections){:.button} (1hr 30min)


## Cyber Symposium Film

The following 23min film played a central role in Mike Lindell's Cyber Symposium which aimed to expose election fraud, held August 10-12, 2021.

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vid807/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>


## Epoch Times 2020 Election Investigative Documentary: Who’s Stealing America?

By the Epoch Times. 1hr 33mins. 

[![Epoch Times 2020 Election Investigative Documentary: Who’s Stealing America?](/images/epoch-times-doco.jpg)](https://www.ntd.com/2020-election-investigation-who-is-stealing-america_540191.html)

[Watch Online at NTD.com](https://www.ntd.com/2020-election-investigation-who-is-stealing-america_540191.html){:.button}

<!-- <div class="video_fit_container epoch_player paragraph-margin-bottom"><div class="player-container" id="player-container-60bb93fa-d621-4d44-b9d0-420b9621cb57" data-id="player-60bb93fa-d621-4d44-b9d0-420b9621cb57"></div></div><script src="//vs.youmaker.com/assets/player/60bb93fa-d621-4d44-b9d0-420b9621cb57?r=16x9&amp;s=854x480&amp;d=5613&cat=featured-programs/special-programs&logo=true&api=7&autostart=true&mute=false&url=https%3A%2F%2Fwww.ntd.com%2F2020-election-investigation-who-is-stealing-america_540191.html"></script> -->

<!-- <div class="responsive-iframe"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/DpxHur-tPRE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div> -->

The [YouTube version of the film](https://www.youtube-nocookie.com/embed/DpxHur-tPRE) has been censored.


## Absolute Proof: Exposing Election Fraud and the Theft of America

By Mike Lindell. 2hrs. Watch [here](https://www.bitchute.com/video/5u6UI1306nbv/), on [LindellTV.com](https://lindelltv.com/absolute-proof/) or via [Rumble.com](https://rumble.com/vdlebn-mike-lindell-absolute-proof-exposing-election-fraud-and-the-theft-of-americ.html).

<div class="responsive-iframe"><iframe src="https://www.bitchute.com/embed/5u6UI1306nbv" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen scrolling="auto"  frameborder="0"><small>BitChute embedding powered by <a href="https://embed.tube">embed.tube</a></small></iframe></div>


## Absolute Interference

The sequel to *Absolute Proof* with new evidence that foreign and domestic enemies used computers to have the 2020 election. By Mike Lindell, via [LindellTV.com](https://lindelltv.com/absolute-interference/).

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vd9a53/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

Mike Lindell also did follow-up interviews:

* [Scientific Proof](https://lindelltv.com/scientific-proof/) -- 1hr interview between Mike Lindell and Dr. Douglas G. Frank, PhD scientist.

* [Absolutely 9-0](https://lindelltv.com/mike-lindell-presents-absolutely-90/) -- 26min interview with an anonymous computer security expert.


## Deep Rig

The Deep Rig is a documentary that covers much of the story from Patrick Byrne's eye-opening book "[The Deep Rig](https://www.amazon.com/Deep-Rig-Election-Fraud-Donald-ebook/dp/B08X1Z9FHP)", including interviews from Lt. General Mike Flynn, Patrick Byrne, Bobby Piton, Jovan Pulitzer, cyber experts, and others. Released June 26, 2021.

Below is the trailer which was censored from YouTube:

<div class="responsive-iframe"><iframe src="https://www.bitchute.com/embed/kna89TT8VCzQ" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen scrolling="auto"  frameborder="0"><small>BitChute embedding powered by <a href="https://embed.tube">embed.tube</a></small></iframe></div>

<!-- <div class="responsive-iframe"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/fBZCsBuLPDU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div> -->

[Watch online at TheDeepRig.movie](https://thedeeprig.movie/){:.button} (free registration required)


## Is Voter Fraud Real?

By PragerU. A short 5min explanation of weak points in the American election system that are easily exploited.

This video was posted in February 2020, 9 months prior to the November 2020 election.

<div class="responsive-iframe"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/RkLuXvIxFew" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>


## The Plot to Steal America

By Man in America. 18mins.

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/v9gws7/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

Visit his [Rumble channel](https://rumble.com/c/ManInAmerica) for many more follow-up videos.


## The Plot Against The President

While not specifically about the 2020 election, "*The Plot Against The President*" is a documentary released mid-2020 (prior to the election) exploring the covert operation to undermine Donald Trump's presidency from several different angles, beginning with the ousting of General Michael Flynn, one of the few who could have stood up to the corruption. Here's the trailer:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/mqTu_Btkr08" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Watch Film Online](https://ugetube.com/watch/the-plot-against-the-president_rcEqhbW317DAYjO.html){:.button} or  [visit their website](https://patpmovie.com/)


## Reawakening Docuseries

The *Reawakening* series of documentaries is due to be released on Nov 15, 2021.

<iframe title="vimeo-player" src="https://player.vimeo.com/video/576523855?h=7b6b03c609" width="640" height="360" frameborder="0" allowfullscreen></iframe>

Learn more at [ReawakeningSeries.com](https://reawakeningseries.com/).

### Footnotes & References

