---
title: Maricopa County Forensic Audit Results
parent_page: /in-detail/maricopa-arizona-forensic-audit-report/
meta_desc: Summary of the Maricopa County Forensic Audit Results.
last_updated: 25 Oct 2021
---

{% include maricopa_results %}

### Footnotes & References
