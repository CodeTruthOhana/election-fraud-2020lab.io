---
title: US Capitol Protest Irregularities, Jan 2021
breadcrumb: "[Further Articles](#colophon) › "
last_updated: 24 Sep 2021
faq_title: "Were there also irregularities during the January 6 protests at the US Capitol?"
comments_enabled: true
---

On Wednesday, January 6, 2021, the US Congress undertook the process of officially accepting the election results. With many still disputing the results as possibly fraudulent, protesters clashed with police outside the building, and eventually broke through windows and doors and entered the building, causing damage, looting, and disruption of Congress proceedings. Several people also tragically lost their lives (although, as you'll see below, none were a direct result of the protesters).

The mass-media focused on the smaller pockets of violence that occurred amongst the thousands gathered --- which should be denounced and prosecuted, fairly --- but they largely ignored the other reports of police *initiating* physical violence, or in other cases strangely offering no resistance to crowds entering the property, even what appears to be *welcoming* of them in.

Democrat lawmakers and the mass-media were quick to blame Donald Trump for inciting the violence amongst his followers, despite the fact that Trump had offered National Guard assistance in the days prior, and that protesters breached the Capitol around 12:53pm--1:03pm while Trump was still delivering his rally speech 1.6 miles away. [^10] Later that day, Trump denounced the violence and rioting and called for law and order. [^4] 

In this article we'll focus on those aspects that were largely overlooked and not reported to the public.

{% include toc %}

## Irregularities

Several news outlets released reports of odd and potentially suspicious activity that deserves further investigation:

* A French news reporter uncovered how several members of the Capitol Police were sent home at 10am, prior to the riot, with little explanation. They were not called back in when rioting began. [^1]

* The Department of Defense (including the National Guard), and Federal Bureau of Investigation (FBI) offered support to the Capitol in the days preceding the event, to assist with maintaining law and order. It's alleged that in all cases, this assistance was refused. [^9]

* [A video](https://rumble.com/vcjbrb-police-directing-trump-protesters-into-capitol.html) released on social media shows a member of the Capitol Police waving protesters into the grounds, offering them no resistance. [^5] [^23] 

    <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/v9x5kn/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

    While some protesters entered the building via force, including breaking windows [^27], other videos ([here](https://rumble.com/vcjxnp-shocking-new-video-capitol-police-appear-to-just-let-protesters-into-buildi.html) and [here](https://rumble.com/vhw8xr-insurrection-debunked-new-video-inside-capitol-shows-peaceful-protest.html)) show more evidence of a surprising lack of police resistance, where they even opening barricades and external doors to the building to let people through. [^25] [^24] [^26] [^3] [^6]

    <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/v9xrh1/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>
    
    <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/veravd/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

* Arguments filed in court that officers provoked non-violent protesters into greater aggression by pushing, punching, tear-gassing and pepper spraying them while *outside* the Capitol grounds (not trespassing), and "doing nothing wrong". [^21]

* [A video](https://rumble.com/embed/vhykq4/?pub=m4ux1) (at 1:30 mark) showing a police officer repeatedly punching a restrained protester. Is this normal procedure for restraining an offender?

* Videos ([here](https://amgreatness.com/2021/05/27/new-video-depicts-capitol-police-using-stun-grenades-on-crowd/), [here](https://rumble.com/vhup1r-capitol-police-fired-exploding-flash-grenade-into-crowd-on-jan.-6-of-men-wo.html), [here](https://rumble.com/vkkqve-new-video-reveals-jan.-6-protesters-getting-unruly-only-after-police-fired-.html) and [here](https://www.youtube.com/watch?v=MrW1bD9laoU)) showing flashbangs (stun grenades) thrown -- allegedly by Capitol Police -- and exploding into a crowd of rowdy but otherwise non-violent protesters outside the building [^22] [^28]

* Photographers were seen at the front of the charge into the Capitol building. [^7] Who were they and why did they also breach the building?

* [Video](https://rumble.com/vhw8xr-insurrection-debunked-new-video-inside-capitol-shows-peaceful-protest.html) of non-violent protesters milling around the inside halls of the Capitol, taking photos, singing the national anthem, and chanting, with security officers passively lingering nearby, doing little to control the crowd roaming throughout building [^24]

* A series of buses were seen arriving to the Capitol under State Trooper escort, after Trump's rally had already begun. Protesters disembarked and  walked directly onto the Capitol grounds. [^8] Why were these protesters being given special escort to the Capitol?

* Fox News commentator Tucker Carlson discussed findings that FBI agents were amongst the protesters, and there may be evidence to suggest that they may have provoked or instigated aspects of the rioting. [^13] He also raised questions around why some protesters were prosecuted heavily, while others with left-wing leanings were released without charge.

* Witnesses report seeing and overhearing anti-Trump sentiments from a group dressed in Republican "MAGA" attire, indicating that some protesters may have been dressed deceptively

Journalist Julie Kelly has rightfully posed [20 questions for House Speaker Nancy Pelosi about Jan 6](https://amgreatness.com/2021/07/05/20-questions-for-nancy-pelosi-about-january-6/), which as yet, the public are still waiting answers to.

<div class="info" markdown="1">

### Detailed List
 
A detailed database of over 50 irregularities surrounding the Capitol Riots based on court-admissible evidence has been collated and made available at:  
[HereIsTheEvidence.com](https://hereistheevidence.com/capitol-protest-1-6-21/)

</div>

## Kash Patel on FBI Failings

Kash Patel, Chief of Staff of The Department of Defense on Jan 6, raises numerous concerns about the mistakes (intentional or otherwise) and severe lack of security implemented around the Capitol on the day. He raises concerns about:

* Why the cabinet secretaries under President Trump were not briefed on the potential for a security incident

* Why FBI Directory Christopher Wray was noticeably absent from phone calls discussing Capitol security in the days before Jan 6, between President Trump, then-Chief-of-Staff Mark Meadows, Attorney General William Barr, and the Department of Homeland Security, despite Wray's participation being requested [^15]

* Why the FBI did not put a thousand uniformed agents around the Capitol building, despite knowing in advance what might occur

* Why did they not raise a fence around the grounds to protect the building

* Why an entire side of the Capitol (possibly the south side) was "totally unmanned, no police officers whatsoever"

* Why Mayor Bowser of Washington D.C. turned down the Department of Defense's offer of 10,000--20,000 National Guardsmen and Women to protect the Capitol. (It appears to have ignored due to "political reasons".)

The Capitol breach could have been easily prevented, and Kash believes it was a failure of law enforcement, particularly the FBI, who failed to act on intelligence.

As of September 2021, we are not aware of any thorough probe into the malfeasance by the FBI.

See an 11-minute portion of his interview, below:

<div class="responsive-iframe"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/9eiMMb2YYxk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

Or [watch the full 27 min interview on EpochTV](https://www.theepochtimes.com/kashs-corner-the-deadly-consequences-of-defunding-the-police-and-no-cash-bail_3885058.html){:target="_blank"}.


## Department of Defense Timeline of Jan 6

The Department of Defense has a 2.5-page [timeline of events published on their website](https://media.defense.gov/2021/Jan/11/2002563151/-1/-1/0/PLANNING-AND-EXECUTION-TIMELINE-FOR-THE-NATIONAL-GUARDS-INVOLVEMENT-IN-THE-JANUARY-6-2021-VIOLENT-ATTACK-AT-THE-US-CAPITOL.PDF), however this appears to be a much shorter summary of the 10-page version shown by the Epoch Times and available via Scribd, below.

<iframe class="scribd_iframe_embed" title="Capitol Riots DoD timeline" src="https://www.scribd.com/embeds/503025581/content?start_page=1&view_mode=scroll&access_key=key-DBMmq0c1OoXJNitbDWD6" tabindex="0" data-auto-height="true" data-aspect-ratio="0.7727272727272727" scrolling="no" width="100%" height="600" frameborder="0"></iframe>

<p  style="   font-family: Helvetica,Arial,Sans-serif;   font-style: normal;   font-variant: normal;   font-weight: normal;   font-size: 14px;   line-height: normal;   font-size-adjust: none;   font-stretch: normal;   -x-system-font: none;   display: block;"   ><a title="View Capitol Riots DoD timeline on Scribd" href="https://www.scribd.com/document/503025581/Capitol-Riots-DoD-timeline#from_embed"  style="text-decoration: underline;">Capitol Riots DoD timeline</a> by <a title="View Herridge's profile on Scribd" href="https://www.scribd.com/user/488930117/Herridge#from_embed"  style="text-decoration: underline;">Herridge</a></p>

It's not yet clear why the DoD no longer publishes the 10-page version.


## Misleading Media Narratives Decrying "Murder"

News channels CNN and MSNBC have repeatedly spread the narrative that five people were killed as a result of the Jan 6 protests --- sometimes even labelling it as "murder". [^11] However this is not backed up by the Washington D.C. Medical Examiner's office which has revealed the cause of deaths for the five people who died surrounding the incident. Note that *none* of the deaths were caused by the protesters:

<table class="bordered">
    <tr>
        <th>Deceased Person</th>
        <th>Official Cause of Death</th>
    </tr>
    <tr>
        <td>Ashli Babbit<br><small>(age 35)</small></td>
        <td>Homicide, by unnamed member of the Capitol Police</td>
    </tr>
    <tr>
        <td>Kevin Greeson<br><small>(age 55)</small></td>
        <td>Natural causes -- heart attack<br>(not hastened by injury)</td>
    </tr>
    <tr>
        <td>Benjamin Phillips<br><small>(age 50)</small></td>
        <td>Natural causes -- heart attack<br>(not hastened by injury)</td>
    </tr>
    <tr>
        <td>Roseanne Boyland<br><small>(age 34)</small></td>
        <td>Drug overdose</td>
    </tr>
    <tr>
        <td>Brian Sicknick<br>Capitol Police Officer</td>
        <td>Natural causes -- stroke<br>(not hastened by injury)</td>
    </tr>
</table>

<small>[Source](https://amgreatness.com/2021/04/07/d-c-medical-coroner-rules-ashli-babbitts-death-a-homicide/)</small>

Many politicians and top news organizations are overdue for issuing retractions, but it remains to be seen if many will come.

Julie Kelly aptly summarizes the continuing exposure of false media narratives:

> Much of what the public has been told to believe about January 6 slowly is being exposed as a series of falsehoods. It wasn’t an armed insurrection; five people did not die as a result of the protest; Brian Sicknick was not killed by Trump supporters; and it was not even close to being the “worst attack on our democracy since the Civil War,” as Joe Biden insists. The protest was not orchestrated or executed by white supremacists or “domestic violent extremists,” as the director of national intelligence warns.
> 
> "Now another aspect of the official narrative has crumbled. The question remains: If January 6 was as bad as they say, why do they have to keep lying about what happened?"
> 
> <small>--- Julie Kelly, Investigative Journalist & Author [^12]</small>


## Other Investigation Impediments

Congressman Byron Donalds (R-Fl), member of the Committee on Oversight and Reform has tried to get the head of Capitol Police to attend the committee and share about what information they had prior to January 6, but he states that Speaker Nancy Pelosi won't let them come because "they're under her".

The Committee has had two hearings that the Democrats organized. In both cases the Capitol Police were not present to explain their actions (or lack of action) despite being provided intelligence from the FBI.

He also states that Nancy Pelosi has not been forthcoming about what she knew prior to Jan 6. During Trump's second impeachment over the riots, Republicans called Nancy Pelosi to testify as a witness, but an hour later, the Democrats decided they would no longer permit *any* witnesses.

See [Byron Donalds interview on Flashpoint](https://rumble.com/vk7ds7-flashpoint-friends-dont-let-friends-watch-mainstream-media-july-22-2021-.html), starting at 21:00, episode from July 22, 2021.


## Appalling Mistreatment in Prison

American Greatness has published several reports on the condition of detainees prior to their trial. *[Shawshank for January 6 Detainees](https://amgreatness.com/2021/05/17/shawshank-for-january-6-detainees/){:target="_blank"}* tells of:

* 100 days in solitary confinement
* Abuse by prison guards
* Denial of access to family members and defense attorneys
* Experiences of being tortured “mentally, physically, socially, emotionally, legally, and spiritually.” 
* Religious services not allowed
* Lack of access to exercise 
* Access to personal hygiene such as showers is nearly non-existent
* Being placed in an empty cell for at least 14 hours as punishment for advising other detainees against accepting plea deals. There's “no water, nothing in my cell, no chair to sit, no blanket, no Bible, no toothbrush, no toilet paper ... and no human contact.” Jacob said the "water" is brown and comes out “in chunks.” [^17]


*[Letters from a D.C. Jail](https://amgreatness.com/2021/06/10/letters-from-a-d-c-jail/){:target="_blank"}* outlines more stories of arrested protesters' poor and inhumane treatment:

* Placed in solitary confinement 22-23 hours a day before being given a trial
* Threatened with physical beatings for singing 'God Bless America'

Many of these imprisoned protesters are charged with damaging property or minor misdemeanors, but no serious violent crime. They are also being denied access to government video footage from the Capitol to enable their defence (except for very restricted access to approved clips). [^17]

> Prison guards “instill fear” in the detainees. Ryan Samsel, behind bars since January, allegedly was beaten by prison guards who handcuffed him with zip ties. “He has definitely suffered serious injuries, including a shattered orbital floor [the base of the eye socket], a broken orbital bone, his jaw was broken, his nose was broken,” his lawyer said in an interview last month. He is currently unable to see out of his right eye and may permanently lose his vision.
> 
> <small>-- American Greatness: "[Shawshank for January 6 Detainees](https://amgreatness.com/2021/05/17/shawshank-for-january-6-detainees/){:target="_blank"}", also reported in [The Washington Post](https://www.washingtonpost.com/local/public-safety/capitol-rioter-alleges-beating-jail-guards/2021/04/06/310cb700-9718-11eb-a6d0-13d207aadb78_story.html)"</small>

[An Epoch Times article](https://www.theepochtimes.com/over-535-charged-6-months-after-jan-6-capitol-breach-doj_3890372.html){:target="_blank"} quotes lawyer Steven Metcalf: “I’m being told the water is black—he has to filter the water through a sock in order to even drink water. In addition to only going out one hour a day, [on] the weekend he doesn’t get out at all, and he’s not able to use a shower, get a shave for days on end.”

The Epoch Times also released [a related video interview](https://www.theepochtimes.com/jan-6-detainees-confined-23-hrs-day-risking-all-for-american-dream_3885912.html){:target="_blank"} with lawyers and family members of detained prisoners, kept in "the hole" (solitary confinement) prior to trial. [^16] They were not permitted a private meeting with lawyers, nor even a haircut since being detained 6 months ago. Apparently some have been offered a plea deal which they felt would have given up many of their rights as a US citizen. Some with serious medical conditions including cancer, eye damage, and limb damage are not permitted access to medical treatment. [^14] [^19] [^20] Their lawyers are calling it "blatantly unconstitutional". [^14]

[Another Epoch Times video](https://www.theepochtimes.com/kashs-corner-jan-6-detainees-facing-unfair-treatment-in-detention-including-solitary-confinement_3895163.html) interviewed Kash Patel, Chief of Staff of The Department of Defense, himself having argued over 1,000 cases in court, and he describes it as highly irregular and almost certainly politically motivated.

In a further twist, on September 1, 2021, news reports emerged that John Pierce, the attorney defending 17 clients charged with storming the US Capitol on Jan 6, had gone missing. [^29] He was later reported to have been in hospital on a ventilator with COVID symptoms, but we have not yet seen an official statement.


### Supporting Efforts

A detailed list of 540 people arrested and charged in relation to the January 6 riots is available in [this Google Spreadsheet](https://docs.google.com/spreadsheets/d/1twbUfHHQTfuRIqnGcu39yOsAo3SFCQnZ8Q6SwGd0-e0/edit#gid=82340023).

[The Patriot Freedom Project](https://www.patriotfreedomproject.com/) supports those who have been politically persecuted since Jan 6, with links to many stories and the latest news. Prayer supporters are connecting via [The Prisoner's Record](https://t.me/ThePrisonersRecord/104).

Attorney Sidney Powell and Defending The Republic have [offered support](https://defendingtherepublic.org/a-personal-message-from-sidney/) to detainees seeking a fair and just trial, as well as their legal teams. Similar support is available from [NCLU](https://www.nclu.com/).

Protests against the unconstitutional indefinite detention are being coordinated by [Citizens Against Political Persecution](https://citizensapp.us/).



### Footnotes & References

[^1]: <span class="pill">Censored</span> Victory Update: What Happened in D.C. with Sarah Palin, <https://youtu.be/JK6Ht5BFp6I?t=180>. Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^2]: <span class="pill">Censored</span> Victory Update: What Happened in D.C. with Sarah Palin, <https://youtu.be/JK6Ht5BFp6I?t=310>. Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^3]: <span class="pill">Censored</span> Victory Update: What Happened in D.C. with Sarah Palin, <https://youtu.be/JK6Ht5BFp6I?t=130>. Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^4]: <span class="pill">Censored</span> FlashPoint, <https://youtu.be/u7evOBf_E80?t=15>. Unfortunately, since Flashpoint have been censored by YouTube, the video appears to no longer be accessible via this link.

[^5]: <span class="pill">Censored</span> FlashPoint, <https://youtu.be/u7evOBf_E80?t=255>. Unfortunately, since Flashpoint have been censored by YouTube, the video appears to no longer be accessible via this link.

[^6]: <span class="pill">Censored</span> FlashPoint, <https://youtu.be/u7evOBf_E80?t=618>. Unfortunately, since Flashpoint have been censored by YouTube, the video appears to no longer be accessible via this link.

[^7]: <span class="pill">Censored</span> FlashPoint, <https://youtu.be/u7evOBf_E80?t=671>. Unfortunately, since Flashpoint have been censored by YouTube, the video appears to no longer be accessible via this link.

[^8]: <span class="pill">Censored</span> FlashPoint, <https://youtu.be/u7evOBf_E80?t=927>. Unfortunately, since Flashpoint have been censored by YouTube, the video appears to no longer be accessible via this link.

[^9]: Capitol Police Rejected Multiple Offers of Federal Help  
    <https://nypost.com/2021/01/07/capitol-police-nixed-fbi-national-guard-help-before-siege/>  
    <https://www.theepochtimes.com/capitol-police-rejected-multiple-offers-of-federal-help-report_3649044.html>
    
[^10]: "[How a Presidential Rally Turned Into a Capitol Rampage](https://www.nytimes.com/interactive/2021/01/12/us/capitol-mob-timeline.html)", New York Times, Jan. 12, 2021.

[^11]: *Tucker Carlson Tonight* showed several clips from MSNBC and CNN. [Posted here on Twitter](https://twitter.com/ColumbiaBugle/status/1379979576483278848), around 8 Apr 2021. If anyone has links to the full episode, please let us know.

[^12]: "[Another January 6 Falsehood: $30 Million in Damages to the Capitol](https://amgreatness.com/2021/06/07/another-january-6-falsehood-30-million-in-damages-to-the-capitol/)", American Greatness, June 7, 2021.

[^13]: Tucker Carlson Tonight: "[Was the FBI involved in January 6th Capitol Seige?](https://patrickbyrne.locals.com/post/766818/free-video-bombshell-was-the-fbi-involved-in-the-january-6th-so-called-capitol-seige)", reposted by Patrick Byrne.

[^14]: The Nation Speaks: "[Jan. 6 Detainees Confined 23 hrs/day; Risking All for American Dream](https://www.theepochtimes.com/jan-6-detainees-confined-23-hrs-day-risking-all-for-american-dream_3885912.html)", video interview by The Epoch Times TV, July 3, 2021.

[^15]: American Greatness: "[Kash Patel: FBI Director Wray Was ‘Noticeably Absent’ From Cabinet Phone Calls in Run-Up to Jan 6](https://amgreatness.com/2021/07/06/kash-patel-fbi-director-wray-was-noticeably-absent-from-cabinet-phone-calls-in-run-up-to-jan-6/)", July 6, 2021.

[^16]: In an Epoch Times video interview [^14] Ned Lang mentions that his son Jacob was at one time in solitary confinement for an 8-day period, before being released back to the general prison, however in an *American Greatness* article Jacob is quoted to have said “I’ve been in solitary confinement for a hundred days now and haven’t been convicted of any crime with no end in sight.” [^17] Lang told his father his fellow detainees are being tortured “mentally, physically, socially, emotionally, legally, and spiritually.” [^17]

[^17]: American Greatness: "[Shawshank for January 6 Detainees](https://amgreatness.com/2021/05/17/shawshank-for-january-6-detainees/)", by Julie Kelly, May 17, 2021.

[^18]: American Greatness: "[Letters from a D.C. Jail](https://amgreatness.com/2021/06/10/letters-from-a-d-c-jail/)", by Julie Kelly, June 10, 2021.

[^19]: "[Rally for those that are being held in solitary confinement for Jan. 6 Capitol riot](https://www.ptnewsnetwork.com/rally-for-those-that-are-being-held-in-solitaire-confinement-for-jan-6-capitol-riot/)", June 24, 2021.

[^20]: Citizens Against Political Persecution: [Christopher Worrell, 49, of East Naples, FL](https://citizensapp.us/pages/christopher-worrell-49-of-east-naples-fl)

[^21]: American Greatness: "[Did Cops Attack and Provoke Peaceful Protesters on January 6?](https://amgreatness.com/2021/06/28/did-cops-attack-and-provoke-peaceful-protesters-on-january-6/)". Apparently "For the ten minutes prior to encountering the defendant, Officer N.R. can be seen reaching over the metal barrier and pushing a [flag-holding] female protester to the ground on two separate occasions. The protesters ... were by and large peaceful. It was only after tear gas and pepper spray were deployed by police upon this group of peaceful protesters that the crowds changed."

[^22]: American Greatness: "[New Video Depicts Capitol Police Using Stun Grenades on Crowd](https://amgreatness.com/2021/05/27/new-video-depicts-capitol-police-using-stun-grenades-on-crowd/)", May 27, 2021. A separate close-up video of a flashbang actually exploding at the event is [available here](https://rumble.com/vhup1r-capitol-police-fired-exploding-flash-grenade-into-crowd-on-jan.-6-of-men-wo.html).

[^23]: Rumble.com video: [Police Directing Trump Protesters into Capitol](https://rumble.com/vcjbrb-police-directing-trump-protesters-into-capitol.html), published Jan 8, 2021.

[^24]: Rumble.com video: [INSURRECTION DEBUNKED: New Video Inside Capitol Shows Peaceful Protest](https://rumble.com/vhw8xr-insurrection-debunked-new-video-inside-capitol-shows-peaceful-protest.html)

[^25]: Rumble.com video: [Capitol Police Appear To Just Let Protesters Into Building](https://rumble.com/vcjxnp-shocking-new-video-capitol-police-appear-to-just-let-protesters-into-buildi.html)

[^26]: Rumble.com video: [Capitol Police Wave Protesters Inside, Moving Barricades, Stand By as Protesters Walk In](https://rumble.com/vhdh17-videocapitol-police-wave-protesters-inside-moving-barricades-stand-by-as-pr.html)

[^27]: ABC News Australia: "[How did pro-Donald Trump protesters get into Washington DC's heavily guarded Capitol building?](https://www.abc.net.au/news/2021-01-07/how-did-pro-trump-protesters-get-into-capitol-hill-washington/13038568)", 7 Jan 2021

[^28]: YouTube: [Stop the steal protest Washington D.C Jan 6th 2021](https://www.youtube.com/watch?v=MrW1bD9laoU), 9min 46secs long, posted 9 Jan 2021.

[^29]: Apparently the lawyer missed an Aug 24 court hearing for one of his clients, and nobody has seen him since. Just the News: "[Lawyer defending 17 alleged Capitol breachers goes missing](https://justthenews.com/government/courts-law/lawyer-defending-17-capitol-breachers-goes-missing)", Sep 1, 2021.