---
title: Erich Speckin’s Forensic Analysis of Maricopa County Ballots
meta_desc: Forensic document analyst Erich Speckin's report on ballots from Maricopa County.
breadcrumb: "[Reports by State](#colophon) › [Arizona](/fraud-summary-by-state/arizona/)"
last_updated: 8 Feb 2022
---

{% include speckin-report %}


## Official Written Report

<iframe class="scribd_iframe_embed" title="Speckin Report - Forensic Analysis of Paper Ballots in Maricopa County, Arizona, Sep 2021" src="https://www.scribd.com/embeds/557578081/content?start_page=1&view_mode=scroll&access_key=key-QdGlHDqx2xRt4r3v2nas" data-auto-height="false" data-aspect-ratio="0.7729220222793488" scrolling="no" id="doc_69263" width="100%" height="600" frameborder="0"></iframe>

{:.caption}
[Original PDF](https://electioninvestigationlaw.org/wp-content/uploads/2022/01/speckin-report.pdf)


## Why Was The Report Delayed?

His report, dated September 2021, was peculiarly absent from the official Cyber Ninjas forensic audit report released around the same time, and was not available to the public until into 2022. 

Some have speculated [^speckin1] that the report was not deemed useful since it appeared that the 23,000 ballots from an unknown source were split approximately evenly (50% to Biden and 50% to Trump) and thus had no effect on the outcome. However this view naively overlooks the facts that:

1. Speckin's second finding of 11,000 unevenly weighted ballots is still highly significant and could have potentially flipped the state's results

2. If the 23,000 irregularly printed ballots were fraudulent and actually *replaced* another 23,000 legitimate ballots where there was a higher percentage for Trump, then votes were indeed stolen

3. Even if anomalies or falsified ballots did not change the election outcome, they must be officially investigated in order to protect future elections.

The questions about why his report was supressed or excluded are still awaiting answers.


## Government Response

Disappointingly, as of Feb 2022, 4 months since the report was provided to Arizona's Attorney General and the Senate, neither of them have followed-up with Erich to discuss, investigate, or rectify his findings. [^2]


## Erich Speckin Interview

Below is a further follow-up interview between Erich Speckin and Nick Moseder, Feb 1, 2022:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vr6srs/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vtsyve-interview-w-paper-expert-eric-speckin-the-25000-questionable-az-ballots-you.html)



### Acknowledgements

Thank you to [Election Investigation Law](https://electioninvestigationlaw.org/) for bringing this report to our attention.


### Footnotes & References


[^speckin1]: Nick Moseder asks Erich Speckin about this in his interview on Rumble: "[Erich Speckin Interview! The 36,000 "Questionable" AZ Ballots You NEVER Knew About!](https://rumble.com/vtsyve-interview-w-paper-expert-eric-speckin-the-25000-questionable-az-ballots-you.html)", Feb 1, 2022, starting around 21:45 mark.

[^2]: See [^speckin1] around 25:12 mark.