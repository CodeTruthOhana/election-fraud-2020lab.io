---
title: Matthew Braynard's Court Testimony in Fulton County, Georgia
parent_page: /fraud-summary-by-state/georgia/
last_updated: 8 Oct 2021
---


<div class="info state-article-summary" markdown=1>

## Highlights

* 18.39 percent of registered voters of Georgia who were sent but did not return absentee 
ballots did not request absentee ballots;   

*  33.29 percent of voters who were sent absentee ballots but were not recorded as having 
returned absentee ballots stated that they did mail their ballots back;  

* 1.53 percent of registered voters of Georgia who changed addresses before the election 
and were recorded as having voted stated that they did not cast a vote;  

* 20,312 absentee voters were not residents of the State of Georgia when they voted, and 

* 1,043 early and absentee ballots were cast by people who were registered at post office 
box addresses; and 

* 234 Georgians voted in multiple states.

{:.small}
Note: some numbers have been extrapolated from a small sample. Further [canvassing](/canvassing/overview/) may need to be done to confirm these numbers.

</div>


*The following testimony is reproduced from the Expert Report of Matthew Braynard, given in The Superior Court Of Fulton County, State Of Georgia, submitted Dec 3, 2020. [Read full PDF](https://cdn.factcheck.org/UploadedFiles/BraynardReport.pdf) for his background, expertise and entirety of his statement.*

----

## Documents Reviewed 

I reviewed the following documents in arriving at my opinions. 

1. The voter records and election returns as maintained on the State’s election 
database;  

2. Records maintained by the National Change of Address Source which is 
maintained by the United States Postal Service and which is available for 
licensed users on the internet.  I am a licensed member.  

3. Records developed by the staff of my call centers and social media 
researchers; and  

4. A national voter database maintained by L2 Political; 

5. The US Postal Service’s official list of owned and leased facilities.  
In addition, I discussed the facts of this matter with Petitioner’s attorney Erick G. 
Kaardal and members of his legal team.

## Statement Of Opinions 

As set forth above, I have been engaged to provide expert opinions regarding 
analysis in the November 3, 2020 election of Presidential electors.  Based on my review 
of the documents set forth above, my discussions with statisticians and analysts working 
with me and at my direction, my discussions with the attorneys representing the 
Petitioners, I have the following opinions: 

1.  It is my opinion, to a reasonable degree of scientific certainty, that in the State, the 
State’s database for the November 3, 2020 election show 138,029 voters whom the 
state marks as having requested and been sent an absentee ballot did not return it.  
It is my opinion, to a reasonable degree of scientific certainty, that in my sample 
of this universe, 18.39% of these absentee voters in the State did not request an 
absentee ballot. 
 
2.  From the State’s database for the November 3, 2020 election and our call center 
results, it is my opinion to a reasonable degree of scientific certainty that 138,029 
individuals whom the State’s database identifies as having not returned an 
absentee ballot, that in my sample of this universe, 33.29% of those absentee 
voters did in fact mail back an absentee ballot to the clerk’s office. 
 
3.  From the State’s database for the November 3, 2020 election, the NCOA database, 
and our call center results, it is my opinion to a reasonable degree of scientific 
certainty that out of the 138,221 individuals had changed their address before the 
election, that in my sample of this universe, 1.53% of those individuals denied 
casting a ballot. 
 
4.  From the State’s database for the November 3, 2020 election and the NCOA 
database and other state’s voter databases, it is my opinion to a reasonable degree 
of scientific certainty, that at least 20,312 absentee or early voters were not 
residents of the State when they voted. 
 
5.  From the State’s database for the November 3, 2020 election and comparing that 
to the USPS Owned and Leased Facilities Reports, it is my opinion that 1,043 
early and absentee ballots were cast by voters who were registered with a postal 
box disgusted as a residential address.  
 
6.  From the State’s database for the November 3, 2020 election and comparing that 
data to other states voting data and identifying individuals who cast early/absentee 
ballots in multiple states, it is my opinion to a reasonable degree of scientific 
certainty, that at least 234 individuals in the State voted in multiple states.  
 
## Basis And Reasons Supporting Opinions

It is my opinion that due to the lax controls on absentee voting in the November 3, 
2020 election that the current unofficial results of that election include tens of thousands 
of individuals who were not eligible to vote or failed to record ballots from individuals 
that were.   

First, State maintains a database for the November 3, 2020 election which I 
obtained from L2 Political and which L2 Political obtained from the State’s records on, 
among other things, voters who applied for an absentee or early voter status.  I received 
this database from L2 Political in a table format with columns and rows which can be 
searched, sorted and filtered.  Each row sets forth data on an individual voter.  Each 
column contained information such as the name of the voter, the voter’s address, whether 
the voter applied for an absentee ballot, whether the voter voted and whether the voter 
voted indefinitely confined status.   

Second, we are able to obtain other data from other sources such as the National 
Change of Address Database maintained by the United States Postal Service and licensed 
by L2 Political.  This database also in table format shows the name of an individual, the 
individual’s new address, the individual’s old address and the date that the change of 
address became effective.   

Third, I conducted randomized surveys of data obtained from the State’s database 
by having my staff or the call center’s staff make phone calls to and ask questions of 
individuals identified on the State’s database by certain categories such as absentee voters 
who did not return a ballot.  Our staff, if they talked to any of these individuals, would 
then ask a series of questions beginning with a confirmation of the individual’s name to 
ensure it matched the name of the voter identified in the State’s database.  The staff 
would then ask additional questions of the individuals and record the answers. 

Fourth, my team compared the residential addresses of record for early and 
absentee voters and established  

Fifth, attached as Exhibits 2 is my written analysis of the data obtained.   

Below are the opinions I rendered and the basis of the reasons for those opinions.   

{:.small}
>(1.)  It is my opinion, to a reasonable degree of scientific certainty, that in the 
State, the State’s database for the November 3, 2020 election 138,029 
individuals applied for and the State sent an absentee ballot but did not 
return that ballot.  It is also my opinion, to a reasonable degree of scientific 
certainty, that in my sample of this universe, 18.39% of these absentee 
voters in the State did not request an absentee ballot. 
 
I obtained this data from the State via L2 Political after the November 3, 2020, 
Election Day.  This data identified 138,029 absentee voters who were sent a ballot but 
who failed to return the absentee ballot.   

I then had my staff make phone calls to a sample of this universe.  When 
contacted, I had my staff confirm the individual’s identity by name.  Once the name was 
confirmed, I then had staff ask if the person requested an absentee ballot or not.  Staff 
then recorded the number of persons who answered yes.  My staff then recorded that of 
the 722 individuals who answered the question, 630 individuals answered yes to the 
question whether they requested an absentee ballot. My staff recorded that 142 
individuals answered no to the question whether they requested an absentee ballot.  

Attached as Exhibit 2 is my written analysis containing information from the data above 
on absentee voters.  Paragraph 2 of Exhibit 2 presents this information.   

Next, I then had staff ask the individuals who answered yes, they requested an 
absentee ballot, whether the individual mailed back the absentee ballot or did not mail 
back the absentee ballot.  Staff then recorded that of the 583 individuals who answered 
the question, 257 individuals answered yes, they mailed back the absentee ballot.  Staff 
recorded 326 individuals answered no, they did not mail back the absentee ballot.  
Paragraph 2 of Exhibit 2 presents this information.   

Based on these results, 18.39% of our sample of these absentee voters in the State 
did not request an absentee ballot. 

{:.small}
>(2.)  From the State’s database for the November 3, 2020 election and our call 
center results, it is my opinion to a reasonable degree of scientific certainty 
that out of the 138,029 individuals who the State’s database identifies as 
having not returned an absentee ballot, that in my sample of this universe, 
33.29% of those absentee voters did in fact mail back an absentee ballot to 
the clerk’s office. 
 
This opinion includes the analysis set forth above.  Among the 583 who told our 
call center that they did request an absentee ballot and answered the second question, 257 
told our staff that they mailed the absentee ballot back, which is 33.29% of those whom 
the State identified as having not returned the absentee ballot the State sent them. 
Paragraph 2 of Exhibit 2 presents this information.   

{:.small}
>(3.)  From the State’s database for the November 3, 2020 election, the NCOA 
database, and our call center results, it is my opinion to a reasonable degree 
of scientific certainty that out of the 138,221 individuals had changed their 
address before the election, that in my sample of this universe, 1.53% of 
those individuals denied casting a ballot. 
 
On Exhibit 2, in paragraph 4, I took the State’s database of all absentee or early 
voters and matched those voters to the NCOA database for the day after election day.  
This data identified 138,221 individuals whose address on the State’s database did not 
match the address on the NCOA database on election day.  Next, I had my staff call the 
persons identified and ask these individuals whether they had voted.  My call center staff 
identified 2,379 individuals who confirmed that they had casted a ballot.  My call center 
staff identified 37 individuals who denied casting a ballot.  Our analysis shows that 
1.53% of our sample of these individuals who changed address did not vote despite the 
State’s data recorded that the individuals did vote. 

{:.small}
>(4.)  From the State’s database for the November 3, 2020 election and the 
NCOA database and other state’s voter databases, it is my opinion to a 
reasonable degree of scientific certainty, that at least 20,312 absentee or 
early voters were not residents of the State when they voted. 
 
On Exhibit 2, in paragraph 1, I took the State’s database of all absentee or early 
voters and matched those voters to the NCOA database for the day after Election Day.  
This data identified 15,700 individuals who had moved of the State prior to Election Day.  
Further, by comparing the other 49 states voter databases to the State’s database, I 
identified 4,926 who registered to vote in a state other than the State subsequent to the 
date they registered to vote in the State.  When merging these two lists and removing the 
duplicates, and accounting for moves that would not cause an individual to lose their 
residency and eligibility to vote under State law, these voters total 20,312.   

{:.small}
>(5.)  From the State’s database for the November 3, 2020 election and 
comparing that to the USPS Owned and Leased Facilities Reports, it is my 
opinion that 1,043 early and absentee ballots were cast by voters who were 
registered with a postal box disgusted as a residential address.  
 
For this determination, I had my staff compare the official list of leased and owned 
postal facilities provided by the United States Postal Service to the list of early and 
absentee voters. The matches are identified in Exhibit 3.  

We identified 1,043 voters that listed a postal facility as their physical address. In 
many cases these residential addresses disguised their PO box number as an apartment 
number or suite number (E.G. “Apt 5402,” “Suite 305B”, “Unit 305A,” etc.)  

{:.small}
>(6.)  From the State’s database for the November 3, 2020 election and 
comparing that data to other states voting data and identifying individuals 
who cast early/absentee ballots in multiple states, it is my opinion to a 
reasonable degree of scientific certainty, that at least 395 individuals in the 
State voted in multiple states. 
 
On Exhibit 2, in paragraph 2, I had my staff compare the State’s early and 
absentee voters to other states voting data and identified individuals who cast 
early/absentee ballots in multiple states. My staff located 395 individuals who voted in 
the State and in other states for the November 3, 2020 general election.   

[View the Full Report](https://cdn.factcheck.org/UploadedFiles/BraynardReport.pdf){:.button}

----

*Stephen Ansolabehere submitted a rebuttal to the claims above in [his report, here](https://cdn.factcheck.org/UploadedFiles/AnsolabehereReport.pdf).*

