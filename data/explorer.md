---
title: US Election Data Explorer
meta_desc: NYT time-series election data and other statistics, helping shine light on anomalies and potential fraud. JSON and CSV download options.
breadcrumb: "[Follow the Data](#colophon) ›"
wide_layout: true
comment_intro: Have an idea for a chart or graph that you would find useful here? Let us know in the comments below.
last_updated: 13 Jan 2022
---

*<small>Using data from [The New York Times](https://www.nytimes.com/interactive/2020/11/03/us/elections/results-president.html) (NYT), as provided by Edison Research.</small>*

{% include data_explorer.html %}


*More charts and tables may be coming soon, as our volunteer developers are able. Stay tuned, and [follow us on Telegram](https://t.me/ElectionFraud20_org) to get notified.*


{% include raw_data title="Further Data Sources" showExplorer=false %}


## Election Data Analyzer Tool, for Canvassing Voter Registrations

USEIP is currently developing a tool that takes voter registration data, combines it with maps, analyzes it for issues such as large numbers of people at a single address, and helps focus canvassing efforts on areas with the most irregularities.

Learn more about the tool via [this Rumble video](https://rumble.com/vm3iif-before-you-canvass-use-the-election-data-analyzer.html) or from their [Telegram Channel](https://t.me/electiondataanalyzer). It's supported by [USEIP.org](https://useip.org/).