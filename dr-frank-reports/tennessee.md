---
title: Tennessee Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 18 Feb 2022
state: Tennessee
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report_briefer %}

The following video from an election integrity event in Murfreesboro, Tennessee (Jan 22, 2022) includes a presentation from Dr. Frank on his findings, beginning at the 45min mark.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vqhnkk/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vt3tog-election-integrity-dr-frank-professor-clements-and-captain-seth-keshel-1-22.html)


The next video contains Dr. Frank's presentation slides with the findings in Tennessee. It's the slides only, without sound.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vqlrms/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vt7xqo-dr-franks-tennessee-slides.html)

[Dr. Frank's interview with the Tennessee Conservative](https://rumble.com/vshyz9-scientific-research-raises-election-integrity-concerns-interview-with-dr.-d.html) (Jan 13, 2022) also contains additional material.


<!-- { % include dr-frank/notes % } -->

{% include dr-frank/compared-to-other-states %}

