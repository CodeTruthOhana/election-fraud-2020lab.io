---
title: Kentucky Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 18 Feb 2022
state: Kentucky
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report_briefer %}


## Summary of Findings

Dr. Doug Frank studied the data from Kentucky's voter registration rolls and reports numerous concerning issues:

1. Kentucky registration rolls are *irrationally inflated* --- there are more people registered to vote than there are eligible citizens

2. Kentucky registration rolls are *centrally manipulated*

3. Large numbers of *voters are missing* from the registration rolls --- officials cannot tell you exactly who voted

4. In four counties, there were *more votes* for president than there were ballots

5. The ballot demographics in the rolls are unnaturally predictable (see below), even a year after the election


The following video contains Dr. Frank's presentation of his findings in Kentucky. It's the presentation slides only, without sound.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vo8bv8/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vquhze-kentucky-analysis.html)


{% include bloated_voter_rolls %}


## Changes in Kentucky Voter Registrations Over Time

This chart shows the Kentucky voter registration numbers over time for all 120 counties. It's represented as the total number of registrants on the rolls for each county as a percent of its maximum value over this time period.

![](/images/dr-frank/kentucky/voter-charts/1.jpg)

![](/images/dr-frank/kentucky/voter-charts/2.jpg)

In 2019, Judicial Watch sued them for having too many voters in their voter rolls, so KY cleaned up the rolls. Then, there was another lawsuit, and they added them back. Just in time for the 2019 election.

Dr. Frank asks: *If they knew they needed to remove them, why would they deliberately dirty up the rolls again?*

Then, notice the surge in registration rolls leading up to the November 2020 election, and the cleanup that follows.

Suspicious.

{:.small}
Source: [Telegram Post](https://t.me/FollowTheData/1288). Data is publicly available from the [elect.ky.gov](https://elect.ky.gov) website.


## Registration Percentages by County

[![](/images/dr-frank/kentucky/voter-charts/4.jpg)](/images/dr-frank/kentucky/voter-charts/4.jpg)

{:.caption} 
Source: [Telegram Post](https://t.me/FollowTheData/1320), Dec 15, 2021


## Vote Count Anomalies

The following charts show further anomalies identified by Dr. Frank:

[![](/images/dr-frank/kentucky/voter-charts/3.jpg)](/images/dr-frank/kentucky/voter-charts/3.jpg)

{:.caption} 
Source: [Telegram Post](https://t.me/FollowTheData/1319), Dec 15, 2021

[![](/images/dr-frank/kentucky/voter-charts/5.jpg)](/images/dr-frank/kentucky/voter-charts/5.jpg)

{:.caption} 
Source: [Telegram Post](https://t.me/FollowTheData/1325), Dec 15, 2021. Only 54 counties are represented here because they were the only ones Dr. Frank had access to.



{% include dr-frank/compared-to-other-states %}


