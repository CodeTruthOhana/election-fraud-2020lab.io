---
title: Oregon Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 18 Feb 2022
state: Oregon
list_of_counties:
---

{% include see_main_article %}

We've not yet seen a full report on Oregon from Dr. Frank, only this initial chart that shows how 2018 and 2020 registration statistics deviate a long way from the earlier trend. What is the explanation for this?

![](/images/dr-frank/oregon/chart1.jpg)

{:.caption}
Source: [Telegram Post](https://t.me/FollowTheData/1385), Dec 19, 2021