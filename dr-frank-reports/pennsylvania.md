---
title: Pennsylvania Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 18 Feb 2022
state: Pennsylvania
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report %}


## Charts For Each County

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vlsq0l/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>


## Full Presentation on Pennsylvania

27min runtime

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhgztu/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>


## Additional Presentations 

* Dr. Frank joined a 2hr 20min Zoom call with *Audit The Vote PA*, covering additional material, which has been recorded [here](https://rumble.com/vgarr7-audit-the-vote-pa-w-dr.-doug-frank.html).

* A further presentation by Dr. Frank in PA in May 2021 is recorded [here on Bitchute](https://www.bitchute.com/video/9QJGcgmZatyx/)

* [Dr. Doug Frank speaks in Chester County about Election Fraud - Part 1](https://rumble.com/voaigz-dr.-doug-frank-speaks-in-chester-county-on-election-fraud.html) and [Part 2](https://rumble.com/vobasf-dr.-doug-frank-speaks-in-chester-county-about-election-fraud-pt.-2.html) --- presentation in Chester County, PA, Oct 27, 2021.


## Pennsylvania PA04 Voting Demographics History

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/voqivr/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vrcozn-pennsylvania-pa04-voting-demographics-history.html)


## Further Update

{% include tg-embed url="https://t.me/FollowTheData/1298" %}
