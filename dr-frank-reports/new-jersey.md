---
title: New Jersey Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 18 Feb 2022
state: New Jersey
list_of_counties:
---

{% include see_main_article %}

We've not yet seen a full report on New Jersey from Dr. Frank, only this initial comment:

{% include tg-embed url="https://t.me/FollowTheData/1383" %}

