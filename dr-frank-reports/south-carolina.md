---
title: South Carolina Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 28 Jan 2022
state: South Carolina
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report_brief %}

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vl1dsn/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vnnjxd-the-registration-key-for-south-carolina.html)


{% include dr-frank/notes %}

{% include dr-frank/compared-to-other-states %}


