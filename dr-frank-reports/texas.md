---
title: Texas Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 28 Jan 2022
state: Texas
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report_brief %}


<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vnvlbg/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vqhrfm-the-registration-key-for-texas.html)


{% include dr-frank/notes %}

{% include dr-frank/compared-to-other-states %}



## Texas Voting Patterns

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vpwvli/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vsj1pe-texas-voting-patterns-in-the-2020-general-election.html)

