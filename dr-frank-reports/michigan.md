---
title: Michigan Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 28 Jan 2022
state: Michigan
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report_briefer %}


## Preliminary Analysis of Michigan's Election Data

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vfbyhb/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vhy4ml-dr-franks-preliminary-analysis-of-michigans-election-data.html)


## Nine Michigan Counties

Dr. Frank's slides from April 2021, reflecting much of the same material from the video above:

<iframe class="scribd_iframe_embed" title="[02] Dr. Frank #1 [9 Michigan Counties] [040621]" src="https://www.scribd.com/embeds/534306821/content?start_page=1&view_mode=slideshow&access_key=key-NL1FXH2AN3a41zYFjQ21" tabindex="0" data-auto-height="true" data-aspect-ratio="1.2941176470588236" scrolling="no" width="100%" height="600" frameborder="0"></iframe>


## Antrim County Michigan

Further presentation slides from April 2021, focusing specifically on Antrim County:

<iframe class="scribd_iframe_embed" title="[06] Dr. Frank #2 [Antrim Precincts] [042621]" src="https://www.scribd.com/embeds/534306371/content?start_page=1&view_mode=slideshow&access_key=key-iRo1aoEsqoRcShhmoOs9" tabindex="0" data-auto-height="true" data-aspect-ratio="1.7790927021696252" scrolling="no" width="100%" height="600" frameborder="0"></iframe>


{% include dr-frank/compared-to-other-states %}

