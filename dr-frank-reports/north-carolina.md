---
title: North Carolina Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 28 Jan 2022
state: North Carolina
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report comparison=false %}

## Charts For Each County

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vjn8b5/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>


{% include bloated_voter_rolls %}

{% include dr-frank/compared-to-other-states %}
