---
title: Montana Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 23 Nov 2021
state: Montana
list_of_counties:
  - Beaverhead
  - Big Horn
  - Blaine
  - Broadwater
  - Carbon
  - Carter
  - Cascade
  - Chouteau
  - Custer
  - Daniels
  - Dawson
  - Deer Lodge
  - Fallon
  - Fergus
  - Flathead
  - Gallatin
  - Garfield
  - Glacier
  - Golden Valley
  - Granite
  - Hill
  - Jefferson
  - Judith Basin
  - Lake
  - Lewis and Clark
  - Liberty
  - Lincoln
  - Madison
  - McCone
  - Meagher
  - Mineral
  - Missoula
  - Musselshell
  - Park
  - Petroleum
  - Phillips
  - Pondera
  - Powder River
  - Powell
  - Prairie
  - Ravalli
  - Richland
  - Roosevelt
  - Rosebud
  - Sanders
  - Sheridan
  - Silver Bow
  - Stillwater
  - Sweet Grass
  - Teton
  - Toole
  - Treasure
  - Valley
  - Wheatland
  - Wibaux
  - Yellowstone

---

{% include see_main_article %}

{% include dr-frank/state_report %}



## Dr. Frank's Presentation at *Take Back Montana*

The *Take Back Montana* event featured Captain Seth Keshel, Professor David Clements, and Dr. Douglas Frank (from 1hr 53min mark), Sep 25, 2021:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vkbk6c/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vmxqb2-take-back-montana-2020-election-analysis.html)

<!-- See also https://rumble.com/vmz1gu-take-back-montana-event-dr.-douglas-frank.html -->


## Counties with High/Low Correlations

![](/images/dr-frank/montana/correlations.jpg)



{% for county in page.list_of_counties %}

## {{ county }} County

![{{ county }} County 2020 Election Analysis Chart by Dr. Doug Frank](/images/dr-frank/montana/{{ county | slugify }}.jpg)

----

{% endfor %}

