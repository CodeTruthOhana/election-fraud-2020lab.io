---
title: Washington Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 18 Feb 2022
state: Washington
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report_brief %}

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhgzww/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vk3626-the-registration-key-for-washington-state.html)


{% include dr-frank/notes %}


## Presentation at Washington Election Integrity Public Hearing 

Dr Frank's 51min presentation at Washington Election Integrity Public Hearing at Heritage Ranch in Bow, WA on Aug 29th, organized by Bill Bruch, Chair of the Washington State Election Integrity Committee.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vjxojo/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vmjuoo-dr-franks-presentation-at-washington-election-integrity-public-hearing.html)

[Dr. Frank's presentation in Idaho](https://rumble.com/vo7wa6-dr.-douglas-frank-nationwide-overview-of-results.html) also covers some material related to Washington.



{% include dr-frank/compared-to-other-states %}

