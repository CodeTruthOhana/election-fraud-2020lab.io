---
title: Colorado Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 18 Feb 2022
state: Colorado
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report %}

## Charts For Each County

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vkawca/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

## Dominion's "Software Update" That Wiped Election Data in Mesa

In the following video Dr. Frank explains the forensic evidence proving that Dominion wiped important election data from the machines in Mesa County:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/viep8l/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vl0vdv-dominion-software-update-in-2021-deleted-voting-data-from-machine-in-mesa-c.html)

