---
title: Ohio Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 18 Feb 2022
state: Ohio
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report_brief %}

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhh044/?pub=4" frameborder="0" allowfullscreen></iframe>



{% include dr-frank/notes %}


## Dr. Frank's Keynote Speech in Wellington, Ohio

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhh0h6/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vk36mg-dr-frank-in-wellington-ohio.html)



{% include dr-frank/compared-to-other-states %}



## Further Updates on Ohio

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="FollowTheData/1260" data-width="100%"></script>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="FollowTheData/1298" data-width="100%"></script>

{% include tg-embed url="https://t.me/FollowTheData/1433" %}


## Methodology and Replicating the Findings

An independent researcher replicated Dr. Frank's statistics and demonstrated the process of doing so in a video presentation [available here](https://rumble.com/vryl2j-confirming-dr-douglas-frank-ohio-key-step-by-step-richland-county.html).

