---
title: Indiana Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 28 Jan 2022
state: Indiana
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report_brief %}

{:.hint}
*Note: Dr Frank removed the following Indiana slideshow video from Rumble for a reason unknown to us, possibly because it contained an error. Please confirm with him before acting on this data.*

{% include tg-embed url="https://t.me/ElectionFraud20_org_discussion/3057" %}


{% include dr-frank/notes %}

{% include dr-frank/compared-to-other-states %}



