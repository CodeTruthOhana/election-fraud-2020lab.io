---
title: West Virginia Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 23 Nov 2021
state: West Virginia
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report %}

## Charts For Each County

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vktqax/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>