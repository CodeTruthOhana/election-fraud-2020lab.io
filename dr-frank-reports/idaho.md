---
title: Idaho Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 18 Feb 2022
state: Idaho
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report_brief %}

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhhr2e/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vk3x7o-idaho-registration-key.html)


{% include dr-frank/notes %}


## Dr. Frank's Presentation in Idaho

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vllq5g/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vo7wa6-dr.-douglas-frank-nationwide-overview-of-results.html). October 9th, Greyhound Park & Event Center, Post Falls, ID.



{% include dr-frank/compared-to-other-states %}



