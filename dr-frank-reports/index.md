---
title: Dr. Douglas G. Frank’s Election Analysis
last_updated: 19 Feb 2022
breadcrumb: "[Follow the Data](#colophon) ›"
meta_desc: An overview of Dr. Douglas Frank's analysis of the 2020 election, and the anomalies he found.
comment_intro: What are your thoughts on Dr. Frank's analysis? Or have you found additional materials related to his work that would benefit others? Let us know in the comments below.
covered_telegram_posts_up_until: 28 Dec 2021
---

{:.small}
*Introductory material sourced from [Montana Election Integrity Project](https://www.mtelectionintegrity.com/news/40-year-elections-modeler-predicted-outcome-of-53-of-56-montana-county-elections).*

Dr. Douglas G. Frank is a 40-year modeler of pandemics and elections, a renowned scientist, and inventor who received his PhD in Electrochemistry at UC Santa Barbara. He has authored approximately sixty peer-reviewed scientific reports, including feature and cover articles in the leading scientific journals in the world (Science, Nature, Naturwissenschaften). 

Dr. Frank built upon Kurt Hyde’s theory from 2010 that suggested that US Census data could be used to artificially inflate voter registration databases across the country. He further asserted that voter registration databases were indeed inflated during the 2020 General Election and that, using an algorithm that he theorizes was developed at the state level and applied to the voter registration information in each individual county in a state, the voter turnout of the election was predictable to an unusually high level of accuracy.

Using 2010 US Census data and updated 2019 American Community Survey data from the Census Bureau, Dr. Frank is able to predict not only the voter turnout by age, but voter registration by age. When Frank’s prediction is compared to the actual voter turnout and voter registration by age, it correlates to an unusually high level of accuracy. In the case of Missoula County, and most other of the 55 other counties in Montana, his prediction was correlative to 0.99.

![Dr Frank Missoula, Montana](/images/dr-frank/montana/missoula.jpg)

{:.small}
Missoula County election results analysis. Dr. Douglas G. Frank. September 21, 2021.  
**Blue Line:** Population  
**Black Line:** Registered Voters  
**Red Line:** Ballots cast  
**Light Blue Line:** Predicted ballots based on US Census Bureau data combined with Dr. Frank's algorithm

Notice how closely the red and light blue lines are in alignment.

Counties across Montana had more registered voters in certain age groups than eligible voters exist in the population, and in many cases in older cohorts, voter turnout was nearly 100% and sometimes exceeded the total eligible population. 

More astonishingly, the same, exact algorithm (or “key” as Frank calls the mathematical formula) used in Missoula County could be applied to any other county in Montana (except for Petroleum, Treasure, and Wibaux counties) to predict their voter registration and turnout by age, too. See the [detailed charts for all of Montana](montana/).

Dr. Frank has performed his analysis on at least 14 other states and has found the same disturbing pattern where he is able to predict the voter turnout rate for every age bracket in many counties based only US Census Bureau data. <!-- and registration numbers too, possibly? -->

The following chart shows an overview of each state and how "clean" or "dirty" their voter rolls and turnout numbers appear to be:

![Dr. Frank National Overview Chart](/images/dr-frank/all-states.jpg)

{:.caption}
Source: [Telegram Post](https://t.me/FollowTheData/1172), Nov 28, 2021


## Findings By State

{:.dr-frank-states.responsive-table.small-on-mobile}
&nbsp; | Detailed Charts | Chart Slideshow | Detailed Video
-|-
[Alabama](alabama/) ||✔️|
[Arizona](arizona/) |||✔️
[Colorado](colorado/) |✔️|✔️|
[Florida](florida/) ||✔️|
[Idaho](idaho/) ||✔️|
[Indiana](indiana/) ||✔️|
[Kansas](kansas/) |✔️|✔️|
[Kentucky](kentucky/) | Some |✔️|
[Michigan](michigan/) || Some |✔️
[Missouri](missouri/) ||✔️|
[Montana](montana/) |✔️||
[Nebraska](nebraska/) ||✔️|
[New Jersey](new-jersey/) |||
[North Carolina](north-carolina/) |✔️|✔️|
[Ohio](ohio/) ||✔️|✔️
[Oregon](oregon/) |||
[Pennsylvania](pennsylvania/) |✔️|✔️|✔️
[South Carolina](south-carolina/) ||✔️|
[Tennessee](tennessee/) ||✔️|
[Texas](texas/) ||✔️|
[Utah](utah/) ||✔️|
[Washington](washington/) ||✔️|
[West Virginia](west-virginia/) |✔️|✔️|
[Wisconsin](wisconsin/) |Some|✔️|✔️

<style>
    .dr-frank-states td:not(:first-child) {
        text-align: center;
    }
</style>


## State Registration & Turnout Rates Compared

The following chart shows which states have the highest percentage of eligible voters (18 year-olds and above) actually registered to vote, with some states above 100%:

![State Voter Registration Rates 2020](/images/dr-frank/state-reg-rates.jpg)

{:.caption}
Source: [Telegram Post](https://t.me/FollowTheData/1181), Nov 28, 2021

This chart shows the states with the highest percentage of registered voters who actually voted in Nov 2020:

![State Voter Turnout Rates 2020](/images/dr-frank/state-turnout-rates.jpg)

{:.caption}
Source: [Telegram Post](https://t.me/FollowTheData/1175), Nov 28, 2021

By combining the two charts together, we can see which states are really pushing these two strategies heavily:

![State Voter Registration + Turnout Rates 2020](/images/dr-frank/state-reg-combined.jpg)

{:.caption}
Source: [Telegram Post](https://t.me/FollowTheData/1177), Nov 28, 2021




<!-- Dr. Frank was able to predict voter turnout by age and voter registration by age with a very high degree of accuracy (a correlation of 0.996). He also found that counties across Montana had more registered voters in certain age groups than eligible voters in the population. Dr. Frank shared his analysis of all 56 Montana counties at the Montana Election Integrity livestream and in-person event Take Back Montana: 2020 Election Analysis. -->


## Written Reports

<!-- Should move these two onto a Michigan page -->

* [Nine Michigan Counties](https://www.scribd.com/document/534306821/02-Dr-Frank-1-9-Michigan-Counties-040621)
* [Antrim County, Michigan Voting Precinct Demographics](https://www.scribd.com/document/534306371/06-Dr-Frank-2-Antrim-Precincts-042621)


## Video Presentations

The simplest possible explanation of his findings is covered in this 3min video clip *A Simple Metaphor*:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhh0s0/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

A more detailed overview of Dr. Frank's work can be seen in his 1-hour interview with Mike Lindell, *Scientific Proof*:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vcmcjz/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

He also presented his findings at Lindell's Cyber Symposium, Aug 10--12, 2021 (which we [covered in another article here](/in-detail/cyber-symposium-mike-lindell/)):

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vid7gk/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Read transcript](https://theconservativetreehouse.com/blog/2021/08/14/transcript-of-dr-douglas-g-frank-presentation-during-cyber-symposium/)


### Other Presentations

* [Introduction to the Key: Dr. Frank](https://rumble.com/vj98g1-introduction-to-the-key-dr.-frank.html) --- Dr. Frank explains on Frankspeech.com the introduction to the key of the algorithm. Jun 30, 2021.

* [Dr. Frank: Divine Appointments, Algorithms, and Saving the Country One Step at a Time](https://rumble.com/vhf7tz-dr.-frank-divine-appointments-algorithms-and-saving-the-country-one-step-at.html) --- interview with Professor David Clements, May 21, 2021.

* [Truth & Liberty Coalition Livecast: Dr. Douglas G. Frank on Analyzing the 2020 Election](https://truthandliberty.net/episode/dr-douglas-g-frank-on-analyzing-the-2020-election/), May 31, 2021 (also [on Rumble](https://rumble.com/vn63gg-dr.-douglas-g.-frank-analyzing-the-2020-election.html))

* [Truth & Liberty Coalition Livecast: Dr. Douglas G. Frank on Following the Election Data to Find Evidence](https://truthandliberty.net/episode/dr-douglas-g-frank-on-following-the-election-data-to-find-evidence/), Aug 2, 2021 (also [on Rumble](https://rumble.com/vkolk1-dr.-douglas-g.-frank-on-following-the-election-data-to-find-evidence.html))

* [Mike Lindell Cyber Symposium: Dr. Douglas Frank "2020 Election Algorithms"](https://rumble.com/vkzdlu-mike-lindells-cyber-symposium-dr.-douglas-frank.html), Aug 11, 2021

* [LINDELL CYBER SYMPOSIUM: Dr. Douglas Frank and Col. Kurt Hyde on Election Fraud and Integrity](https://rumble.com/vlgfsl-lindell-cyber-symposium-dr.-douglas-frank-and-col.-kurt-hyde-on-election-fr.html) --- interview by The New American, Aug 20, 2021

* [Dr Frank's Presentation at Washington Election Integrity Public Hearing](https://rumble.com/vmjuoo-dr-franks-presentation-at-washington-election-integrity-public-hearing.html), Heritage Ranch, Bow WA, Aug 29, 2021

* [Take Back Montana: 2020 Election Analysis](https://www.youtube.com/watch?v=Fc8E3_Qvl6o) --- featuring Captain Seth Keshel, Professor David Clements, and Dr. Douglas Frank (from 1hr 53min mark), Sep 25, 2021

* [FrankSpeech Thanks-A-Thon Special Program Joined by Brannon Howse and Dr. Douglas G. Frank](https://frankspeech.com/video/frankspeech-thanks-thon-special-program-joined-brannon-howse-and-dr-douglas-g-frank), Nov 28, 2021

* [Ann Vandersteel Interviews Dr. Frank](https://t.me/SteelTruthChannel/6257), Dec 8, 2021

* [Dr. Frank's Presentation to Wisconsin Assembly Committee on Campaigns and Elections](https://rumble.com/vqj701-dr-frank-speaking-at-election-committee-12-8-21.html), Dec 8, 2021 <span class="pill">Major Government Hearing</span>

* [Let's Talk America: Jon Schrock Guest Hosting Featuring Dr. Douglas Frank](https://www.brighteon.com/b8815ad9-4868-4a21-81e3-b1e344721d4c), Dec 10, 2021

* [Dr. Frank Interview with William Wallis](https://rumble.com/vqq95g-dr-frank-interview-with-william-wallis.html), Dec 14, 2021

* [United States Election Stats](https://rumble.com/vr3gar-united-states-election-stats.html) (slides only, no sound), Dec 20, 2021

* [United States Election Turnout Presentation](https://rumble.com/vrg64f-united-states-election-turnout.html) (slides only, no sound), Dec 26, 2021


## Methodology

An independent researcher demonstrated Dr. Frank's process and statistics and how he was able to replicate the findings. [Watch the video here](https://rumble.com/vryl2j-confirming-dr-douglas-frank-ohio-key-step-by-step-richland-county.html).


## Supreme Court Case

Dr. Douglas Frank publicly confirmed  that he is contributing a significant amount of research and data into an upcoming Supreme Court case. In a [video interview with Pete Santilli](https://rumble.com/vp4i7h-dr.-frank-epic-interview-with-pete-santilli-november-12-2021.html) (from 21min mark) he gives more background to the case.

He also suggests that the much-discussed packet captures (PCAPs) that Mike Lindell has been in possession of were actually captured using government (CISA) infrastructure, which is why they were not able to be legally released to the public previously. But they will be included in this upcoming case on November 23.

<div class="info" markdown=1>
#### Get Involved

The upcoming Supreme Court case needs Republican State Attorneys General to sign on as litigants. Let your state representatives know that you want them to support this action.
</div>


## Follow Dr. Frank's Updates

<iframe id="preview" style="border:0px;height:500px;width:500px;margin:5px;box-shadow: 0 0 16px 3px rgba(0,0,0,.2);" src="https://xn--r1a.website/s/FollowTheData"></iframe>

