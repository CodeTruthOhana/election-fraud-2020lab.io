---
title: List of Election Audit Channels on Telegram
meta_desc: A detailed list of Telegram channels pursuing election integrity, following 2020 Presidential election.
last_updated: 10 Apr 2022
comment_intro: Know of other reputable election integrity channels? Let us know in the comments below.
---

Join forces with others in your region who are working towards election integrity. Because of the heavy censorship occurring on Twitter and Facebook, most groups have moved to Telegram.

Download the app at [Telegram.org](https://telegram.org/).

Here's a list of the Telegram channels we know about so far that are working together for election audits and other integrity measures:

{:.warning.emoji}
⚠️ **WARNING:**
We encourage everyone to use discernment. If you feel a channel is not meeting your expectations please join another one or create a new one. 

{:.info.emoji}
💡 You can also volunteer with USEIP which has grassroots groups working across many states, although they don't use Telegram for collaboration. Visit [USEIP.org](https://useip.org/get-involved/) to sign up.


<table>
{% for channels in site.data.telegramAuditChannels %}
    {% assign stateData = site.data.states | find: 'abbr_lowercase', channels[0] %}
    <tr>
        <th>
            {% if stateData %}
                {{ stateData.name }}
            {% else %}
                Nation-Wide
            {% endif %}
        </th>
        <td>
            {% for channel in channels[1] %}
                {% unless forloop.first %}
                    <br>
                {% endunless %}
                {% include telegram_link channel=channel %}
            {% endfor %}
        </td>
    </tr>
{% endfor %}
</table>

If there's no Audit Force for your state and you would like to get plugged in, join the [Audit Force State Requests Discussion](https://t.me/joinchat/lyijRUjHark1NmQx).


### Other Election Integrity Channels

[@LibertyOverwatch](https://t.me/LibertyOverwatchChannel)  
[@RealSKeshel](https://t.me/RealSKeshel) (Seth Keshel)  
[@FollowTheData](https://t.me/FollowTheData) (Dr. Doug Frank)  
[@electiondataanalyzer](https://t.me/electiondataanalyzer) (a tool for analyzing voter registration anomalies)  
[@theprofessorsrecord](https://t.me/theprofessorsrecord) (Professor David K. Clements)
[@electionevidence](https://t.me/electionevidence)  
[@KanekoaTheGreat](https://t.me/KanekoaTheGreat)  
[@maninamerica](https://t.me/maninamerica)  
[@praying_medic](https://t.me/praying_medic)  
[@joeoltmann](https://t.me/joeoltmann) (Joe Oltmann)  
[@JovanHuttonPulitzer](https://t.me/JovanHuttonPulitzer) (Jovan Pulitzer)  
[@CodeMonkeyZ](https://t.me/CodeMonkeyZ) (Ron Watkins)  
[@drawandstrikechannel](https://t.me/drawandstrikechannel)  

And last, but not least, our own channel [@ElectionFraud20_org](https://t.me/ElectionFraud20_org)

