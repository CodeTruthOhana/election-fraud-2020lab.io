---
title: How to Volunteer for Grassroots Canvassing In Your State
meta_desc: How to get involved in canvassing in your state, including contact details for groups already running.
parent_page: /canvassing/overview/
last_updated: 10 Apr 2022
comment_intro: Do you know of other canvassing efforts going on around the country? Or know of details we might have missed? Let us know in the comments below.

## List of states and who to contact:
canvass_teams_by_state:

  Alabama: The [Alabama Audit Force](https://t.me/joinchat/zZuwUzDRcL44Yzlh) are coordinating volunteers of various kinds for canvassing, research, FOIA (sending information requests off), gathering fraud, etc. [@Dmiddleof5](https://t.me/Dmiddleof5) and [@AMFIO6](https://t.me/AMFIO6) are overseeing canvassing. If you're able to help out, send them a message with your name and county. You can also join [Alabama Audit Force Discussion](https://t.me/joinchat/ObRhKuycxCZmOTEx).

  # Arkansas: ...to review Audit Force Channel...

  Arizona: Liz Harris organized the grassroots canvassing in Maricopa County earlier in 2021, with [significant findings](/canvassing/#arizona). Join her [Telegram channel](https://t.me/VoteLizHarris) to discuss other opportunities to volunteer and assist with any further canvassing.
  
  # Others in Arizona? Pima County? TBC.....

  California: |    
    Firstly, visit [USEIP › Get Involved](https://useip.org/get-involved/), complete your details, and tick the box that says "I want to help canvass!"

    Also try joining the [California Audit Force Discussion](https://t.me/joinchat/FF0GSJRJouxhNTBh) and [California Audit Chat](https://t.me/CaAuditChat) channels and connecting with others in your area. 

    In you're in Nevada County, get in touch with [Citizen Auditors of Nevada County](https://citizenauditorsofnevadacounty.com/) who are keen to connect with volunteers to assist with auditing the election results.
    
    Telegram channel members [@Image_Bearer](https://t.me/CAAuditChat/85715) and [@dtrusner](https://t.me/CAAuditChat/83965) also appear to be working on canvassing groups, so send them a message.

  # Also Urson R. is connected with California Audit Force and USEIP -- would be good to mention them once we've confirmed the details.

  Colorado: Visit [USEIP › Get Involved](https://useip.org/get-involved/), complete your details, and tick the box that says "I want to help canvass!"

  # Delaware: ...no mention on Telegram channel thus far, only that which was done months earlier...

  Florida: Visit [DefendFlorida.org](https://defendflorida.org/) for more information and to find out how to get involved.

  Georgia: Anyone interested in canvassing in Georgia can email <msmoss@protonmail.com> and <gapatriots@protonmail.com> and tell them you want to volunteer to canvass.

  # Added 10 Apr 2022
  Hawaii: |
    Email <VerifyVotersHI@protonmail.com> for more information on canvassing in Hawaii and how to get involved. 

  # The [Audit the Vote Hawaii](https://t.me/+fxWLfX8vVbEwNzJh) Telegram group may also provide assistance with getting involved. (TBC)

  Illinois: It was mentioned on our Telegram group that anyone who lives in Illinois and wants to be involved can call T.J. Gaynor on [773-474-3853](tel:773-474-3853).

  Indiana: |
    Canvassing teams are being recruited through two grassroots groups:
    
    * [Indiana First Action](https://t.me/INAuditChat2) have almost 2,000 people working on election integrity and are continuing to canvass as of March-April 2022. Join their [Telegram channel](https://t.me/INAuditChat2), [Facebook group](https://www.facebook.com/groups/410962590678872/?ref=share_group_link), or email <indianafirstaction@protonmail.com>.
    
    * The [Indiana Audit Force](https://t.me/joinchat/MNvpESJu9OhjZDBh) 
    are making excellent progress, are well organized and have the benefit of national expert support as well as effective networking among many states. They are recruiting county captains and willing volunteers to help visit suspicious addresses (you may only need as little as 2 hours to help out). Join the channel to express your interest, or you can also message Peter on Telegram at [@pemery](https://t.me/pemery).  

  # INAuditChat2 are accepting interested volunteers who are encouraged to join a group for their respective county. They'd also love assistance from teachers, human resource managers or training facilitators, in addition to willing door-knockers. Reach out to [@MommaBear_TrumpWon](https://t.me/MommaBear_TrumpWon) and [@HippieHippiest](https://t.me/HippieHippiest) for more information on how to find your county group.

  Iowa: |
    There is a group starting up to canvass in Iowa, with Liz Harris and Jovan Pulitzer guiding them through the process. They are looking for a team of willing volunteers. 

    Join the [Iowa Canvassing & Kinematics Telegram Group](https://t.me/joinchat/yQifgM_19ZQ0N2Fh).    

    You must be a registered voter in Iowa to participate, and be able to dedicate at least 2 hours per week to the group. Please identify the county you are from using the hashtag, example `#Scott`.

  Kansas: The [Kansas Audit Force](https://t.me/joinchat/hvrTo2UkdbhkZjgx) are not canvassing as yet, but are laying the groundwork in preparation for it. Join the channel and express your interest in helping out.

  Maine: A team in Maine is getting ready to canvass, following the Liz Harris plan that was used in Arizona. If you are interested in volunteering, message [@WillTheRealJoeBidenPleaseStandUp](https://t.me/WillTheRealJoeBidenPleaseStandUp) on Telegram. You can also join [Maine First Audit Chat](https://t.me/MEAuditChat) to find out more.

  Maryland: Join the [Maryland First Canvass](https://t.me/joinchat/KmnnObTo09M0Y2Ex) channel if you're interested in canvassing, and let them know which county you're in.

  Michigan: | 
    Canvassing in Macomb County (north-eastern Detroit) is happening most Saturdays, and some weekdays. Express your interest for Jacky Eubank's team, by emailing <joanne.electioninfo@gmail.com>. 
  
    Additional details on [Telegram here](https://t.me/MichiganAuditChat/75568). If you are able to do weekday canvassing, contact Sue Hefling, [(586) 876-3010](tel: 586 876-3010).

    Muskegon, Michigan is also canvassing. Contact Paula at [(231) 286-3048](tel:231 286-3048).

  Mississippi: |
    They are preparing to roll out canvassing, but need people to help as County Captains! Join [Mighty Mississippi Canvassing](https://t.me/mscanvas) on Telegram, which is aligned with [USEIP](https://useip.org).

    There's also [Mississippi AFA Audit & Canvas Chat](https://t.me/mightymississippi), or for anyone not on telegram, they can email Emily at <emilyinms@gmx.com>.

  Missouri: Join the Telegram channels [Missouri Audit Force](https://t.me/joinchat/zz2Do1xwurwwZDIx) and [Missouri Audit Force Discussion](https://t.me/joinchat/V06B_AayQ2EwZDYx). If you're interested in helping canvass, [complete this form](https://qfreeaccountssjc1.az1.qualtrics.com/jfe/form/SV_0V5w98gKgKiayai). In October, canvassing will be underway near the airport north of Kansas City.

  Montana: |
    [The Montana Election Integrity Project](https://www.mtelectionintegrity.com) has launched *Restore Montana: Canvass Montana* -- a campaign aiming to gather volunteers for door-to-door canvassing and other roles, to assist in identifying phantom and ghost voters. This is a bi-partisan, citizen-led effort. Visit the [Canvass Montana](https://www.mtelectionintegrity.com/take-action) page to submit your details.

  # Last updated 10 Apr 2022
  Nebraska: There is canvassing going on in approximately 7 counties. Get connected in via [Nebraska Voter Accuracy Project](https://t.me/NebraskaVAP) Telegram group.

  Nevada: |
    [@NevadaAuditChat](https://t.me/NevadaAuditChat) currently has FIVE work groups set up: Canvassing, FOIA, Research, Documents, Communication. Volunteers can choose one or more and post in the channel which team(s) they want to join. Then direct-message any admin, telling them a little about yourself (if interested in canvassing, include your county) so they have an idea how to best utilize your skills/talents.

  New Hampshire: |
    [New Hampshire Audit Force](https://t.me/joinchat/xsz-DMNKxJkxNDEx) is canvassing in Rye (Rockingham County) and Waterville Valley (Grafton County), before potentially expanding elsewhere. They are looking for canvassers and data entry assistants. Express your interest via the [NH Audit Force Channel](https://t.me/joinchat/xsz-DMNKxJkxNDEx), [Discussion Channel](https://t.me/joinchat/WeaFTNinIg41Njdh), [@NHVoterIntegrityGroup](https://t.me/NHVoterIntegrityGroup) or via emailing <action@nhvoterintegrity.org>.

  New Jersey: Join the [New Jersey First Audit Chat](https://t.me/NJAuditChat) channel on Telegram. Listen to the [Sep 24 statewide audio chat](https://t.me/NJAuditChat/42292) which explains the latest progress. Then, if you'd like to volunteer, send a direct message to [@ForPublius2021](https://t.me/ForPublius2021) with brief introduction on yourself & she will contact you about joining the appropriate group.

  ### DISABLED FOR NOW ###
  # New Jersey: Volunteers are keen to get into canvassing, but GOP and County Officials are stalling the process, and they cannot obtain voter rolls. If anyone can assist, connect with .... (NOT SURE WHO.)
  # "Prayer Trooper" mentioned this on our EF20 Telegram channel, but they were not able to point Si to a coordinator for the state, so commenting this out for now, until we have better, verified information.

  New Mexico: |
    [New Mexico Audit Force](https://t.me/joinchat/GZAUJl8wM7pjYzdh) have an ongoing address canvassing effort going on that you can get involved in now. Address canvassing involves plugging addresses into a free mapping application to find the obvious fraud like people registered to empty lots and businesses. Then going door to door to verify the rest of the names are legit. If you’re interested in helping with this please direct message [@erinclements](https://t.me/erinclements) with your email and county in NM where you live. 

  North Carolina: |
    The [North Carolina Audit Force](https://t.me/joinchat/cEG1T37INmVjZDNh) has been running canvassing training with the intention to start canvassing around Raleigh in early Oct and then proceed to other counties. Join the main channel and [discussion group](https://t.me/joinchat/x6kJUvQVMdIzYWE5) and introduce yourself and your desire to assist with canvassing.

    [A form here](https://docs.google.com/forms/d/e/1FAIpQLSd312hJN-Dq9Ni5bSU_m_CmrIVzrcKKyoBwDsihZ2m2OAXEOA/viewform) allows you to request access to one of the private, by-invite-only community groups set up by location. If you don’t feel comfortable using this form you can email <NCAFCanvassing@gmail.com> and let them know you're interested in getting involved in a community group.

  Ohio: If you are in Ohio and interested in helping, check out [@OhioAuditChat](https://t.me/OhioAuditChat). When the admin welcomes you, let them know what county you're in and they will send you the link to that county. Some are further along than others. Delaware County is planning on having training in October and starting to canvass the end of that month. [@OhioAuditForce](https://t.me/joinchat/ZwtRXRnHRW8wYTRh) is also exploring canvassing in areas not yet covered.

  Oregon: If you would like to be included in canvassing to prove that the voter lists are inaccurate, help find ghost/phantom voters and lost votes, please private message G. Tall, [@Seven_Spirits](https://t.me/Seven_Spirits) on Telegram. They have groups growing in many counties. Some are already canvassing. Interest is growing. Also join the [Oregon Audit Force](https://t.me/joinchat/Vk-NsBm1NkpjZTdh). They are also getting resources on training and process together now which will be shared. They've also posted a [presentation on Canvassing basics](https://www.dropbox.com/s/stef6s8l996i4jq/Jovan%20Hutton%20Pulitzer%20Canvassing%20101%20-%20Module%201.mp4?dl=0).

  Pennsylvania: |
    Currently they have over 500+ volunteers in 56 counties with canvassing in approximately 20 counties. If interested, please use the contact tab on [AuditTheVotePA.com](https://www.auditthevotepa.com) and send them a message. Include your name, email, and the county you live in.

    See also [@AuditTheVotePA](https://t.me/auditthevotepa) and [@PennsylvaniaAudit](https://t.me/PennsylvaniaAudit) on Telegram.

  South Carolina: |
    The South Carolina Audit Force is a 100% grass-roots citizen effort, with groups forming in each county within the state to fight for election integrity. Join the [South Carolina Audit Force Discussion](https://t.me/joinchat/jqkatg7dMpg3ODJh) on Telegram and express your interest in helping for a state audit.

    The [South Carolina Safe Elections Group](https://www.auditsouthcarolina.org/) are also performing similar work in SC and would love to hear from willing volunteers. They also have a [Telegram Channel here](https://t.me/joinchat/sRfrsM-iuvoxODAx).

  South Dakota: |
    South Dakota is getting organized for a canvass of the [three suspect counties identified by Seth Keshel](/seth-keshel-reports/south-dakota/): Minnehaha, Pennington, and Lincoln. Email Teresa at <SDCanvass@protonmail.com> for more information. There's also a [Telegram group for canvassing here](https://t.me/joinchat/H1PRBtAkgOM0YzAx).

  Tennessee: Travis Thompson is gathering an audit force team in TN for anyone living in the state who wants to be involved. Email him at <travisfromtennessee@gmail.com>. Also join the [Tennesee Audit Force](https://t.me/joinchat/Rowpjb68LaE2ZjAx) and [Audit Force Discussion Group](https://t.me/joinchat/MJRE8snzNblmZmYx) to express your interest.

  Texas: |
    [TX Voter Verification](https://www.txvoterverification.org/) (affiliated with Audit Force) is coordinating both physical and digital canvassing efforts in Central Texas (currently Bell, Bexar, Hays, Travis & Williamson counties). Those interested in volunteering for these counties can email <Volunteer@TXVoterVerification.org> to be connected with the Lead for your county. 
    
    You can also find your specific county channel by requesting information on the [Texas Audit Force Telegram Channel](https://t.me/joinchat/hFfC4AUeEGg3Y2Fh).

  Virginia: |
    Virginia is full speed ahead. They are engaged in training and forming teams now. Canvassers have been active in several localities. Check <https://virginiansforamericafirst.com/events/> for info on training seminars.

    All canvass volunteers need to go through a vetting process, but you are provided with identification, a walkbook app, and legal documents for record findings. 
    
    A team of attorneys has been assembled and are in process of reviewing collected anomalies for further investigation by law makers. They are working with multiple State Senators who will use the data to make the legislative changes necessary for free, fair and transparent elections in VA.
    
    Contact [Joshua Pratt](mailto:VFAFLYH@gmail.com) and/or sign up for more info at [ElectionTransparency.net](https://ElectionTransparency.net) or [this signup form](https://mailchi.mp/f1c4c0b8a60e/electiontransparency).

  # There's also Virginia First Audit by @PeachesNmoxie - awaiting them to confirm details:
  # "Virginia First Audit is conducting canvassing to rectify errors in the 2020 election. We are working in alignment with USEIP and Maricopa canvassing protocols. We are focusing on areas not addressed by other organizations."

  Washington: |
    The [Skagit Voter Integrity Project](https://skagitrepublicans.com/ElectionIntegrity) (SVIP) previously canvassed over 3,000 properties in Skagit County, with [significant findings](/canvassing/#washington). 

    An event in Post Falls on Sat Oct 9 was recruiting volunteers for canvassing in Spokane County where approx 700 addresses look fishy. We don't yet have details of who is coordinating, but ask in [Washington First Audit Chat](https://t.me/WAAuditChat) and if you find out, please let us know.
    
    Canvassing may be happening in other counties also, so ask around.

    Volunteers are also being sought by the [Washington First Audit Channel](https://t.me/WAAuditNews/134) for other tasks across three categories: Document Group, Communication Group, and FOIA Group. Post in the channel to learn more and to let them know of your availability.

  West Virginia: The [West Virginia Audit Force](https://t.me/joinchat/ppYYAEzpSIQ5Y2Mx) are in the early stages of planning their canvassing, identifying anomalies in the data and may then visit addresses as needed. If you're able to assist, join the channel and also send a direct message to [@BitBanger](https://t.me/BitBanger) introducing yourself and identifying your county.

---

{:.info}
For an introduction into canvassing and how it works, see *[Grassroots Canvassing Efforts](/canvassing/overview/)*.


## Join an Existing Team

Canvassing can be a time-intensive task, and often many volunteers are needed. We recommend contacting existing organizations in your state to find out what canvassing efforts are underway and if you can volunteer. 

Here are the canvassing efforts we know about in {{ page.canvass_teams_by_state | size }} states so far, and how to get in touch with them:

<!--
This table is populated via the data at the top of this file.
We obfuscate email addresses so they are less likely to be
picked up by spam bots.
-->
<div class="responsive-table">
    <table id="canvass-table">
        <tr>
            <th>Nation-Wide</th>
            <td>
                <p>US Election Integrity Plan (USEIP) is a leading grass-roots organization in exposing and correcting election fraud. By registering your details on their website via <a href="https://useip.org/get-involved/">USEIP › Get Involved</a>, they will aim to connect you with others in your region or area of interest. You can tick the box that says “I want to help canvass!” if that's how you'd like to help.</p>

                <p>Separately, <a href="https://canvass50.org/">Canvass50.org</a> has an online form for surveying all voters nationwide about the voting method they used in the Nov 2020 election. Help spread this link to as many people as possible to help gather some baseline stats and identify anomalies.</p>
            </td>
        </tr>
        {% for state in page.canvass_teams_by_state %}
        <tr><th id="{{ state[0] | slugify }}">
                <a href="/fraud-summary-by-state/{{ state[0] | slugify }}/">{{ state[0] }}</a>
            </th>
            <td>{{ state[1]
                    | markdownify
                    | replace: '@', '&#64;'
                    | replace: 'mailto:', site.data.globals.mailtoPrefix
                }}</td>
            </tr>
        {% endfor %}
    </table>
</div>

If your state or county is not listed, try asking on [the Telegram channels for your state](/telegram-channels/) about what might be happening in your area, or register with [USEIP.org](https://useip.org/get-involved/) as mentioned above.

If you've discovered any new information missing from this list, please let us know via [Telegram]({{ site.data.globals.telegramLink }}), [Twitter]({{ site.data.globals.tweetUsLink }}), [Email]({{ site.data.globals.mailtoPrefix }}{{ site.data.globals.electionDataEmailEncoded }}), or post a comment at the bottom of this page.

----

If you've contacted USEIP and cannot find any canvassing groups in your area, [learn more about what's involved in running a canvass](/canvassing/get-started/), with our list of resources and groups that might be able to assist.


<style>
/* Styling tweak, just for this page - reduces the spacing slightly between items */
td > p:last-child, 
td > ul:last-child, 
td > ol:last-child 
{ margin-bottom: 0.5em; }

/* Make table more readable on mobile by stacking the table vertically */
@media (max-width: 500px) {
  #canvass-table th, #canvass-table td {
    display: block;
  }
}
</style>