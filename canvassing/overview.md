---
title: Grassroots Canvassing Efforts
breadcrumb: "[Follow the Data](#colophon) ›"
meta_desc: We explain the process of canvassing voters, door-to-door, and present the eleven states that are already showing alarming results.
last_updated: 10 Oct 2021
comment_intro: Do you know of other canvassing efforts going on around the country? Or know of details we might have missed? Let us know in the comments below.
---

> The only election ever overturned by a federal court, Marks vs Stinson (1993) included extensive canvassing that proved fraud through absentee ballots." [^1]

One of the best and most provable ways to identify fraud is through voter canvassing --- going door-to-door to identify whether the county or state records match the actual residents living at the address. This usually involves checking both the party registration records, as well as the 2020 voter records to ensure that information such as name, address, and voting method all match.

**Why Canvass?**

1. To provide hard evidence that supports requests for a forensic audit in the county or state

2. If the number of errors proves large enough, it may also provide enough evidence *on its own* to demonstrate that there was an illegitimate certification of the election, and that it should be decertified and redone

3. To clean up voter rolls that have not been properly maintained. In some regions there are hundreds of thousands of registrations that need to be removed from the rolls.

Since there may be *millions* of residents within a given county, this process typically involves analyzing the voter rolls for statistical anomalies and invalid addresses first, and then ideally narrowing the canvass to those invalid addresses or regions where the most anomalies are found. These errors can then be directly verified, and if a resident is able to be interviewed at the address, they may be willing to sign a sworn affidavit that testifies to the irregularity found. Affidavits add significant weight to allegations when presented to courts and state legislatures.

Despite an outcry from prominent Democrats and Attorney General Merrick Garland who believe this amounts to "voter intimidation" (we think it must be *them* who are intimidated by an open audit), it's possible to interview voters to verify key information without asking who they voted for.

Canvassing is particularly good at identifying "phantom voters" --- fake or fraudulent names and addresses added to the voter registration rolls for the purpose of injecting fake ballots that appear real. It appears that this is a common tactic in the overall strategy of manipulating elections.

{% include page_card url="/canvassing/" %}
{% include page_card url="/canvassing/get-involved/" %}
{% include page_card url="/canvassing/get-started/" %}


### Footnotes & References

[^1]: Quote is from Steve Bannon's War Room: [Episode 1,244 – Canvassing is Coming to all 50 States](https://rumble.com/vm8s8h-episode-1244-canvassing-is-coming-to-all-50-states.html). 

    See also The New York Times: "[Vote-Fraud Ruling Shifts Pennsylvania Senate](https://www.nytimes.com/1994/02/19/us/vote-fraud-ruling-shifts-pennsylvania-senate.html)", Feb. 19, 1994; and the [legal case report here](https://casetext.com/case/marks-v-stinson).

    According to posts on [Politics StackExchange](https://politics.stackexchange.com/a/60747), it may not be the only one ever overturned, but it appears that most election fraud cases like this are *invalidated*, meaning the election must be redone. Being *overturned* is a rare situation whereby a the winning party is  declared illegitimate and another party is declared the winner.

