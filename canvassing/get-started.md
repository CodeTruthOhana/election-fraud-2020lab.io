---
title: Getting Started with Grassroots Canvassing to Uncover Election Fraud
meta_desc: How to get started with a canvassing effort in your state, with resources, links and an overview of what will be required.
parent_page: /canvassing/overview/
last_updated: 10 Oct 2021
comment_intro: Do you know of any other helpful resources or details we might have missed? Let us know in the comments below.
---

*Are there canvassing efforts already happening in your state or county? You'll want to check out our **[list of states doing canvassing](/canvassing/get-involved/)** before proceeding further.*


## Initiating a Canvass In Your Area

If nothing is yet organized you might be able to initiate one yourself, although you will likely need a team of other like-minded people in the target area, and it's important to learn from the successes and mistakes made in other states.

Here's a few suggestions on how to get started:

1. Watch and absorb the following video by James Knox which explains some of the common pitfalls with canvassing:

   <div class="responsive-iframe">
       <iframe width="560" height="315" src="https://videopress.com/embed/XxhrAwKa" frameborder="0" allowfullscreen></iframe>
       <script src="https://videopress.com/videopress-iframe.js"></script>
   </div>

1. Read [USEIP's County & Local Organizing Playbook](https://useip.org/2021/08/14/useip-county-local-organizing-playbook-is-now-available/) which explains everything the Colorado team has learnt during their process.

1. Join some of the canvassing online groups mentioned above to learn what tactics other groups have been using and potential issues you might encounter.

1. Gather an initial core team of willing volunteers in your area, including people familiar with election data and processes

2. Identify the counties and cities with the most statistical anomalies --- this will show you where to focus your efforts. Some good references are:

    - [Seth Keshel's Reports and Heat Maps](/seth-keshel-reports/)
    - [Identifying Electoral Fraud Using Trend Analysis](/trend-analysis/)
    - [USEIP's Election Fraud Data Analytics Guide](https://useipdotus.files.wordpress.com/2021/08/election-analytics-guide.pdf)
    - [USEIP's Election Data Analyzer Tool](https://rumble.com/vm3iif-before-you-canvass-use-the-election-data-analyzer.html) (see also their [Telegram Channel](https://t.me/electiondataanalyzer) or [USEIP.org](https://useip.org/))
    

3. Create a clear plan for how you're going to approach it, how many properties you will need to visit, what questions to ask, how to collate the results, and how best to collect written affidavits (legal advice would be important in getting this step right). Consider contacting the canvass organizers we mentioned above for their suggestions and advice --- many will be willing to help.

4. Armed with a plan, recruit additional volunteers, run training for the team, in person (if possible) or online, to ensure the work is done accurately and consistently

5. Collate your findings into a database or spreadsheet, and write a written report on what was discovered. Get a third-party to review your report and findings.

6. Present your findings to your county officials and election clerk, if they're willing, asking for further investigations such as a forensic audit. You could also contact state representatives that have voiced support for election integrity and audits.

7. Share your findings with us and any media organization who will listen (and don't be discouraged by the many that will not!)

We'll be glad to promote your canvassing effort and the results here on our site. [Get in touch](https://twitter.com/SiResearcher) if you're interested.
