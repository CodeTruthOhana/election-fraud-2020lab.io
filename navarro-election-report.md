---
title: The Peter Navarro Report on the 2020 Election 
breadcrumb: "[Follow the Data](#colophon) ›"
last_updated: 28 Jan 2022
content_width: 860
---

Peter Navarro, professor emeritus of economics and public policy at the University of California-Irvine for more than 20 years, served as *Assistant to the President* and *Director of the Office of Trade and Manufacturing Policy* at the White House during the Trump Administration.

He produced the three volumes of The Navarro Report in his personal capacity to shed light on potentially illegal activity that may have occurred during the 2020 Presidential Election.

His summary page shows the extent of the issues he discovered:

![Peter Navarro Election Report Summary](/images/navarro-report/chart.png){:width="1239" height="696"}

![Peter Navarro Election Report Numbers](/images/navarro-report/chart-2.png){:width="1405" height="1875"}


<!-- Might expand on some of his key points, later on -->


Here is Volume I of the report:

<iframe class="scribd_iframe_embed" title="The Immaculate Deception Revised " src="https://www.scribd.com/embeds/488928271/content?start_page=1&view_mode=scroll&access_key=key-hbOoB1s2mfbJvoEMQHZk" tabindex="0" data-auto-height="true" data-aspect-ratio="0.7729220222793488" scrolling="no" width="100%" height="1000" frameborder="0"></iframe>


{% include pdf_box.html
    url="https://www.dropbox.com/s/584r7xtnngauc4t/The%20Navarro%20Report%20Vol%20I%2C%20II%2C%20III%20-%20Feb.%202%2C%202021.pdf?dl=0"
    title="The Navarro Report - Volumes I, II, III"
    description="All three volumes combined into a single PDF. From [PeterNavarro.com](https://peternavarro.com/the-navarro-report/)."
%}