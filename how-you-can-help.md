---
title: How You Can Help
subtitle: Volunteer an hour or more a week to help expand our coverage of election issues
last_updated: 25 Mar 2022
comments_enabled: false
---

ElectionFraud20.org is run completely by volunteers who wish to see the public truthfully informed about the 2020 election, the anomalies, and any fraud that marred a free and fair election.

If you're interested in assisting with securing America's elections, first check out the local election integrity groups in your state, as assisting *locally* is one of the crucial ways to see change --- one county at a time. There are many active groups on Telegram, and you can find these on our [List of Telegram Groups](/telegram-channels/). There's also canvassing efforts underway in many states, and most would gladly welcome volunteers. [See the list here](/canvassing/get-involved/).

If you'd like to assist with our website, keeping it up-to-date and providing thorough coverage across the entire nation, we would also gladly receive your help, even if it's only an hour a week. Consider what gifts and abilities you have and how you could best contribute. Reach out on [Telegram](https://t.me/SiWiFi), or you can email [{{ site.data.globals.electionDataEmailEncoded }}]({{ site.data.globals.mailtoPrefix }}{{ site.data.globals.electionDataEmailEncoded }}) and express your interest.

**Even though this website has close to 200 pages and 135,000 words, we estimate that this covers less than 20% of the issues that have been reported surrounding the election. We'd love to expand the coverage and help provide the public with clear, succinct information about what really occurred.**


## Current Focus Areas

#### Examining the Data

One part of our work is looking at the data such as ballot tallies and voter rolls, and identifying anomalies that indicate where to look for fraud. Our [trend analysis articles](/trend-analysis/) show some of the research to date. Anyone that is capable with Excel, working with databases, or creating charts/graphs would be welcome to assist with this.


#### Writing Clear Explanatory Articles

Similar to Wikipedia, we'd like to provide clear and well-referenced explanations of the the key election issues. As of March-April 2022, we're keen to publish more information about:

* The forensic audit underway in Otero County, [New Mexico](/fraud-summary-by-state/new-mexico/)

* The Gableman Reports, True The Vote presentation, and other happenings in [Wisconsin](/fraud-summary-by-state/wisconsin/)

* The lawsuits in [Colorado](/fraud-summary-by-state/colorado/)

* Any states that are [canvassing](/canvassing/) voters directly and reporting their results, especially [Colorado](/fraud-summary-by-state/colorado/), [Pennsylvania](/fraud-summary-by-state/pennsylvania/) and [Michigan](/fraud-summary-by-state/michigan/)

* [ERIC](/fraud-summary-by-state/colorado/#score-voter-registration-database-vulnerabilities), the inter-state voter registration system that may be involved in manipulating voter rolls. Articles [here](https://100percentfedup.com/what-is-eric-the-electronic-registration-information-center-a-dream-database-for-voter-fraud/) and [here](https://www.thegatewaypundit.com/2022/01/eric-investigation-part-3-soros-open-society-founding-nations-largest-voter-roll-clean-operation/) and [here](https://www.youtube.com/watch?v=_DMLXlZMlqI) (at 30min mark) might get you started.

* The organizations that affect elections nationally, and their potential role in enabling or permitting fraud: Federal Election Assistance Commission (EAC), the [voting system test laboratories](/in-detail/vstl-labs-not-eac-accredited/) Pro V&V and SLI Compliance, Centre for Tech and Civic Life (CTCL), [Dominion Voting Systems](/in-detail/dominion-voting-machines/), ES&S, HART, Clearballot, Smartmatic, Runbeck, etc.

* The hearings in Pima County, [Arizona](/fraud-summary-by-state/arizona/)

You could pick one of the above topics and help with research, writing succinct well-referenced summaries, or anything else that you can offer. Let us know what you'd be interested in tackling.


## Other General Needs

Alternatively you could consider adopting a particular state and helping us keep that information up-to-date. 

Or here are a few suggestions for things we would find helpful:

* **Research** -- finding accurate and well-researched articles and reports about election issues that have not already been covered on the website; and improving our footnotes and references to ensure our information is based on solid, verifiable information

* **Editing and proofreading** -- correcting typos, and helping improve our wording to make things as clear and accurate as possible

* **Video clips** -- trimming long videos into small, bite-sized clips that highlight the most important information and are easily shared on social media. There are many long videos throughout the site that have great nuggets buried within them.

* **Graphics** -- creating infographics and diagrams that help illustrate the statistics within our articles

* **Social media** -- helping craft accurate, succinct Telegram posts on the election news, and sharing these on other networks such as Gab, Gettr, Truth Social, Twitter etc.


## Submitting Contributions

If you've done research or written content that you'd like to submit to us, you can do so via [Telegram](https://t.me/SiWiFi) or [email]({{ site.data.globals.mailtoPrefix }}{{ site.data.globals.electionDataEmailEncoded }}). You can send it in plain text, a Word doc, or Markdown (which is what our website uses). We'll review and get it onto the website ASAP.

Or if you have little more technical ability, you might like to make direct changes to the website files [via Gitlab](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/blob/master/README.md). We've provided some initial instructions there for how to set that up, but please reach out if you need assistance.

